import numpy as np

from fealpy.functionspace.LagrangeFiniteElementSpace import LagrangeFiniteElementSpace
from fealpy.mesh import TriangleMesh

from fealpy.tools.Cartesian_coordinates_function import Cartesian_coordinates_function, Cartesian_coordinates_function2
from fealpy.decorator import cartesian

from fealpy.tools.plotshow import Contourf_show, Image_show
import matplotlib.pyplot as plt


class FepgPost_processing:
    def __init__(self,sh,uh,node,cell):
        '''
        二维线弹性力学方程fepg三角形线性元求解结果后处理程序，绘制云图以及函数值
        '''

        mesh = TriangleMesh(node,cell)
        space =  LagrangeFiniteElementSpace(mesh, 1, spacetype='C')
        
        self.Sh_car_func = Cartesian_coordinates_function2(space,sh)
        self.Uh_car_func = Cartesian_coordinates_function2(space,uh)

        self.number_of_global_dofs = 2*uh.shape[0]
        self.space = space

    #########################################################################
    @cartesian
    def Stress(self,p,k=None):
        p = np.array(p,dtype=np.float64)
        Sigmah = self.Sh_car_func.value_u(p)
        if k is not None:
            Sigmah = Sigmah[...,k]
        return Sigmah

    @cartesian
    def Sxx(self,p):
        return self.Stress(p,k=0)


    @cartesian
    def Syy(self,p):
        return self.Stress(p,k=1)

    @cartesian
    def Sxy(self,p):
        return self.Stress(p,k=2)



    ##########################################################################
    @cartesian
    def Displacement(self,p,k=None):
        #位移函数值
        #k要求的位移值
        #0 -- u
        #1 -- v
        p = np.array(p,dtype=np.float64)
        Uh = self.Uh_car_func.value_u(p)
        if k is not None:
            Uh = Uh[...,k]
        return Uh




    ############################################################################
    def show_stress(self,show_n,k=None,level=None,show_image=False):
        #应力云图
        #k要展示的图
        #0 -- s_xx
        #1 -- s_yy
        #2 -- s_xy
        x = np.linspace(0,1,show_n)
        y = np.linspace(1,0,show_n) #用imshows绘图时,y坐标轴刚好反了过来
        X,Y = np.meshgrid(x,y)
        shape = X.shape
        shape +=(2,)
        p = np.zeros(shape,dtype=float)
        p[...,0] = X
        p[...,1] = Y

        level = 2*show_n if level is None else level
        stress_type =['s_xx','s_yy','s_xy']
        if k is None:
            k = [0,1,2]
        elif type(k) is int:
            k = [k]

        Sigmah = self.Stress(p,k=k)
        for i in range(len(k)):
            Z =  Sigmah[...,i]

            if show_image:
                Image_show(X,Y,Z,stress_type[k[i]],level=level)

            else:
                #idx = np.arange(show_n//2)
                #print(np.max(np.abs(Z[idx]-Z[show_n-idx-1])),'ss')
                #print(np.max(np.abs(Z[idx]+Z[show_n-idx-1])),'sss')
                #print(Z)
                Contourf_show(X,Y,Z,stress_type[k[i]],level=level)

        print('The stress diagram has been drawn.')




    ############################################################################


    ############################################################################
    def show_displacement(self,show_n,k=None,level=None,show_image=False):
        #位移云图
        #k要展示的图
        #0 -- u
        #1 -- v
        x = np.linspace(0,1,show_n)
        y = np.linspace(1,0,show_n) #用imshows绘图时,y坐标轴刚好反了过来 
        X,Y = np.meshgrid(x,y)
        shape = X.shape
        shape +=(2,)
        p = np.zeros(shape,dtype=float)
        p[...,0] = X
        p[...,1] = Y

        displacementh_type =['u','v']
        level = 2*show_n if level is None else level
        if k is None:
            k = [0,1]
        elif type(k) is int:
            k = [k]

        Displacementh = self.Displacement(p,k=k)
        for i in range(len(k)):
            Z = Displacementh[...,i]

            if show_image:
                Image_show(X,Y,Z,displacementh_type[k[i]],level=level)

            else:
                Contourf_show(X,Y,Z,displacementh_type[k[i]],level=level)

        print('The displacement diagram has been drawn.')







    def show_mesh(self,show_node=False,show_edge=False,show_cell=False):
        fig = plt.figure()
        axes = fig.gca()

        mesh = self.mesh
        mesh.add_plot(axes)

        if show_node:
            mesh.find_node(axes, showindex=True)
        if show_edge:
            mesh.find_edge(axes, showindex=True)
        if show_cell:
            mesh.find_cell(axes, showindex=True)
        print('The mesh has been drawn.')


