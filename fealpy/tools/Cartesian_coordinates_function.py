import numpy as np
from numpy.linalg import inv
from fealpy.decorator import cartesian


class Cartesian_coordinates_function:
    def __init__(self,space,uh):
        "实现将重心坐标变为笛卡尔坐标"
        mesh = space.mesh
        cell2dof = space.cell_to_dof()
        basis = space.basis

        cell = mesh.entity('cell') #(NC,gdim+1)
        node = mesh.entity('node') #(NN,gdim)
        gdim = node.shape[-1]
        NC = mesh.number_of_cells()
        Node = node[cell] #(NC,gdim+1,gdim)
        node0 = Node[:,0,:]
        matrix = Node[:,1:,:] - node0[:,None,:]
        self.gdim = gdim
        self.mesh = mesh
        self.matrix = matrix
        self.get_lam_coef = inv(matrix) #(NC,gdim,gdim)
        self.node0 = node0
        self.NC = NC
        self.basis = basis
        self.uh = uh
        self.cell2dof = cell2dof
        self.space = space

    def point_to_index_and_bc(self,p): #p.shape = (Np,gdim)
        gdim = self.gdim
        if p.shape[-1] != gdim:
            print('dimension mismatch')
            return
        NC = self.NC
        matrix = self.matrix
        node0 = self.node0.T[:,None,:] #(gdim,1,NC)
        Np = p.shape[0] #要判断点的个数
        p = p.T #(gdim,Np)
        idx = np.arange(Np) #未判断出来单元的点
        index = np.zeros(Np,dtype=int) #点对应点单元
        bc = np.zeros((Np,gdim+1),dtype=float) #重心坐标


        for i in range(NC):
            A = matrix[i,:].T
            node = node0[...,i]
            #print(A)
            lam = np.linalg.solve(A,p[:,idx]-node) #(gdim,...)
            if gdim == 2:
                idxs = (((lam[0,:] > -1e-15)&(lam[1,:] > -1e-15))&((lam[0,:]+lam[1,:])<1+1e-15))
            elif gdim == 3:
                idxs = (((lam[0,:] > -1e-15)&(lam[1,:] > -1e-15))
                        &((lam[2,:] > -1e-15)&((lam[0,:]+lam[1,:]+lam[2,:])<1+1e-15)))
            lam = lam[:,idxs].T
            bc[idx[idxs],1:] = lam
            bc[idx[idxs],0] = 1.0 - np.sum(lam,axis=-1)
            index[idx[idxs]] = i
            idx = np.delete(idx,idxs)
            if len(idx) == 0:
                break

        if len(idx) !=0:
            print('some points are wrang!')
            return
        return index, bc


    def point_to_index_and_bcs(self,p): #p.shape = (Np,gdim)
        gdim = self.gdim
        if p.shape[-1] != gdim:
            print('dimension mismatch')
            return
        NC = self.NC
        get_lam_coef = self.get_lam_coef #(NC,gdim,gdim)
        node0 = self.node0[None,:,:] #(1,NC,gdim)
        Np = p.shape[0] #要判断点的个数
        p = p[:,None,:] #(Np,1,gdim)
        Lam = np.einsum('ikj,lik->lij',get_lam_coef,p-node0)#(NC,gdim,gdim),(Np,NC,gdim)->(Np,NC,gdim)

        index = []
        bc = []
        lam = np.zeros((NC,gdim+1),dtype=np.float)
        for i in range(Np):
            lam[:,1:] = Lam[i]#(NC,gdim)
            lam[:,0] = 1.0-np.sum(Lam[i],axis=-1)
            idx = lam > -1e-15 #(NC,gdim+1)
            idx, = np.nonzero(np.prod(idx,axis=-1))
            if len(idx) == 0:
                raise ValueError('point',p[i,0], 'does not belong to any cell')
            else:
                bc.append(lam[idx])
                index.append(idx)
        return index,bc


            
            


    def values(self,p):
        gdim = self.gdim
        if p.shape[-1] != gdim:
            print('dimension mismatch')
            return
        if len(p.shape) == 1:
            p = p[None,:] #只有一个点
        shape = p.shape[:-1]
        p = p.reshape((-1,gdim)) #(Np,gdim)
        Np = p.shape[0]
        NC = self.NC
        index, bc = self.point_to_index_and_bcs(p)
        uh = self.uh
        val_shape = list(uh.value(np.zeros(gdim+1,dtype=np.float)).shape)
        val_shape[0] = Np
        val = np.zeros(val_shape,dtype=np.float)#(Np,) or (Np,gdim), or (Np,gdim,gdim)
        for i in range(Np):
            n_Nc = len(index[i])
            val_temp = uh.value(bc[i],index=index[i])[np.arange(n_Nc),np.arange(n_Nc)]#(n_NC,...)
            val[i] = np.sum(val_temp,axis=0)/n_Nc
        shape +=val.shape[1:]
        return val.reshape(shape)







    def value(self,p):
        gdim = self.gdim
        if p.shape[-1] != gdim:
            print('dimension mismatch')
            return
        if len(p.shape) == 1:
            p = p[None,:] #只有一个点
        shape = p.shape[:-1]
        p = p.reshape((-1,gdim)) #(Np,gdim)
        Np = p.shape[0]
        NC = self.NC
        index, bc = self.point_to_index_and_bc(p) #(Np,) (Np,gdim+1)
        uh = self.uh
        if NC > Np:
            val = uh.value(bc,index=index) #(Np,Np,...)
            val = val[np.arange(Np),np.arange(Np)] #(Np,...)
        else:
            val = uh.value(bc)#(Np,NC,...)
            val = val[np.arange(Np),index] #(Np,...)
        shape +=val.shape[1:]
        #print(val.reshape(shape).shape)
        return val.reshape(shape)









    def grad_value(self,p):
        gdim = self.gdim
        if p.shape[-1] != gdim:
            print('dimension mismatch')
            return
        if len(p.shape) == 1:
            p = p[None,:] #只有一个点
        shape = p.shape[:-1]
        p = p.reshape((-1,gdim)) #(Np.gdim)
        Np = p.shape[0]
        index, bc = self.point_to_index_and_bc(p) #(Np,) (Np,gdim+1)
        uh = self.uh
        cell2dof = self.cell2dof
        uh = uh[cell2dof[index]] #(Np,ldof)

        grad_basis = self.space.grad_basis(bc) 
        grad_basis_shape = grad_basis.shape #(Np,NC,ldof,gdim,gdim)
        #print(grad_basis_shape)
        if grad_basis_shape[1] == 1:
            val = np.einsum('ij,ij...->i...',uh,grad_basis[:,0,...]) #(Np,...)
        else:
            val = np.einsum('ij,iij...->i...',uh,grad_basis[:,index,...])
        shape += val.shape[1:]
        #print(shape)
        return val.reshape(shape)

    def Recover_divu(self,p): #恢复div u
        grad_u_val_h = self.grad_value(p)
        return grad_u_val_h[...,0,0]+grad_u_val_h[...,1,1]
    def Recover_sgimah(self,p,lam=1,mu=0.5): #通过u恢复sigma
        grad_u_val_h = self.grad_value(p)
        div_u_val_h = grad_u_val_h[...,0,0]+grad_u_val_h[...,1,1]

        eplison_u_val_h = np.copy(grad_u_val_h)
        eplison_u_val_h[...,1,0] = 0.5*(eplison_u_val_h[...,1,0]+eplison_u_val_h[...,0,1])
        eplison_u_val_h[...,0,1] = eplison_u_val_h[...,1,0]

        sigma_h = 2*mu* eplison_u_val_h
        sigma_h[...,0,0]+=lam*div_u_val_h
        sigma_h[...,1,1]+=lam*div_u_val_h
        sigma_h = sigma_h[...,[0,1,1],[0,1,0]]
        
        return sigma_h
    
        
        
        
            





class Cartesian_coordinates_function2:
    def __init__(self,space,uh):
        '''实现将重新坐标变为笛卡尔坐标,区别在于考虑到有不连续情形，对于
一个点可能属于多个单元，该点有限元函数值为多个单元的有限元函数值平均'''
        mesh = space.mesh
        cell2dof = space.cell_to_dof()
        basis = space.basis

        cell = mesh.entity('cell') #(NC,gdim+1)
        node = mesh.entity('node') #(NN,gdim)
        dim = node.shape[-1]
        NC = mesh.number_of_cells()
        Node = node[cell] #(NC,gdim+1,gdim)
        node0 = Node[:,0,:]#(NC,gdim)
        self.get_lam_coef = inv(Node[:,1:,:] - node0[:,None,:]) #(NC,gdim,gdim)
        self.dim = dim
        self.mesh = mesh
        self.node0 = node0
        self.NC = NC
        self.basis = basis
        self.uh = uh
        self.cell2dof = cell2dof
        self.space = space


    def point_to_index_and_bc(self,p): #p.shape=(dim,),只算一个点
        dim = self.dim
        if len(p) != dim:
            raise ValueError("dimension mismatch")

        bc = np.einsum('ijk,ij->ik',self.get_lam_coef,p[None,:] - self.node0)

        if dim == 2:

            idx, =  np.nonzero(((bc[:,0]> -1e-10)&(bc[:,1]>-1e-10))&((bc[:,0]+bc[:,1])<1+1e-10))

            if len(idx)>0:
                bcs = np.zeros((len(idx),3),dtype=bc.dtype)
                bcs[:,1:] = bc[idx]
                bcs[:,0] = 1.0 - np.sum(bcs[:,1:],axis=-1)


                return idx, bcs

            else:
                raise ValueError("Point is wrang!")

    @cartesian
    def value_u(self,p):
        #得到u的函数值，u=(u1,u2)
        dim = self.dim
        if p.shape[-1] != dim:
            raise ValueError("'dimension mismatch")

        is_one_point = False
        if len(p.shape) == 1:
            p = p[None,:]
            is_one_point = True

        shape = p.shape
        p = p.reshape(-1,dim)
        Np = p.shape[0]
        
        if len(self.uh.shape)==2:
            udim = self.uh.shape[-1]   
        val = np.zeros((Np,udim),dtype=p.dtype)
        if udim != dim:
            shape = list(shape)
            shape[-1] = udim
        
        for i in range(Np):
            index, bc = self.point_to_index_and_bc(p[i]) #(NC_idx,), (NC_idx,gdim+1)
            val[i] = np.mean(np.einsum("ijk,ij->ik",self.uh[self.cell2dof[index]],self.basis(bc)[...,0,:]),axis=0) #取平均值作为函数值
        
        val = val.reshape(shape)

        if is_one_point:
            val = val[0]
        return val


    @cartesian
    def value_s(self,p):
        #得到stress的函数值
        dim = self.dim
        if p.shape[-1] != dim:
            raise ValueError("'dimension mismatch")

        is_one_point = False
        if len(p.shape) == 1:
            p = p[None,:]
            is_one_point = True

        shape = p.shape[:-1]+(3,)
        p = p.reshape(-1,dim)
        Np = p.shape[0]

        val = np.zeros((Np,3),dtype=p.dtype)

        for i in range(Np):
            index, bc = self.point_to_index_and_bc(p[i]) #(NC_idx,), (NC_idx,gdim+1)
            #print(index,'\n')
            #print(self.uh[self.cell2dof[index]])
            val[i] = np.mean(np.einsum("ijk,iijk...->i...",self.uh[self.cell2dof[index]],self.basis(bc,index=index)),axis=0) #取平均值作为函数值

        val = val.reshape(shape)

        if is_one_point:
            val = val[0]
        return val
                                                                      


