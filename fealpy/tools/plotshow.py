import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np


color = ['#030764','#0343df',#蓝色渐变
         '#069af3','#0bf9ea', #青色渐变
         '#01c08d','#0cff0c',#绿色渐变
         '#ffff14',#黄色渐变
         '#e17701',#橙色渐变
         '#f43605' #红色渐变
        ]

my_cmap1=mpl.colors.LinearSegmentedColormap.from_list(name='mycmp1',colors=color)
#系统自带的
cmap1 = mpl.cm.jet 
cmap2 = mpl.cm.turbo

cmap = cmap1

def Contourf_show(X,Y,Z,name,level=50):

    norm = mpl.colors.Normalize(vmin=Z.min(),vmax=Z.max())

    fig, ax = plt.subplots()
    CS = ax.contourf(X, Y, Z, level, cmap=cmap,norm=norm)
    
    cbar=plt.colorbar(CS,)
    
    print(Z.min(),Z.max())
    cbar.set_ticks(np.linspace(Z.min(),Z.max(),13))
    cbar.formatter.set_scientific(True)
    cbar.formatter.set_powerlimits((0,0))
    cbar.update_ticks()

    ax.set_title(name)



def Image_show(X,Y,Z,name,level=50):
    
    norm = mpl.colors.Normalize(vmin=Z.min(),vmax=Z.max())

    fig, ax = plt.subplots()
    im = ax.imshow(Z,  interpolation='bilinear', cmap=cmap, 
                            extent=[0, 1, 0, 1],norm=norm)
    cbar = fig.colorbar(im,)

    print(Z.min(),Z.max())
    cbar.set_ticks(np.linspace(Z.min(),Z.max(),13))
    cbar.formatter.set_scientific(True)
    cbar.formatter.set_powerlimits((0,0))
    cbar.update_ticks()


    ax.set_title(name)




def Show_table(S):
    m = S.shape[0]
    n = S.shape[1]
    
    print('\n')
    for i in range(m):
        for j in range(n):
            print('&','%.3e'%S[i,j],end='')
        print('\\\\',"\n")

    print('\n')
