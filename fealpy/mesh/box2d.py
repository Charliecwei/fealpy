import gmsh
import sys
import numpy as np
import matplotlib.pyplot as plt



def box2d(h0=0.25,h=0.001):

    gmsh.initialize()

    ####################
    gmsh.initialize()

    gmsh.model.geo.addPoint(0,0,0,h,1)
    gmsh.model.geo.addPoint(1,0,0,h,2)
    gmsh.model.geo.addPoint(1,1,0,h,3)
    gmsh.model.geo.addPoint(0,1,0,h,4)


    gmsh.model.geo.addPoint(0.5,0,0,h0,5)
    gmsh.model.geo.addPoint(1,0.5,0,h0,6)
    gmsh.model.geo.addPoint(0.5,1,0,h0,7)
    gmsh.model.geo.addPoint(0,0.5,0,h0,8)



    gmsh.model.geo.addLine(1,5,1)
    gmsh.model.geo.addLine(5,2,2)
    gmsh.model.geo.addLine(2,6,3)
    gmsh.model.geo.addLine(6,3,4)
    gmsh.model.geo.addLine(3,7,5)
    gmsh.model.geo.addLine(7,4,6)
    gmsh.model.geo.addLine(4,8,7)
    gmsh.model.geo.addLine(8,1,8)

    gmsh.model.geo.addCurveLoop([1,2,3,4,5,6,7,8],1)
    gmsh.model.geo.addPlaneSurface([1], 1)



    gmsh.model.geo.synchronize()
    gmsh.model.mesh.generate(2)

    #gmsh.fltk.run()

    node = gmsh.model.mesh.getNodes()[1]
    NN = node.shape[0]//3
    node = node.reshape(NN,3)
    node = np.array(node[:,:2],dtype=np.float64)

    cell = np.array(gmsh.model.mesh.getElements(dim=2)[2][0]-1,dtype=np.int64)
    NC = cell.shape[0]//3
    cell = cell.reshape(NC,3)


    gmsh.finalize()


    return node, cell
