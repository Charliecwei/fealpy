import numpy as np

#数值积分
from fealpy.quadrature import GaussLegendreQuadrature
from fealpy.quadrature import TriangleQuadrature
#from fealpy.quadrature.TriangleQuadrature1 import TriangleQuadrature
#mesh的数据构建
from fealpy.mesh.Mesh2d import Mesh2d, Mesh2dDataStructure
from fealpy.mesh.core import LinearMeshDataStructure

from fealpy.mesh.core import lagrange_grad_shape_function



###########重写 CurveLagrangeTriangleMesh，要求边界cell有且仅有一条边界边，且为第0条边######



class TriangleMeshDataStructure(Mesh2dDataStructure):
    localEdge = np.array([(1, 2), (2, 0), (0, 1)])
    localFace = np.array([(1, 2), (2, 0), (0, 1)])
    ccw = np.array([0, 1, 2])

    NVC = 3
    NVE = 2
    NVF = 2

    NEC = 3
    NFC = 3

    def __init__(self, NN, cell):
        super(TriangleMeshDataStructure, self).__init__(NN, cell)



class CurveLagrangeTriangleMesh(Mesh2d):
    def __init__(self,node,cell,p=1,curve=None,curve_method='scott_exact'):

        self.p = p
        self.GD = 2
        self.TD = 2
        self.meshtype = 'ctri'
        self.ftype = node.dtype
        self.itype = cell.dtype
        self.node = node
        self.cell = cell
        self.curve = curve
        self.curve_method = curve_method
        
        #check 网格是否满足条件，并且初始化ds
        self.init_check_and_correct()

    
    def init_check_and_correct(self):
        """
        检查网格是否符合要求：
        边界单元有且只有一条边界边，而且为0号边界边
        若不满足，则需要修正
        """

        cell = self.cell
        node = self.node
        NN = node.shape[0]
        self.ds = TriangleMeshDataStructure(NN, cell)

        ######################修正边界边#####################
        boundary_edge_index = self.ds.boundary_edge_index()
        edge2cell = self.ds.edge_to_cell()[boundary_edge_index]


        idx, = np.nonzero(edge2cell[:,2] == 1)
        if len(idx) > 0:
            cell[edge2cell[idx[:,None],0],[0,1]] = cell[edge2cell[idx[:,None],0],[1,0]]
        

        idx, = np.nonzero(edge2cell[:,2] == 2)
        if len(idx) > 0:
            cell[edge2cell[idx[:,None],0],[0,2]] = cell[edge2cell[idx[:,None],0],[2,0]]
            self.ds.reinit(NN,cell)
            self.cell = cell
        
        #####################要保证边界边的法向为区域的外法向量#########################
        boundary_edge_index = self.ds.boundary_edge_index()
        edge2cell = self.ds.edge_to_cell()[boundary_edge_index]    
        #TODO TOMORROW 

        bc = np.array([1/2,1/2])
        n = self.edge_unit_normal(bc,index=self.ds.boundary_edge_index())
        
        pp = self.bc_to_point(bc,index=self.ds.boundary_edge_index())
        n0 = self.curve.unit_normal(pp)


        idx, = np.nonzero(np.sum((n-n0)**2,axis=-1)>1e-13)
        
        if len(idx)>0:
            boundary_edge_index = self.ds.boundary_edge_index()[idx]
            edge2cell = self.ds.edge_to_cell()[boundary_edge_index]
            cell[edge2cell[:,0,None],[1,2]] = cell[edge2cell[:,0,None],[2,1]]
            self.ds.reinit(NN,cell)
            self.cell = cell
        
        
        

        bc = np.array([1/2,1/2])
        n = self.edge_unit_normal(bc,index=self.ds.boundary_edge_index())

        if np.max(np.abs(n-n0))>1e-10:
            raise ValueError('max normal error:',np.max(np.abs(n-n0)),'some edge are wrong!')
        

        ######################修正边界边#####################




    def edge_to_cell_Sign(self):
        #edge2cell_Sign.shape=(NE,2)
        #bool,用来判断单元边和边界边是否反号
        #False表示反号了
        #单元边的序号为:(1,2) , (0,2), (0,1)
        edge2cell = self.ds.edge_to_cell()
        NE = edge2cell.shape[0]
        edge2cell_Sign = np.zeros((NE,2),dtype=np.bool8)

        localEdge = np.array([(1, 2), (0, 2), (0, 1)])

        cell = self.ds.cell
        edge = self.ds.edge

        #判断第0个单元
        index = localEdge[edge2cell[:,2]] #(NE,2)
        cell1 = cell[edge2cell[:,0]] #(NE,3)

        idx, = np.nonzero(np.prod(cell1[np.arange(NE)[:,None],index] == edge,axis=-1))
        edge2cell_Sign[idx,0] = True

        #判断第1个单元        
        index = localEdge[edge2cell[:,3]] #(NE,2)
        cell1 = cell[edge2cell[:,1]] #(NE,3)

        idx, = np.nonzero(np.prod(cell1[np.arange(NE)[:,None],index] == edge,axis=-1))
        edge2cell_Sign[idx,1] = True

        return edge2cell_Sign




    def cell_to_edge_Sign(self):
        #cell2edge_Sign.shape=(NC,3)
        #bool,用来判断单元边和边界边是否反号
        #False表示反号了
        #单元边的序号为:(1,2) , (0,2), (0,1)

        edge2cell = self.ds.edge2cell
        NC = self.number_of_cells()
        NEC = 3
        cell2edgeSign = np.zeros((NC, NEC), dtype=np.bool_)

        edge2cell_Sign = self.edge_to_cell_Sign()

        idx, = np.nonzero(edge2cell_Sign[:,0])
        if len(idx) > 0:
            cell2edgeSign[edge2cell[idx,0],edge2cell[idx,2]] = True

        idx, = np.nonzero(edge2cell_Sign[:,1])
        if len(idx) > 0:
            cell2edgeSign[edge2cell[idx,1],edge2cell[idx,3]] = True

        return cell2edgeSign



    def reference_cell_measure(self,TD=2):
        if TD == 2:
            rm = 0.5
        elif TD == 1:
            rm = 1.0
        return rm




    def integrator(self, q, etype='cell'):
        if etype in {'cell', 2}:
            return TriangleQuadrature(q)
        elif etype in {'edge', 'face', 1}:
            return GaussLegendreQuadrature(q)



    def cell_area(self,q=None,index=np.s_[:]):
        """ 

        Notes
        -----
        计算单元的面积。
        """
        p = self.p
        if self.curve_method in {'scott_exact','lenoir_exact'}:
            q = np.min([2*p,11]) #此时映射不是多项式，需要高精度数值积分
        else:
            q = p  if q is None else q

        qf = self.integrator(q, etype='cell')
        bcs, ws = qf.get_quadrature_points_and_weights()
        J = self.jacobi_matrix(bcs, index=index)
        n = np.cross(J[..., 0], J[..., 1], axis=-1)
        n = np.abs(n)
        a = np.einsum('i, ij->j', ws, n)/2.0
        return a

    

    def edge_length(self, q=None, index=np.s_[:]):
        """

        Note
        ----
        计算边的长度
        """
        p = self.p
        if self.curve_method in {'scott_exact','lenoir_exact'}:
            q = 2*p #此时映射不是多项式，需要高精度数值积分
        else:
            q = p  if q is None else q

        qf = self.integrator(q, etype='edge')
        bcs, ws = qf.get_quadrature_points_and_weights()

        J = self.jacobi_matrix(bcs, index=index)
        l = np.sqrt(np.sum(J**2, axis=(-1, -2)))
        a = np.einsum('i, ij->j', ws, l)
        return a

    



    def bc_to_point(self,bc,index=np.s_[:],etype='cell'):
        #F_K : \hat{K} \to K
        #F_K = \tilde{F}_K+\Phi_K
        #具体见
        #Bernardi, Christine. Optimal Finite-Element Interpolation on Curved Domains[J]. SIAM Journal on Numerical Analysis, 1989, 26(5):1212-1240.

        if len(bc.shape) == 1: #只有一个点
            bc = bc[None,:] #bc.shape=(1,2)
            is_one_point = True
        else:
            is_one_point = False

        #\tilde{F}_K
        p = self.tilde_FK(bc,index=index) #(NQ,NE,2) or #(NQ,NC,2)



        if self.curve is not None: #有曲边，想要处理
            #Phi_K
            TD = bc.shape[-1]-1
            if TD == 1: #边插值
                boundary_edge_index = self.ds.boundary_edge_index()
                if type(index) is slice:
                    p[:,boundary_edge_index] += self.Phi_K(bc)
                else:
                    idx_bdege, idx_index = np.where(boundary_edge_index[:,None] == index)
                    boundary_edge_index = boundary_edge_index[idx_bdege]
                    p[:,idx_index] += self.Phi_K(bc,index=idx_bdege)


            elif TD==2:#单元插值
                boundary_cell_index = self.ds.boundary_cell_index()
                if type(index) is slice:
                    p[:,boundary_cell_index] +=self.Phi_K(bc)
                else:
                    idx_bdcell, idx_index = np.where(boundary_cell_index[:,None] == index)
                    boundary_cell_index = boundary_cell_index[idx_bdcell]
                    p[:,idx_index] += self.Phi_K(bc,index=idx_bdcell)
        if is_one_point:
            p = p[0] #恢复到一个点情形
        return p

    
    




    def tilde_FK(self,bc,index=np.s_[:],return_jacobi=False):
        node = self.node
        #NQ = bc.shape[0]
        TD = bc.shape[-1]-1
        entity = self.entity(etype=TD)[index]

        if return_jacobi:
            '''返回tilde_FK 的jacobi
                lambda0 = 1 - xi - eta
                lambda1 = xi, lambda = eta
            或者
                lambda0 = 1 - xi
                lambda1 = xi
            '''
            gphi = self.grad_shape_function(bc,p=1) #(NQ,1,3,2) or #(NQ,1,2,1)
            J = np.einsum('cin, ...cim->...cnm',self.node[entity], gphi)
            return J  #(NQ,NC,2,2) or (NQ,NE,2,1)
        else:
            pp = np.einsum('ik,jkn->ijn',bc,node[entity])
            return pp #(NQ,NC,2) or (NQ,NEbd,2)


    


    def Phi_K(self,bc,index=np.s_[:]):
        #只是边界的一部分需要变换,规模很小
        #所以index为0到NEbd-1 or 0到NCbd-1
        NQ = bc.shape[0]
        TD = bc.shape[-1]-1
        node = self.node


        if TD == 1:
            NEbd = len(self.ds.boundary_edge_index())
            index = np.arange(NEbd)[index]
            boundary_edge_index = self.ds.boundary_edge_index()[index]#(NEbd,)
            bdedge = self.ds.edge[boundary_edge_index] #(NEbd,2)
            bdnode = node[bdedge] #(NEbd,2,2)
            bdp = np.einsum('ik,jkn->ijn',bc,bdnode) #(NQ,NEbd,2)
            if self.curve_method == 'scott_exact':
                xi = bc[...,0]
                bdp = self.curve.phi(bdnode,xi) - bdp




        elif TD == 2:
            boundary_cell_index = self.ds.boundary_cell_index()[index]#(NCbd,)
            bdcell = self.ds.cell[boundary_cell_index] #(NCbd,3)
            bdnode = self.node[bdcell[:,[1,2]]] #(NCbd,2)
            bc = bc[...,[1,2]] #(NQ,2)

            if self.curve_method == 'scott_exact':
                xi = bc[...,0] #(NQ,)
                bdp = self.curve.phi(bdnode,xi)#(NQ,NCbd,2)
                bdp = bdp - (np.einsum('...,ij->...ij',xi,bdnode[:,0]) + np.einsum('...,ij->...ij',1-xi,bdnode[:,1]))
                
                idx, = np.nonzero((1-xi)<1e-15)
                
                bc[idx] *= 0
                bcps = bc[:,1]/(1-bc[:,0]) #(NQC,)
                bdp *=bcps[:,None,None]

        

        return bdp


    def DPhi_K(self,bc,index=np.s_[:]):
        #只是边界的一部分需要变换,规模很小
        #所以index为0到NEbd-1 or 0到NCbd-1
        '''返回Phi_K 的jacobi
                lambda0 = 1 - xi - eta
                lambda1 = xi, lambda2 = eta
            或者
                lambda0 = 1 - xi
                lambda1 = xi
        '''

        NQ = bc.shape[0]
        TD = bc.shape[-1]-1
        node = self.node
        gphi = self.grad_shape_function(bc,p=1) #(NQ,1,3,2) or #(NQ,1,2,1)

        if TD == 1: #边上的jacobi
            boundary_edge_index = self.ds.boundary_edge_index()[index]#(NEbd,)
            NEbd = len(boundary_edge_index)
            bdedge = self.ds.edge[boundary_edge_index] #(NEbd,2)
            bdnode = node[bdedge] #(NEbd,2,2)
            if self.curve_method == 'scott_exact':
                xi = bc[:,0] #(NQ,)
                eta = bc[:,1]
                bdnode = node[self.ds.edge[boundary_edge_index]] #(NEbd,2,2)
                Phi_K = self.curve.phi(bdnode,xi) - (np.einsum('...,ij->...ij',xi,bdnode[:,0]) + np.einsum('...,ij->...ij',1-xi,bdnode[:,1])) #(NQ,NEbd,2)
                D_xi_phi = self.curve.D_xi_phi(bdnode,xi) #(NQ,NEbd,2)
                DPhi_K = np.zeros((NQ,NEbd,2,2))

                idx, = np.nonzero((xi<1.0))
                if len(idx)>0:
                    DPhi_K[idx,:,:,1] = Phi_K[idx]/(1-xi[idx][:,None,None])
                    DPhi_K[idx,:,:,0] = (DPhi_K[idx,:,:,1] + (D_xi_phi[idx] - (bdnode[:,0]-bdnode[:,1])[None,...]))*((eta[idx]/(1-xi[idx]))[:,None,None])

                idx, = np.nonzero((xi>=1.0))
                if len(idx)>0:
                    DPhi_K[idx,:,:,1] = -D_xi_phi[idx]+((bdnode[:,0]-bdnode[:,1])[None,...])

                Dphi_K = np.einsum('...cni,...cim->...cnm',DPhi_K,gphi) #(NQ,NEbd,2,1)


        elif TD == 2: #单元上的jacobi
            boundary_cell_index = self.ds.boundary_cell_index()[index]#(NCbd,)
            bdcell = self.ds.cell[boundary_cell_index] #(NCbd,3)
            bdnode = self.node[bdcell[:,[1,2]]] #(NCbd,2)
            bc = bc[...,[1,2]] #(NQ,2)
            NCbd = len(boundary_cell_index)
            
            if self.curve_method == 'scott_exact':
                 xi = bc[...,0] #(NQ,)
                 eta = bc[...,1] #(NQ,)
                 gphis = gphi[:,0,[1,2],:] #(NQ,2,2)

                 Phi_K = self.curve.phi(bdnode,xi) - (np.einsum('...,ij->...ij',xi,bdnode[:,0]) + np.einsum('...,ij->...ij',1-xi,bdnode[:,1])) #(NQ,NCbd,2)
                 D_xi_phi = self.curve.D_xi_phi(bdnode,xi) #(NQ,NCbd,2)
                 DPhi_K = np.zeros((NQ,NCbd,2,2))

                 idx, = np.nonzero(xi<1.0)
                 DPhi_K[idx,:,:,1] = Phi_K[idx,:]/(1-xi[idx][:,None,None])
                 DPhi_K[idx,:,:,0] = (DPhi_K[idx,:,:,1] + D_xi_phi[idx]-(bdnode[None,:,0]-bdnode[None,:,1]))*((eta[idx]/(1-xi[idx]))[:,None,None])
                 
                 idx, = np.nonzero((xi>=1.0))
                 DPhi_K[idx,:,:,1] = -D_xi_phi[idx] + (bdnode[None,:,0] - bdnode[None,:,1])            
            Dphi_K = np.einsum('...cni,...im->...cnm',DPhi_K,gphis) #(NQ,NEbd,2,2)



        return Dphi_K







    def jacobi_matrix(self,bc,index=np.s_[:],return_grad=False):
        '''
        -----
        计算参考单元 （xi, eta) 到实际 Lagrange 三角形(x) 之间映射的 Jacobi 矩阵。

        x(xi,eta) = tide{F}_K(xi,eta)+Phi_K(xi,eta)

        lambda_0 = 1 - xi - eta
        lambda_1 = xi
        lambda_2 = eta
        '''
        if len(bc.shape) == 1: #只有一个点
            bc = bc[None,:] #bc.shape=(1,2)
            is_one_point = True
        else:
            is_one_point = False

        #\tilde{F}_K的jacobi
        J = self.tilde_FK(bc,index=index,return_jacobi=True)
        if self.curve is not None:
            #\Phi_K的jacobi
            TD = bc.shape[-1]-1
            if TD == 1: #边上jacobi
                boundary_edge_index = self.ds.boundary_edge_index()
                if type(index) is slice:
                    J[:,boundary_edge_index] += self.DPhi_K(bc)
                else:
                    idx_bdege, idx_index = np.where(boundary_edge_index[:,None] == index)
                    boundary_edge_index = boundary_edge_index[idx_bdege]
                    J[:,idx_index] += self.DPhi_K(bc,index=idx_bdege)
                    

            elif TD == 2: #单元上jacobi
                boundary_cell_index = self.ds.boundary_cell_index()
                if type(index) is slice:
                    J[:,boundary_cell_index] +=self.DPhi_K(bc)
                else:
                    idx_bdcell, idx_index = np.where(boundary_cell_index[:,None] == index)
                    boundary_cell_index = boundary_cell_index[idx_bdcell]
                    J[:,idx_index] += self.DPhi_K(bc,index=idx_bdcell)
        if is_one_point:
            J = J[0] #恢复到一个点情形


        if return_grad is False:
            return J
        else:
            return J, gphi









    def first_fundamental_form(self, bc, index=np.s_[:],
            return_jacobi=False, return_grad=False):
        """
        Notes
        -----
            计算网格曲面在积分点处的第一基本形式。
        """

        TD = bc.shape[-1] - 1
        J = self.jacobi_matrix(bc, index=index,
                return_grad=return_grad)

        if return_grad:
            J, gphi = J

        shape = J.shape[0:-2] + (TD, TD)
        G = np.zeros(shape, dtype=self.ftype)
        for i in range(TD):
            G[..., i, i] = np.sum(J[..., i]**2, axis=-1)
            for j in range(i+1, TD):
                G[..., i, j] = np.sum(J[..., i]*J[..., j], axis=-1)
                G[..., j, i] = G[..., i, j]
        if (return_jacobi is False) & (return_grad is False):
            return G
        elif (return_jacobi is True) & (return_grad is False):
            return G, J
        elif (return_jacobi is False) & (return_grad is True):
            return G, gphi
        else:
            return G, J, gphi






    def grad_shape_function(self, bc, p=None, index=np.s_[:], variables='u'):
        """

        Notes
        -----
        计算单元形函数关于参考单元变量 u=(xi, eta) 或者实际变量 x 梯度。

        lambda_0 = 1 - xi - eta
        lambda_1 = xi
        lambda_2 = eta

        """
        p = self.p if p is None else p
        TD = bc.shape[-1] - 1
        if TD == 2:
            Dlambda = np.array([[-1, -1], [1, 0], [0, 1]], dtype=self.ftype)
        else:
            Dlambda = np.array([[-1], [1]], dtype=self.ftype)
        R = lagrange_grad_shape_function(bc, p) # (..., ldof, TD+1)
        gphi = np.einsum('...ij, jn->...in', R, Dlambda) # (..., ldof, TD)




        if variables == 'u':
            return gphi[..., None, :, :] #(..., 1, ldof, TD)
        elif variables == 'x':
            G, J = self.first_fundamental_form(bc, index=index, return_jacobi=True)
            G = np.linalg.inv(G)
            gphi = np.einsum('...ikm, ...imn, ...ln->...ilk', J, G, gphi)


            #J = self.jacobi_matrix(bc,index=index)
            #gphi = np.einsum('...ink,...ln->...ilk',np.linalg.inv(J),gphi)

            return gphi







    

    def uniform_refine(self, n=1):
        for i in range(n):
            NN = self.number_of_nodes()
            NC = self.number_of_cells()
            NE = self.number_of_edges()
            node = self.entity('node')
            edge = self.entity('edge')
            cell = self.entity('cell')
            cell2edge = self.ds.cell_to_edge()
            edge2newNode = np.arange(NN, NN+NE)
            newNode = (node[edge[:,0],:] + node[edge[:,1],:])/2.0

            if self.curve is not None:
                boundary_edge_index = self.ds.boundary_edge_index()
                newNode[boundary_edge_index] = self.curve.project(newNode[boundary_edge_index])




            self.node = np.concatenate((node, newNode), axis=0)
            p = np.r_['-1', cell, edge2newNode[cell2edge]]
            cell = np.r_['0', p[:, [0, 5, 4]], p[:, [5, 1, 3]], p[:, [4, 3, 2]], p[:, [3, 4, 5]]]
            NN = self.node.shape[0]
            self.ds.reinit(NN, cell)












    def entity_barycenter(self,etype=2,index=np.s_[:]):
        if etype in ['cell',2]:
            bc = np.array([1/3,1/3,1/3],dtype=self.ftype)
            bc = self.bc_to_point(bc,index=index)
        elif etype in ['edge', 'face', 1]:
            bc = np.array([1/2,1/2],dtype=self.ftype)
            bc = self.bc_to_point(bc,index=index)
        elif etype in ['node', 0]:
            bc = node[index]
        else:
            raise ValueError('the entity `{}` is not correct!'.format(entity))
        return bc


    def edge_tangent(self,bc,index=np.s_[:]):
        TD = bc.shape[-1]-1
        if TD == 1:
            v = self.jacobi_matrix(bc,index=index)[...,0] #(NQ,NE,2)
        else:
            raise ValueError('TD is `{}` is not correct!'.format(TD))
        return v

    def edge_unit_tangent(self, bc, index=np.s_[:]):
        v = self.edge_tangent(bc,index=index)
        length = np.sqrt(np.sum(v**2, axis=-1))
        v /= length[...,None]
        return v

    def face_tangent(self,bc,index=np.s_[:]):
        return self.edge_tangent(bc,index=index)

    def face_unit_tangent(self,bc,index=np.s_[:]):
        return self.edge_unit_tangent(bc,index=index)

    def edge_normal(self,bc,index=np.s_[:]):
        v = self.edge_tangent(bc,index=index)
        w = np.array([(0,-1),(1,0)])
        v = np.einsum('...j,jk->...k',v,w)
        return v

    def edge_unit_normal(self,bc,index=np.s_[:]):
        v = self.edge_unit_tangent(bc,index=index)
        w = np.array([(0,-1),(1,0)])
        v = np.einsum('...j,jk->...k',v,w)
        return v

    def edge_frame(self,bc,index=np.s_[:]):
        return self.edge_unit_normal(bc,index=index)

    def face_normal(self,bc,index=np.s_[:]):
        return self.edge_normal(bc,index=index)

    def face_unit_normal(self,bc,index=np.s_[:]):
        return self.edge_unit_normal(bc,index=index)




































































if __name__ == '__main__':
    from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh as CurveLagrangeTriangleMesh0
    from fealpy.pde.curve import curve_circle
    import scipy.io as sio

    Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']


    N = 10
    theta = np.arange(N)*2*np.pi/N
    node = np.zeros((N+1,2),dtype=np.float_)
    node[1:,0] = np.cos(theta)
    node[1:,1] = np.sin(theta)
    cell = np.zeros((N,3),dtype=np.int_)
    cell[:,1] = np.arange(N)+1
    cell[:,2] = np.arange(N)+2
    cell[-1,2] = 1


    #i = 2
    #node = Node[i,0]
    #cell = np.array(Cell[i,0],dtype=np.int_)



    curve_circle = curve_circle()
    mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
    mesh0 = CurveLagrangeTriangleMesh0(node,cell,curve=curve_circle)
    

    ############check mesh through mesh0#############################

    ##
    check_term = mesh.entity('cell')-mesh0.entity('cell') 
    print(np.max(np.abs(check_term)))

    check_term = mesh.entity('node')-mesh0.entity('node')
    print(np.max(np.abs(check_term)))

    check_term = mesh.entity('edge')-mesh0.entity('edge')
    print(np.max(np.abs(check_term)))

    check_term = mesh.ds.edge_to_cell()-mesh0.ds.edge_to_cell()
    print(np.max(np.abs(check_term)))




    idx1, = np.nonzero(mesh.edge_to_cell_Sign().reshape(-1))
    idx0, = np.nonzero(mesh0.edge_to_cell_Sign().reshape(-1))
    print(idx0)
    print(idx1)
    check_term = idx1-idx0
    print(np.max(np.abs(check_term)))







