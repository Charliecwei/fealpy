import numpy as np
from scipy.sparse import coo_matrix, csr_matrix, csc_matrix, spdiags, bmat
from scipy.sparse.linalg import spsolve

from ..decorator import barycentric
from .Function import Function

from ..quadrature import FEMeshIntegralAlg
from ..decorator import timer


class CurveLagrangeFiniteElementSpace:
    def __init__(self, mesh, p, q=None, spacetype='C'):
        """
        Notes
        -----
            mesh 为一个曲边三角形或者四面体网格,曲边映射是scott 或者lenoir,见
            Bernardi, Christine. Optimal Finite-Element Interpolation on Curved Domains[J]. SIAM Journal on Numerical Analysis, 1989, 26(5):1212-1240.

            p 是参数拉格朗日有限元空间的次数，它和 mesh 的次数可以不同。
        """
        self.p = p
        self.mesh = mesh
        self.dof = mesh.lagrange_dof(p, spacetype=spacetype)
        self.multi_index_matrix = mesh.multi_index_matrix

        self.GD = mesh.geo_dimension()
        self.TD = mesh.top_dimension()

        q = q if q is not None else p+3 
        self.integralalg = FEMeshIntegralAlg(
                self.mesh, q,
                cellmeasure=self.cellmeasure)
        self.integrator = self.integralalg.integrator

        self.itype = mesh.itype
        self.ftype = mesh.ftype

    def __str__(self):
        return "Curve Lagrange finite element space!"

    def 
    

