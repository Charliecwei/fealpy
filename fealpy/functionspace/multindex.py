import numpy as np


class Multindex:
    def __init__(self,dim=2,p=0):
        self.dim = dim
        self.p = p
    def multindex(self,dim=None,p=None):
            '''homogeneous polynomials of degree p, multiple indicators '''
            if p is None:
                p = self.p
            
            if dim is None:
                dim = self.dim

            if dim == 1:
                return np.array([p],dtype=int)
            else:
                totalnumber = self.Combination_number(dim=dim,p=p)
                multindex = np.zeros((totalnumber,dim),dtype=int)
                globali = 0

                for i in range(p+1):
                    localnumber = self.Combination_number(dim=dim-1,p=i)
                    multindex[globali:globali+localnumber,0] = multindex[globali:globali+localnumber,0]+p-i
                    multindex[globali:globali+localnumber,1:] = self.multindex(dim=dim-1,p=i)
                    globali = globali+localnumber
                return multindex



    def Combination_number(self,dim=None,p=None):
        if p is None:
            p = self.p
            
        
        if dim is None:
            dim = self.dim
            
        return np.math.factorial(dim+p-1)//(np.math.factorial(dim-1)*np.math.factorial(p))         


if __name__ == '__main__':
    import sys
    p = int(sys.argv[1])
    mul = Multindex(dim=3+1,p=p)
    
    print(mul.multindex())