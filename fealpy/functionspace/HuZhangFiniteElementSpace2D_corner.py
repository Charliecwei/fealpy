import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool as Pool
from scipy.sparse import csr_matrix


from fealpy.functionspace.Function import Function
from fealpy.functionspace import LagrangeFiniteElementSpace
from fealpy.quadrature import  IntervalQuadrature
from fealpy.decorator import barycentric

import pdb


class HuZhangFiniteElementSpace():
    """
    Hu-Zhang Mixed Finite Element Space 2D.
    """
    def __init__(self, mesh, p, q=None):
        self.space = LagrangeFiniteElementSpace(mesh, p, q=q) # the scalar space
        self.mesh = mesh
        self.p = p
        self.dof = self.space.dof
        self.dim = self.space.GD

        self.edof = (p-1)
        self.fdof = (p-1)*(p-2)//2
        self.cdof = (p-1)*(p-2)//2

        self.init_find_corner_and_boundary_point()
        self.init_edge_to_dof()
        self.init_face_to_dof()
        self.init_cell_to_dof()
        self.init_orth_matrices()
        self.integralalg = self.space.integralalg
        self.integrator = self.integralalg.integrator


    def init_find_corner_and_boundary_point(self):
        '''
        找到边界角点以及非角点
        '''
        mesh = self.mesh

        self.Corner_point_index = [] #存储角点，用list来存储，规模不大
        self.Non_Corner_point_and_bdFcae_index = [] #存储边界非角点以及对应边界面指标
        self.Corner_point_bdFace_index = [] #角点对应的边界面指标,以及其所在面的顶点位置
        self.Corner_point_cell_index = [] #对应的单元指标,以及其所在单元的顶点位置



        bdNode_index = mesh.ds.boundary_node_index() #所有边界顶点点指标
        Nnbd = len(bdNode_index) #边界顶点个数
        bdFace_index = mesh.ds.boundary_face_index() #所有边界面指标
        #NFbd = len(bdFace_index) #边界面个数
        bdFace2node = mesh.entity('face')[bdFace_index] #边界面到点的对应 (NFbd,gdim)
        bd_n = mesh.face_unit_normal()[bdFace_index] #边界单位外法向量 #(NFbd,gdim)
        corner_to_face_idx = np.zeros((2,2),dtype=int)
        #face = mesh.entity('face')
        for i in range(Nnbd):
            index, = np.nonzero(np.sum(bdFace2node==bdNode_index[i],axis=-1)) #找到对应到边界面
            indexs = np.array([index[0]],dtype=int)
            index = np.delete(index,0)
            while len(index)>0:
                if np.min(np.max(np.abs(bd_n[index[0]][None,:]-bd_n[indexs]),axis=-1))>1e-15:
                    #说明当前法向与现有的法向不重合
                    indexs = np.append(indexs,index[0])
                index = np.delete(index,0)
                
            if len(indexs) == 1:
                self.Non_Corner_point_and_bdFcae_index.append(np.array([bdNode_index[i],bdFace_index[indexs[0]]]))
            elif len(indexs) == 2:
                self.Corner_point_index.append(bdNode_index[i])
                k,j = np.nonzero(bdFace2node[indexs]==bdNode_index[i])
                corner_to_face_idx[:,0] = bdFace_index[indexs]
                corner_to_face_idx[:,1] = j
                #print(face[corner_to_face_idx[:,0],corner_to_face_idx[:,1]],bdNode_index[i])
                self.Corner_point_bdFace_index.append(np.copy(corner_to_face_idx))
            else:
                raise ValueError("Warn: The geometry shape is complex, and there are more than two boundary edges related to the corner！")


        Corner_to_cell = mesh.ds.node_to_cell()[self.Corner_point_index]
        face_to_cell = mesh.ds.face_to_cell()
        Corner_to_cell_idx = mesh.ds.node_to_cell(return_localidx=True)[self.Corner_point_index]
        self.N_cornerpoint = len(self.Corner_point_index) #number of corner point
        self.N_Noncornerpoint = len(self.Non_Corner_point_and_bdFcae_index)

        #print(self.N_Noncornerpoint,self.Non_Corner_point_and_bdFcae_index)

        corner_to_cell_idx = np.zeros((2,2),dtype=int)

        for i in range(self.N_cornerpoint):
            k,j= np.nonzero(Corner_to_cell[i])
            if len(j) == 1:
                raise ValueError("Need to refine corner point!")
            elif len(j) == 2:
                if np.prod(face_to_cell[self.Corner_point_bdFace_index[i][:,0],0]==j)==1:
                    js = j[[0,1]]
                elif np.prod(face_to_cell[self.Corner_point_bdFace_index[i],0][:,0]==j[[1,0]])==1:
                    js = j[[1,0]]
                else:
                    raise ValueError("face_cell_index not equal corner_cell_index")
                #print(face_to_cell[self.Corner_point_bdFace_index[i],0],js)
                corner_to_cell_idx[:,0] = js
                corner_to_cell_idx[:,1] = Corner_to_cell_idx[i,js].toarray()[0] - 1
                self.Corner_point_cell_index.append(np.copy(corner_to_cell_idx))
            elif len(j) > 2:
                raise ValueError("complex cases to do next!")

        self.Corner_point_bdFace_index = np.array(self.Corner_point_bdFace_index)
        self.Corner_point_cell_index = np.array(self.Corner_point_cell_index)
        self.Corner_point_index = np.array(self.Corner_point_index)
        self.Non_Corner_point_and_bdFcae_index = np.array(self.Non_Corner_point_and_bdFcae_index)


    def init_orth_matrices(self):
        """
        Initialize the othogonal symetric matrix basis.
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()
        gdof = self.number_of_global_dofs()
        self.Tensor_Frame = np.zeros((gdof,tdim),dtype=float) #self.Tensor_Frame[i,:]表示第i个基函数的标架

        
        NE = mesh.number_of_edges()
        idx = np.array([(0, 0), (1, 1), (0, 1)])
        TE = np.zeros((NE, tdim, tdim), dtype=float)
        self.T = np.array([[(1, 0), (0, 0)], [(0, 0), (0, 1)], [(0, 1), (1, 0)]])

        t = mesh.edge_unit_tangent()
        n = mesh.edge_unit_normal()
        frame = np.zeros((NE,gdim,gdim),dtype=float)
        frame[:,0,:] = t
        frame[:,1,:] = n

        for i, (j, k) in enumerate(idx):
            TE[:, i] = (frame[:, j, idx[:, 0]]*frame[:, k, idx[:, 1]] + frame[:, j, idx[:, 1]]*frame[:, k, idx[:, 0]])/2
        TE[:, gdim:] *=np.sqrt(2) 


        base0 = 0

        #顶点标架
        T = np.eye(tdim,dtype=float)
        T[gdim:] = T[gdim:]/np.sqrt(2)
        NN = mesh.number_of_nodes()
        shape = (NN,tdim,tdim)
        self.Tensor_Frame[:NN*tdim] = np.broadcast_to(T[None,:,:],shape).reshape(-1,tdim) #顶点标架
        base0 += tdim*NN
        edof = self.edof
        #print(base0)
        if edof > 0: #边内部连续自由度标架
            NE = mesh.number_of_edges()
            shape = (NE,edof,tdim-1,tdim)
            self.Tensor_Frame[base0:base0+NE*edof*(tdim-1)] = np.broadcast_to(TE[:,None,1:],shape).reshape(-1,tdim)
            base0 += NE*edof*(tdim-1)
        #print(base0)
        cdof = self.cdof
        if cdof > 0:
            NC = mesh.number_of_cells()
            shape = (NC,cdof,tdim,tdim)
            self.Tensor_Frame[base0:base0+NC*cdof*tdim] = np.broadcast_to(T[None,None,:,:],shape).reshape(-1,tdim) #内部标架
            base0 += NC*cdof*tdim

        #print(base0)

        if edof > 0: #边上不连续标架     
            E = (gdim+1)*(gdim)//2
            NC = mesh.number_of_cells()
            cell2edge = mesh.ds.cell_to_edge()
            idx, = np.nonzero(self.dof_flags_1()[1])
            idx = self.dof_flags()[1][idx]
            idxs = np.zeros(E*edof,dtype=int)
            for i in range(E*edof):
                idxs[i], = np.nonzero(idx[i])
            self.Tensor_Frame[base0:base0+NC*E*edof] = TE[cell2edge[:,idxs],0].reshape(-1,tdim)
            base0 += NC*E*edof


        # 对于边界顶点，如果只有一个外法向，选取该外法向来做标架
        for i in range(self.N_Noncornerpoint):
            bdnode_idx = self.Non_Corner_point_and_bdFcae_index[i,0]
            bdface_idx = self.Non_Corner_point_and_bdFcae_index[i,1]
            self.Tensor_Frame[tdim*bdnode_idx+np.arange(tdim)] = TE[bdface_idx]

        
        #print(base0,self.number_of_global_dofs()-tdim*self.N_cornerpoint)

        # 角点边界点张量基处理,以及边的连续性将系数定出来
        self.Corner_coffefices = np.zeros((self.N_cornerpoint,2,4),dtype=float) #0,1行记录对应系数
        corner_node2face = mesh.ds.node_to_edge()[self.Corner_point_index]
        corner_b = np.zeros((2,4),dtype=float)
        corner_A = np.zeros((2,2),dtype=float)

        for i in range(self.N_cornerpoint):
            bdnode_idx = self.Corner_point_index[i]
            bdface_idx = self.Corner_point_bdFace_index[i,:,0]
            TE_corner = TE[bdface_idx]
            self.Tensor_Frame[tdim*bdnode_idx+np.arange(tdim)] = TE_corner[0]
            self.Tensor_Frame[base0:base0+tdim] = TE_corner[1]


            
            base0 += tdim

            #角点系数处理
            k, j= np.nonzero(corner_node2face[i])
            idx, = np.nonzero(np.prod(self.Corner_point_bdFace_index[i,:,0][:,None]!=j,axis=0))
            #print(j,self.Corner_point_bdFace_index[i,:,0],idx)

            corner_n = n[j[idx]][0]
            TE_corner_n = np.einsum('ijk,klm,m->ijl',TE_corner,self.T,corner_n)

            #print(TE_corner_n.shape)
            corner_b[:,:2] = np.transpose(-TE_corner_n[0,1:])
            corner_b[:,2:] = np.transpose(TE_corner_n[1,1:])

            corner_A[:,0] = TE_corner_n[0,0]
            corner_A[:,1] = -TE_corner_n[1,0]

            self.Corner_coffefices[i] = np.linalg.solve(corner_A,corner_b) #(2,4)

            
    def __str__(self):
        return "Hu-Zhang mixed finite element space 2D!"


    def number_of_global_dofs(self):
        p = self.p
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()

        mesh = self.mesh

        NC = mesh.number_of_cells()
        NN = mesh.number_of_nodes()
        gdof = tdim*NN

        if p > 1:
            edof = self.edof
            NE = mesh.number_of_edges()
            gdof += (tdim-1)*edof*NE # 边内部连续自由度的个数 
            E = mesh.number_of_edges_of_cells() # 单元边的个数
            gdof += NC*E*edof # 边内部不连续自由度的个数 

        if p > 2:
            fdof = self.fdof # 面内部自由度的个数
            if gdim == 2:
                gdof += tdim*fdof*NC

        #这里考虑的每个角点只有两个单元相连
        #没有考虑角点处的法向连续性条件，在后续方程中考虑
        gdof += 3*self.N_cornerpoint
        return gdof 


    def number_of_local_dofs(self):
        ldof = self.dof.number_of_local_dofs()
        tdim = self.tensor_dimension()
        return ldof*tdim


    def cell_to_dof(self):
        return self.cell2dof


    def face_to_dof(self):
        return self.face2dof


    def edge_to_dof(self):
        return self.edge2dof


    def init_cell_to_dof(self):
        """
        构建局部自由度到全局自由度的映射矩阵

        Returns
        -------
        cell2dof : ndarray with shape (NC, ldof*tdim)
            NC: 单元个数
            ldof: p 次标量空间局部自由度的个数
            tdim: 对称张量的维数
        """
        mesh = self.mesh
        NN = mesh.number_of_nodes()
        NE = mesh.number_of_edges()
        NC = mesh.number_of_cells()

        #gdim = self.geo_dimension()
        tdim = self.tensor_dimension() # 张量维数
        #p = self.p
        dof = self.dof # 标量空间自由度管理对象 
       
        c2d = dof.cell2dof[..., np.newaxis]
        ldof = dof.number_of_local_dofs() # ldof : 标量空间单元上自由度个数
        cell2dof = np.zeros((NC, ldof, tdim), dtype=int) # 每个标量自由度变成 tdim 个自由度
        base0 = 0
        base1 = 0

        dofFlags = self.dof_flags_1() # 把不同类型的自由度区分开来
        idx, = np.nonzero(dofFlags[0]) # 局部顶点自由度的编号
        cell2dof[:, idx, :] = tdim*c2d[:, idx] + np.arange(tdim)
        base1 += tdim*NN # 这是张量自由度编号的新起点
        base0 += NN # 这是标量编号的新起点

        idx, = np.nonzero(dofFlags[1]) # 边内部自由度的编号
        if len(idx) > 0:
            # 0号局部自由度对应的是切向不连续的自由度, 留到后面重新编号
            cell2dof[:, idx, 1:] = base1 + (tdim-1)*(c2d[:, idx] - base0) + np.arange(tdim - 1)
            edof = self.edof
            base1 += (tdim-1)*edof*NE
            base0 += edof*NE

        #print(np.max(cell2dof),base1)

        idx, = np.nonzero(dofFlags[2])
        if len(idx) > 0:           
            cell2dof[:, idx, :] = base1 + tdim*(c2d[:, idx] - base0) + np.arange(tdim)
            cdof = self.cdof
            base1 += tdim*cdof*NC
 
        idx, = np.nonzero(dofFlags[1])
        if len(idx) > 0:
            cell2dof[:, idx, 0] = base1 + np.arange(NC*len(idx)).reshape(NC, len(idx))
            base1+=NC*len(idx)
        #print(np.max(cell2dof),base1)

        #corner point 处理
        self.Corner_coffefices_idx = np.zeros((self.N_cornerpoint,2),dtype=int) #记录角点切向张量基对应位置,同时将其当作固定边界条件处理
        self.Corner_coffefices2_idx = np.zeros((self.N_cornerpoint,4),dtype=int) #记录角点切向张量基与法向张量基的对应关系
        dofFlags_idx, = np.nonzero(dofFlags[0])
        #print(self.Corner_point_cell_index,dofFlags_idx)
        for i in range(self.N_cornerpoint):#角点特殊处理
            cell_idx = self.Corner_point_cell_index[i][1]
            cell2dof[cell_idx[0],dofFlags_idx[cell_idx[1]],:] = base1+np.arange(tdim)

            self.Corner_coffefices_idx[i,0] = self.Corner_point_index[i]*tdim
            self.Corner_coffefices_idx[i,1] = base1

            self.Corner_coffefices2_idx[i] = np.array([self.Corner_point_index[i]*tdim+1,
                                                        self.Corner_point_index[i]*tdim+2,
                                                                base1+1,base1+2],dtype=int)

            base1 += tdim
            #print(cell2dof[cell_idx[0],dofFlags_idx[cell_idx[1]],:])
            #print(self.Corner_point_index[i])

        self.cell2dof = cell2dof

        
    def init_face_to_dof(self):
        """
        构建局部自由度到全局自由度的映射矩阵
        Returns
        -------
        face2dof : ndarray with shape (NF, ldof,tdim)
            NF: 单元个数
            ldof: p 次标量空间局部自由度的个数
            tdim: 对称张量的维数
        """
        self.face2dof = np.copy(self.edge_to_dof())


    def init_edge_to_dof(self):
            mesh = self.mesh
            NN = mesh.number_of_nodes()
            NE = mesh.number_of_edges()
            #NC = mesh.number_of_cells()                    
            #gdim = self.geo_dimension()
            tdim = self.tensor_dimension() # 张量维数
            #p = self.p
            dof = self.dof # 标量空间自由度管理对象
            e2d = dof.edge_to_dof()[...,np.newaxis]
            ldof = dof.number_of_local_dofs(doftype='edge')
            edge2dof = np.zeros((NE,ldof,tdim),dtype=int)-1# 每个标量自由度变成 tdim 个自由度

            dofFlags = self.edge_dof_falgs_1() # 把不同类型的自由度区分开来
            idx, = np.nonzero(dofFlags[0])# 局部顶点自由度的编号
            edge2dof[:,idx,:] = tdim*e2d[:,idx] + np.arange(tdim)

            base0 = 0
            base1 = 0
            idx, = np.nonzero(dofFlags[1]) # 边内部自由度的编号
            if len(idx)>0:
                base0 += NN # 这是标量编号的新起点
                base1 += tdim*NN # 这是张量自由度编号的新起点
                #  0号局部自由度对应的是切向不连续的自由度, 此部分点乘外法向量为0,不算是边界，可以不用编号，已经被标记为-1
                edge2dof[:,idx,1:] = base1+(tdim-1)*(e2d[:, idx] - base0) + np.arange(tdim - 1)




            base1 = self.number_of_global_dofs()-tdim*self.N_cornerpoint
            idx, = np.nonzero(dofFlags[0])# 局部顶点自由度的编号
            for i in range(self.N_cornerpoint): #角点特殊处理
                edge_idx  = self.Corner_point_bdFace_index[i][1]
                edge2dof[edge_idx[0],idx[edge_idx[1]],:] = base1+np.arange(tdim)
                base1 +=tdim
                #print(edge2dof[edge_idx[0],idx[edge_idx[1]],:])

            self.edge2dof = edge2dof


    def geo_dimension(self):
        return self.dim


    def tensor_dimension(self):
        dim = self.dim
        return dim*(dim - 1)//2 + dim


    def interpolation_points(self):
        return self.dof.interpolation_points()


    def dof_flags(self):
        """ 对标量空间中的自由度进行分类, 分为边内部自由度, 面内部自由度(如果是三维空间的话)及其它自由度 

        Returns
        -------

        isOtherDof : ndarray, (ldof,)
            除了边内部和面内部自由度的其它自由度
        isEdgeDof : ndarray, (ldof, 3) or (ldof, 6) 
            每个边内部的自由度
        isFaceDof : ndarray, (ldof, 4)
            每个面内部的自由度
        -------

        """
        dim = self.geo_dimension()
        dof = self.dof 
        
        isPointDof = dof.is_on_node_local_dof()
        isEdgeDof = dof.is_on_edge_local_dof()
        isEdgeDof[isPointDof] = False
        
        isEdgeDof0 = np.sum(isEdgeDof, axis=-1) > 0 # 
        isOtherDof = (~isEdgeDof0) # 除了边内部自由度之外的其它自由度
                                   # dim = 2: 包括点和面内部自由度
        if dim == 2:
            return isOtherDof, isEdgeDof
        else:
            raise ValueError('`dim` should be 2 !')


    def dof_flags_1(self):
        """ 
        对标量空间中的自由度进行分类, 分为:
            点上的自由由度
            边内部的自由度
            面内部的自由度
            体内部的自由度

        Returns
        -------

        """
        gdim = self.geo_dimension() # the geometry space dimension
        dof = self.dof 
        isPointDof = dof.is_on_node_local_dof()
        isEdgeDof = dof.is_on_edge_local_dof()
        isEdgeDof[isPointDof] = False
        isEdgeDof0 = np.sum(isEdgeDof, axis=-1) > 0
        if gdim == 2:
            return isPointDof, isEdgeDof0, ~(isPointDof | isEdgeDof0)
        else:
            raise ValueError('`dim` should be 2!')


    def face_dof_falgs(self):
        """
        对标量空间中面上的基函数自由度进行分类，分为：
            点上的自由由度
            边内部的自由度
            面内部的自由度        
        """
        p = self.p
        gdim = self.geo_dimension()
        if gdim == 2:
            return self.edge_dof_falgs()
        else:
            raise ValueError('`dim` should be 2!')


    def face_dof_falgs_1(self):
        """
        对标量空间中面上的基函数自由度进行分类，分为：
            点上的自由由度
            边内部的自由度
            面内部的自由度        
        """
        p = self.p
        gdim = self.geo_dimension()
        if gdim == 2:
            return self.edge_dof_falgs_1()
        else:
            raise ValueError('`dim` should be 2!')


    def edge_dof_falgs(self):
        """
        对标量空间中边上的基函数自由度进行分类，分为：
            点上的自由由度 
        """ 
        p = self.p
        TD = 1
        multiIndex = self.space.multi_index_matrix[TD](p)#(ldof,2)
        isPointDof = (np.sum(multiIndex == p, axis=-1) > 0)    
        isEdgeDof0  = ~isPointDof
        return isPointDof, isEdgeDof0    


    def edge_dof_falgs_1(self):
        """
        对标量空间中边上的基函数自由度进行分类，分为：
            点上的自由由度 
        """ 
        p = self.p
        TD = 1
        multiIndex = self.space.multi_index_matrix[TD](p)#(ldof,2)
        isPointDof = (np.sum(multiIndex == p, axis=-1) > 0)    
        isEdgeDof0  = ~isPointDof
        return isPointDof, isEdgeDof0    


    @barycentric
    def face_basis(self,bc):
        gdim = self.geo_dimension()
        if gdim == 2:
            return self.edge_basis(bc)
        else:
            raise ValueError('`dim` should be 2!')


    @barycentric
    def edge_basis(self,bc):
        phi0 = self.space.face_basis(bc) #(NQ,1,ldof)       
        edge2dof = self.edge2dof #(NC,ldof,tdim)
        phi = np.einsum('nijk,...ni->...nijk',self.Tensor_Frame[edge2dof],phi0) #(NE,ldof,tdim,tdim), (NQ,1,ldof)
        #在不连续标架算出的结果不对，但是不影响，因为其自由度就是定义在单元体上的
        #不连续标架有:边内部第0个标架
        return phi #(NQ,NE,ldof,tdim,tdim)  


    @barycentric
    def basis(self, bc, index=np.s_[:],p=None):
        """
        Parameters
        ----------
        bc : ndarray with shape (NQ, dim+1)
            bc[i, :] is i-th quad point
        index : ndarray
            有时我我们只需要计算部分单元上的基函数
        Returns
        -------
        phi : ndarray with shape (NQ, NC, ldof, tdim, 3 or 6)
            NQ: 积分点个数
            NC: 单元个数
            ldof: 标量空间的单元自由度个数
            tdim: 对称张量的维数
        """
        #mesh = self.mesh

        #gdim = self.geo_dimension() 
        #tdim = self.tensor_dimension()
        phi0 = self.space.basis(bc,index=index,p=p) #(NQ,1,ldof)
        cell2dof = self.cell2dof[index] #(NC,ldof,tdim)

        phi = np.einsum('nijk,...ni->...nijk',self.Tensor_Frame[cell2dof],phi0) #(NC,ldof,tdim,tdim), (NQ,1,ldof)
        return phi  #(NQ,NC,ldof,tdim,tdim) 最后一个维度表示tensor


    @barycentric
    def div_basis(self, bc, index=np.s_[:]):
        #mesh = self.mesh
        #gdim = self.geo_dimension()
        tdim = self.tensor_dimension() 

        # the shape of `gphi` is (NQ, NC, ldof, gdim)

        gphi = self.space.grad_basis(bc, index=index) 
        cell2dof = self.cell2dof[index]
        shape = list(gphi.shape)
        shape.insert(-1, tdim)
        # the shape of `dphi` is (NQ, NC, ldof, tdim, gdim)

        VAL = np.einsum('iljk,kmn->iljmn',self.Tensor_Frame[cell2dof],self.T) #(NC,ldof,tdim,gdim,gdim)
        dphi = np.einsum('...ikm,ikjmn->...ikjn',gphi,VAL) #(NQ,NC,ldof,gdim), (NC,ldof,tdim,gdim,gdim)
        #print(dphi.shape)


        return dphi #(NQ,NC,ldof,tdim,gdim)


    @barycentric
    def value(self, uh, bc, index=np.s_[:]):
        phi = self.basis(bc, index=index)
        cell2dof = self.cell_to_dof()
        uh = uh[cell2dof[index]]
        val = np.einsum('...ijkm, ijk->...im', phi, uh) #(NQ,NC,tdim)
        val = np.einsum('...k, kmn->...mn', val, self.T)
        return val #(NQ,NC,gdim,gdim)


    @barycentric
    def div_value(self, uh, bc, index=np.s_[:]):
        dphi = self.div_basis(bc, index=index) #(NQ,NC,ldof,tdim,gdim)
        cell2dof = self.cell_to_dof()
        uh = uh[cell2dof[index]]
        val = np.einsum('...ijkm, ijk->...im', dphi, uh)
        return val #(NQ,NC,gdim)


    def compliance_tensor_matrix(self,mu=1,lam=1):
        ldof = self.number_of_local_dofs()
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        NQ = bcs.shape[0]

        phi = self.basis(bcs).reshape(NQ,NC,-1,tdim)#(NQ,NC,ldof,tdim)
        #compliance_tensor
        aphi = phi.copy()
        t = np.sum(aphi[..., 0:gdim], axis=-1)
        aphi[..., 0:gdim] -= lam/(2*mu+gdim*lam)*t[..., np.newaxis]
        aphi /= 2*mu

        #construct matrix
        d = np.array([1, 1, 2])
        M = np.einsum('i, ijkm, m, ijom, j->jko', ws, aphi, d, phi, self.mesh.entity_measure(), optimize=True)

        I = np.einsum('ij, k->ijk', self.cell2dof.reshape(NC,-1), np.ones(ldof))
        J = I.swapaxes(-1, -2)
        tgdof = self.number_of_global_dofs()

        M = csr_matrix((M.flat, (I.flat, J.flat)), shape=(tgdof, tgdof)).tolil()

        for i in range(self.N_cornerpoint):
            corner_point_idx = self.Corner_coffefices_idx[i]#(2,)
            corner_coffefices_idx = self.Corner_coffefices2_idx[i]#(4,)
            corner_coffefices = self.Corner_coffefices[i]#(2,4)
            M[corner_coffefices_idx] += np.transpose(corner_coffefices)@M[corner_point_idx]
            M[:,corner_coffefices_idx] += M[:,corner_point_idx]@corner_coffefices
            #print((np.transpose(corner_coffefices)@M[corner_point_idx]).shape)
        
        M = M.tocsr()



        return M


    def  parallel_compliance_tensor_matrix(self,mu=1,lam=1):
        tgdof = self.number_of_global_dofs()
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        Tmeasure = self.mesh.entity_measure()

        basis0 = self.basis
        cell2dof0 = self.cell_to_dof().reshape(NC,-1)
        gdof0 = tgdof

        gdof1 = gdof0

        # 对问题进行分割
        nc = mp.cpu_count()-2

        block = NC//nc
        r = NC%nc

        #print(NC,nc,block,r)

        index = np.full(nc+1,block)
        index[0] = 0
        index[1:r+1] +=1
        np.cumsum(index,out=index)

        M = csr_matrix((gdof0,gdof1))
        #M1 = csr_matrix((gdof0,gdof1))

        def f(i):
            s = slice(index[i],index[i+1])
            measure = Tmeasure[s]
            c2d0 = cell2dof0[s]
            c2d1 = c2d0
            shape = (len(measure),c2d0.shape[1],c2d1.shape[1]) #（lNC,ldof0,ldof1)
            lNC  = index[i+1]-index[i]

            d = np.array([1, 1, 2])
            Mi = np.zeros(shape,measure.dtype)
            for bc, w in zip(bcs,ws):
                phi0 = basis0(bc,index=s).reshape(lNC,-1,tdim) #(lNC,ldof,tdim)
                t = np.sum(phi0[...,:gdim],axis=-1)
                Mi +=np.einsum('jkl,l,jol,j->jko',phi0, d, phi0, w*measure)
                Mi -=lam*np.einsum('jk,jo,j->jko',t,t,w*measure)/(2*mu+lam*gdim)

            Mi /= 2*mu
            I = np.broadcast_to(c2d0[:, :, None], shape=Mi.shape)
            J = np.broadcast_to(c2d1[:, None, :], shape=Mi.shape)

            Mi = csr_matrix((Mi.flat, (I.flat, J.flat)), shape=(gdof0,gdof1))

            return Mi


    
        
        # 并行组装总矩阵
        with Pool(nc) as p:
            Mi= p.map(f, range(nc))

        for val in Mi:
            M += val

        M = M.tolil()
        for i in range(self.N_cornerpoint):
            corner_point_idx = self.Corner_coffefices_idx[i]#(2,)
            corner_coffefices_idx = self.Corner_coffefices2_idx[i]#(4,)
            corner_coffefices = self.Corner_coffefices[i]#(2,4)
            M[corner_coffefices_idx] += np.transpose(corner_coffefices)@M[corner_point_idx]
            M[:,corner_coffefices_idx] += M[:,corner_point_idx]@corner_coffefices
            #print((np.transpose(corner_coffefices)@M[corner_point_idx]).shape)
        
        M = M.tocsr()



        return M


    def div_matrix(self,vspace):
        '''

        Notes
        -----
        (div tau, v)

        gdim == 2
        v= [[phi,0],[0,phi]]

        [[B0],[B1]]

        '''
        tldof = self.number_of_local_dofs()
        vldof = vspace.number_of_local_dofs()
        tgdof = self.number_of_global_dofs()
        vgdof = vspace.number_of_global_dofs()
        
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        NQ = bcs.shape[0]

        dphi = self.div_basis(bcs).reshape(NQ,NC,-1,gdim) #(NQ, NC, ldof*tdim, gdim)
        vphi = vspace.basis(bcs)#(NQ,1,vldof)


        B0 = np.einsum('i,ijk,ijo,j->jko',ws,vphi,dphi[...,0],self.mesh.entity_measure(), optimize=True)
        B1 = np.einsum('i,ijk,ijo,j->jko',ws,vphi,dphi[...,1],self.mesh.entity_measure(), optimize=True)


        I = np.einsum('ij, k->ijk', vspace.cell_to_dof(), np.ones(tldof,dtype=int))
        J = np.einsum('ij, k->ikj', self.cell_to_dof().reshape(NC,-1), np.ones(vldof,dtype=int))   

        B0 = csr_matrix((B0.flat, (I.flat, J.flat)), shape=(vgdof, tgdof)).tolil()
        B1 = csr_matrix((B1.flat, (I.flat, J.flat)), shape=(vgdof, tgdof)).tolil()

        for i in range(self.N_cornerpoint):
            corner_point_idx = self.Corner_coffefices_idx[i]#(2,)
            corner_coffefices_idx = self.Corner_coffefices2_idx[i]#(4,)
            corner_coffefices = self.Corner_coffefices[i]#(2,4)

            B0[:,corner_coffefices_idx] += B0[:,corner_point_idx]@corner_coffefices
            B1[:,corner_coffefices_idx] += B1[:,corner_point_idx]@corner_coffefices

        B0 = B0.tocsr()
        B1 = B1.tocsr()


        return B0, B1


    def parallel_div_matrix(self,vspace):
        '''
        把网格中的单元分组，再分组组装相应的矩阵。对于三维大规模问题，如果同时计
        算所有单元的矩阵，占用内存会过多，效率过低。
        '''

        #tldof = self.number_of_local_dofs()
        #vldof = vspace.number_of_local_dofs()
        tgdof = self.number_of_global_dofs()
        vgdof = vspace.number_of_global_dofs()
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        Tmeasure = self.mesh.entity_measure()
        




        basis1 = self.div_basis
        cell2dof1 = self.cell_to_dof().reshape(NC,-1)
        gdof1 = tgdof

        basis0 = vspace.basis
        cell2dof0 = vspace.cell_to_dof()
        gdof0 = vgdof




        # 对问题进行分割
        nc = mp.cpu_count()-2

        block = NC//nc
        r = NC%nc

        #print(NC,nc,block,r)

        index = np.full(nc+1,block)
        index[0] = 0
        index[1:r+1] +=1
        np.cumsum(index,out=index)
        #print(index)

        B0 = csr_matrix((gdof0, gdof1))
        B1 = csr_matrix((gdof0, gdof1))

        def f(i):
            s = slice(index[i],index[i+1])
            measure = Tmeasure[s]
            c2d0 = cell2dof0[s]
            c2d1 = cell2dof1[s]

            shape = (len(measure),c2d0.shape[1],c2d1.shape[1]) #（lNC,ldof0,ldof1)
            lNC  = index[i+1]-index[i]
            M0 = np.zeros(shape,measure.dtype)
            M1 = np.zeros(shape,measure.dtype)
            for bc, w in zip(bcs, ws):
                phi0 = basis0(bc,index=s)#(1,vldof)
                phi1 = basis1(bc,index=s).reshape(lNC,-1,gdim)#(lNC, ldof*tdim, gdim)
                M0 += np.einsum('jk,jo,j->jko',phi0, phi1[...,0], w*measure)
                M1 += np.einsum('jk,jo,j->jko',phi0, phi1[...,1], w*measure)

            I = np.broadcast_to(c2d0[:, :, None], shape=M0.shape)
            J = np.broadcast_to(c2d1[:, None, :], shape=M0.shape)

            Bi0 = csr_matrix((M0.flat, (I.flat, J.flat)), shape=(gdof0, gdof1))
            Bi1 = csr_matrix((M1.flat, (I.flat, J.flat)), shape=(gdof0, gdof1))
            return Bi0,Bi1

        # 并行组装总矩阵

        with Pool(nc) as p:
            Bi= p.map(f, range(nc))
            
        for val in Bi:
            B0 += val[0]
            B1 += val[1]

        B0 = B0.tolil()
        B1 = B1.tolil()

        for i in range(self.N_cornerpoint):
            corner_point_idx = self.Corner_coffefices_idx[i]#(2,)
            corner_coffefices_idx = self.Corner_coffefices2_idx[i]#(4,)
            corner_coffefices = self.Corner_coffefices[i]#(2,4)
            B0[:,corner_coffefices_idx] += B0[:,corner_point_idx]@corner_coffefices
            B1[:,corner_coffefices_idx] += B1[:,corner_point_idx]@corner_coffefices

        B0 = B0.tocsr()
        B1 = B1.tocsr()

        

        return B0, B1



    def interpolation(self, u):
        ipoint = self.dof.interpolation_points()
        c2d = self.dof.cell2dof
        val = u(ipoint)[c2d][:,:,None] #u 是一个tensor val.shape = (NC,ldof,1,gdim,gidm)
        cell2dof = self.cell2dof #(NC,ldof,tdim)

        uI = Function(self)
        Tensor_Frame = np.einsum('...k,kmn->...mn',self.Tensor_Frame,self.T) #(gdof,gdim,gdim)
        uI[cell2dof] = np.einsum('...mn,...mn->...',Tensor_Frame[cell2dof],val)
        return uI


    def array(self, dim=None):
        gdof = self.number_of_global_dofs()
        return np.zeros(gdof, dtype=float)


    def function(self, dim=None):
        f = Function(self)
        return f


    def set_essential_bc(self, uh, gN, threshold=None):
        """
        初始化压力的本质边界条件，插值一个边界sigam,使得sigam*n=gN,对于角点，要小心选取标架
        由face2bddof 形状为(NFbd,ldof,tdim)
        2D case 时face2bddof[...,0]--切向标架， face2bddof[...,1]--法向标架， face2bddof[...,2]--切法向组合标架
        """
        mesh = self.mesh
        #gdim = self.geo_dimension()
        #tdim = self.tensor_dimension()
        ipoint = self.dof.interpolation_points()
        gdof = self.number_of_global_dofs()
        #node = mesh.entity('node')


        

        if type(threshold) is np.ndarray:
            index = threshold #此种情况后面补充
        else:
            if threshold is not None:
                index = mesh.ds.boundary_face_index()
                bc = mesh.entity_barycenter('face',index=index)
                flag = threshold(bc) #(2,gNEbd),第0行表示给的法向投影，第1行分量表示给的切向投影
                flag_idx = (np.sum(flag,axis=0)>0) #(gNFbd,)
                index = index[flag_idx] #(NFbd,)
                NFbd = len(index)

                bd_index_type = np.zeros((2,NFbd),dtype=np.bool)
                bd_index_type[0] = flag[0][flag_idx] #第0个分量表示给的法向投影
                bd_index_type[1] = flag[1][flag_idx] #第1个分量表示给的切向投影
                #print(bd_index_type)


        n = mesh.face_unit_normal()[index] #(NEbd,gdim)
        t = mesh.edge_unit_tangent()[index] #(NEbd,gdim)
        isBdDof = np.zeros(gdof,dtype=np.bool)#判断是否为固定顶点
        f2dbd = self.dof.face_to_dof()[index] #(NEbd,ldof)
        ipoint = ipoint[f2dbd] #(NEbd,ldof,gdim)
        facebd2dof = self.face2dof[index] #(NEbd,ldof,tdim)
        #print(f2dbd,index.shape,facebd2dof.shape)
        frame = np.zeros((NFbd,2,2),dtype=float)
        frame[:,0] = n
        frame[:,1] = t

        val = gN(ipoint,n[...,None,:],t=t[...,None,:]) #(NEbd,ldof,gdim)，可能是法向，也可能是切向，或者两者的线性组合

        #将边界边内部点与顶点分别处理


 
        self.set_essential_bc_inner_edge(facebd2dof,bd_index_type,frame,val,uh,isBdDof)#处理所有边界边内部点
        self.set_essential_bc_vertex(index,facebd2dof,bd_index_type,frame,val,uh,isBdDof)#处理所有边界边顶点

        return isBdDof

    def set_essential_bc_inner_edge(self,facebd2dof,bd_index_type,frame,val,uh,isBdDof):
        #处理所有边界边内部点
        inner_edge_dof_flag, = np.nonzero(self.face_dof_falgs_1()[1])
        val_temp = val[:,inner_edge_dof_flag] #(NFbd,edof,gdim)
        bdinedge2dof = facebd2dof[:,inner_edge_dof_flag,1:] #(NFbd,edof,tdim-1)
        bdTensor_Frame = self.Tensor_Frame[bdinedge2dof] #(NFbd,edof,2,tdim)
        n = frame[:,0]
        for i in range(2):
            bd_index_temp, = np.nonzero(bd_index_type[i])
            if len(bd_index_temp)>0:
                bdTensor_Frame_projection = np.einsum('ijl,lmn,in,im->ij',bdTensor_Frame[bd_index_temp,:,i,:],
                                                               self.T,frame[bd_index_temp,i],n[bd_index_temp])
                val_projection = np.einsum('ijk,ik->ij',val_temp[bd_index_temp],frame[bd_index_temp,i])
                uh[bdinedge2dof[bd_index_temp,:,i]] = val_projection/bdTensor_Frame_projection
                isBdDof[bdinedge2dof[bd_index_temp,:,i]] = True
                #print(uh[bdinedge2dof[bd_index_temp,:,i]])

    def set_essential_bc_vertex(self,index,facebd2dof,bd_index_type,frame,val,uh,isBdDof):

        NFbd = len(index)
        Corner_point_index = self.Corner_point_index #所有边界角点
        bdedge = self.mesh.entity('edge')[index]

        bdnode = np.unique(bdedge[:])

        bdiner_node = np.setdiff1d(bdnode,Corner_point_index)
        INNbd = len(bdiner_node) #边界内部顶点个数

        bdcorner_node = np.setdiff1d(Corner_point_index,bdnode)
        bdcorner_node = np.setdiff1d(Corner_point_index,bdcorner_node)
        
        

        #tdim = self.tensor_dimension()
        #gdim = self.geo_dimension()
        node_dof_flag, = np.nonzero(self.face_dof_falgs_1()[0])
        bd_val = val[:,node_dof_flag] #(NFbd,2,gdim)
        bdnode2dof = facebd2dof[:,node_dof_flag] #(NFbd,2,tdim)

        
        
        node2edge_idx = np.zeros((INNbd,2),dtype=int) #(INNbd,2)
        bd_dof = np.zeros((INNbd,2,2),dtype=int) #(INNbd,2,2)

        #####################################
        #边界顶点插值
        #print(bd_edge2node)
        bdedge = bdedge.T.reshape(-1)
        idx = bdiner_node[:,None]==bdedge

        node2edge_idx[:,0] = np.argwhere(idx[:,:NFbd])[:,1]  #(INNbd,2)
        node2edge_idx[:,1] = np.argwhere(idx[:,NFbd:])[:,1]
        #print(node2edge_idx)
        bd_dof[:,0] = bdnode2dof[node2edge_idx[:,0],0,1:] #(INNbd,2) 固定边界点的自由度
        bd_dof[:,1] = bdnode2dof[node2edge_idx[:,1],1,1:] #(INNbd,2) 固定边界点的自由度
        if len(bd_dof)>0:
            if np.max(np.abs(bd_dof[:,0]-bd_dof[:,1]))>1e-15:
                raise ValueError("Warn: The dof not right")
            else:
                bd_dof = bd_dof[:,0] #(INNbd,2)


            bdnode_index_type = bd_index_type[:,node2edge_idx] #(2,INNbd,2) 边界自由度类型

            val_temp = bd_val[node2edge_idx,[0,1]] #(INNbd,2,gdim)
            val_temp = np.einsum('ijk,ijlk->lji',val_temp,frame[node2edge_idx]) #(2,2,INNbd)

            for i in range(2):
                idx = bdnode_index_type[i] #(INNbd,2)
                idx_temp, = np.nonzero(np.sum(idx,axis=-1))
                if len(idx_temp)>0:
                    idx = idx[idx_temp]
                    if i == 0:
                        uh[bd_dof[idx_temp,i]] = np.einsum('ji,ij->i',val_temp[i][:,idx_temp],idx)/np.sum(idx,axis=-1)
                    else:
                        Tensor_Frame = self.Tensor_Frame[bd_dof[idx_temp,1]] #(True_INNbd,tdim) #可能差个负号，引入
                        n_temp = frame[node2edge_idx[idx_temp]][:,:,0] #(INNbd,2,gdim)
                        t_temp = frame[node2edge_idx[idx_temp]][:,:,1] #(INNbd,2,gdim)
                        #print(n_temp.shape,t_temp.shape)
                        Tnt = np.einsum('lk,kij,lsi,lsj->sl',Tensor_Frame,self.T,n_temp,t_temp) #(2,INNbd)
                        #print(Tnt.shape)
                        val_temp = val_temp[i][:,idx_temp] #(2,INNbd)
                        uh[bd_dof[idx_temp,i]] = np.einsum('ij,ji,ij->j',val_temp,idx,Tnt)/np.einsum('ij,ji,ij->j',Tnt,idx,Tnt)
        

                    isBdDof[bd_dof[idx_temp,i]] = True
            #######################################
            #边界角点插值
            #Corner_point_bdFace_index = self.Corner_point_bdFace_index
            CorNbd = len(bdcorner_node)
            #print(Corner_point_bdFace_index.shape)
            if CorNbd>0: 
                idx = (bdcorner_node[:,None] == bdedge)


                for i in range(CorNbd):
                    #若为第一个边界边第一个点
                    node2edge_idx, = np.nonzero(idx[i,:NFbd])
                    if len(node2edge_idx)>0:
                        Ncor = len(node2edge_idx)
                        bd_dof = bdnode2dof[node2edge_idx,0,1:] #(Ncor,2)
                        bdnode_index_type = bd_index_type[:,node2edge_idx] #(2,Ncor)
                        
                        val_temp = bd_val[node2edge_idx,0] #(Ncor,gdim)
                        val_temp = np.einsum('...k,...lk->l...',val_temp,frame[node2edge_idx]) #(2,Ncor)
                        val_temp[1,:] *= np.sqrt(2.0)
                        
                        for j in range(Ncor):
                            idx_temp, = np.nonzero(bdnode_index_type[:,j])
                            uh[bd_dof[j,idx_temp]] = val_temp[idx_temp,j]
                            isBdDof[bd_dof[j,idx_temp]] = True

                    #print(bd_val.shape)
                    #若为边界边第二个点
                    node2edge_idx, = np.nonzero(idx[i,NFbd:])
                    if len(node2edge_idx)>0:
                        Ncor = len(node2edge_idx)
                        bd_dof = bdnode2dof[node2edge_idx,1,1:] #(Ncor,2)
                        bdnode_index_type = bd_index_type[:,node2edge_idx] #(2,Ncor)
                        
                        val_temp = bd_val[node2edge_idx,1] #(Ncor,gdim)
                        val_temp = np.einsum('...k,...lk->l...',val_temp,frame[node2edge_idx]) #(2,Ncor)
                        val_temp[1,:] *= np.sqrt(2.0)
                        
                        for j in range(Ncor):
                            idx_temp, = np.nonzero(bdnode_index_type[:,j])
                            uh[bd_dof[j,idx_temp]] = val_temp[idx_temp,j]
                            isBdDof[bd_dof[j,idx_temp]] = True

        isBdDof[self.Corner_coffefices_idx] = True

        








    def set_nature_bc(self, gD, threshold=None, q=None):
        """
        设置 natural边界条件到右端项中，由于是混合元，故此时为u的gD自由边界条件
        若对应应力边界未设置，则默认该边界方向u为0
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        gdof = self.number_of_global_dofs()

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = self.mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = self.mesh.entity_barycenter('face',index=index)
                flag = threshold(bc)
                flag = (np.sum(flag,axis=0)>0)
                index = index[flag]
        
        bd2dof = self.edge_to_dof()[index] #(NEbd,ldof,tdim)

       
        n = mesh.face_unit_normal(index=index) #(NFbd,gdim)
        measure = mesh.entity_measure('face',index=index)
        qf = self.integralalg.faceintegrator if q is None else mesh.integrator(q, 'face')
        bcs, ws = qf.get_quadrature_points_and_weights()
        phi = self.face_basis(bcs)[:,index,...] #(NQ,NFbd,ldof,tdim,tdim)
        shape = list(phi.shape)
        shape[-1] = gdim
        phin = np.zeros(shape,dtype=float) #sigam*n, (NQ,NFbd,ldof,tdim,gdim)

        phin[...,0] = np.einsum('...ijlk,ik->...ijl',phi[...,[0,2]],n)
        phin[...,1] = np.einsum('...ijlk,ik->...ijl',phi[...,[2,1]],n)

        pp = mesh.bc_to_point(bcs,index=index)
        t = mesh.edge_unit_tangent(index=index)
        val = gD(pp,n=n,t=t) #(NQ,NFbd,gdim) 此时gD函数,可能给法向分量，也可能给切向分量，具体形式在gD中体现
        bb = np.einsum('m,mil,mijkl,i->ijk', ws, val, phin, measure) #(NFbd,ldof,tdim)
        idx = bd2dof>=0 #标记出连续边界
        F = np.zeros(gdof,dtype=float)
        np.add.at(F,bd2dof[idx],bb[idx])
        return F




if __name__ == '__main__':
    from fealpy.mesh import MeshFactory as mf
    from fealpy.fem.integral_alg import IntegralAlg
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from fealpy.functionspace import LagrangeFiniteElementSpace
    import numpy as np
    import sys
    p = int(sys.argv[1])
    #mesh = mf.one_triangle_mesh()
    #mesh = mf.boxmesh2d([0,1,0,1],nx=1,ny=1,meshtype='tri')
    mesh = mf.special_boxmesh2d([0,1,0,1],n=2,meshtype='cross')
    #mesh.uniform_refine(2)
    space = HuZhangFiniteElementSpace(mesh,p=p)
    vspace = LagrangeFiniteElementSpace(mesh,p=p-1,spacetype='D')
    #print(space.number_of_global_dofs())

    #fig = plt.figure()
    #axes = fig.gca()
    #mesh.add_plot(axes)
    #mesh.find_node(axes, showindex=False, fontsize=24)
    #mesh.find_edge(axes, showindex=False, fontsize=22)
    #mesh.find_cell(axes, showindex=False, fontsize=20)
    #plt.show()

    if False:
        #test corner point idx
        cell2dof = space.cell2dof
        face2dof = space.face2dof
        corner_point = space.Corner_point_index

        corner2cell = space.Corner_point_cell_index
        corner2face = space.Corner_point_bdFace_index

        dofFlags = space.dof_flags_1()
        dofcellFlags_idx, = np.nonzero(dofFlags[0])

        dofFlags = space.edge_dof_falgs_1() # 把不同类型的自由度区分开来
        doffaceFlags_idx, = np.nonzero(dofFlags[0])

        BDdof = space.Corner_coffefices

        for i in range(space.N_cornerpoint):
            cor2cell = corner2cell[i]
            cor2face = corner2face[i]
            #print(cell2dof[cor2cell[:,0],dofcellFlags_idx[cor2cell[:,1]]])
            print(face2dof[cor2face[:,0],doffaceFlags_idx[cor2face[:,1]]])
            print(BDdof[i])

    elif False:
        #test matrix
        M0 = space.compliance_tensor_matrix()
        M1 = space.parallel_compliance_tensor_matrix()

        print(np.max(np.abs(M0-M1)))
        B00,B01 = space.div_matrix(vspace)

        B10,B11 = space.parallel_div_matrix(vspace)

        print(np.max(np.abs(B00-B10)),np.max(np.abs(B01-B11)))


    


     
    







