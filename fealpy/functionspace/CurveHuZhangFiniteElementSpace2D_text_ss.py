import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool as Pool
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve, cg, lgmres, LinearOperator
import pyamg

from fealpy.mesh.core import multi_index_matrix
from fealpy.functionspace.Function import Function
from fealpy.functionspace import ParametricLagrangeFiniteElementSpace
from fealpy.functionspace.BarycentricSpace import BarycentricSpace
from fealpy.decorator import barycentric
from fealpy.quadrature.Gauss_type_quadrature import Gauss_type_quadrature
from fealpy.quadrature import FEMeshIntegralAlg
















class CurveHZ_CLagrangeTriangleSpace():
    '''
    Notes
    -----
    内部单元用p次lagrange元
    边界单元的边界边用p次元-----但选择Gauss点为插值点，此时为非协调元
    '''
    def __init__(self,mesh,p):
        self.p = p
        self.k = p

        self.mesh = mesh
        self.dim = mesh.geo_dimension()
        self.cellmeasure = mesh.entity_measure('cell')
        
        
        self.itype = mesh.itype
        self.ftype = mesh.ftype
        self.Gauss_type_quadrature = Gauss_type_quadrature

        self.init_multiIndex()
        self.init_index_node()
        self.init_edge_to_dof()
        self.init_cell_to_dof()
        self.init_boundary_edge_point()
        

        self.inner_cell_space = BarycentricSpace(mesh, self.inner_cell_multiIndex)
        self.inner_edge_space = BarycentricSpace(mesh, self.inner_edge_multiIndex)

        self.boundary_cell_space = BarycentricSpace(mesh, self.boundary_cell_multiIndex)
        self.boundary_edge_space = BarycentricSpace(mesh, self.boundary_edge_multiIndex)



        self.init_bdedge_basis_coefs()
        self.init_bdcell_basis_coefs()

        q = 2*p
        self.integralalg = FEMeshIntegralAlg(
                self.mesh, q,
                cellmeasure=self.cellmeasure)
        
        self.integrator = self.integralalg.integrator


        self.interpolation_points()

        


    
    def init_multiIndex(self):
        p = self.p
        k = self.k
        #内部单元和内部边
        self.inner_cell_multiIndex = multi_index_matrix[2](p)
        self.inner_edge_multiIndex = multi_index_matrix[1](p)


        #########################################
        #边界单元和边界边
        self.boundary_edge_multiIndex = multi_index_matrix[1](k)

        #边界单元的多重指标
        bdldof = self.number_of_local_dofs('cell',is_bd_dof=True)
        multiIndex_p = multi_index_matrix[2](p)
        boundary_cell_multiIndex = np.zeros((bdldof,3),dtype=np.int8)

        #p次元
        boundary_cell_multiIndex[:-(k+1)] = multiIndex_p[:-(p+1)]
        boundary_cell_multiIndex[-(k+1):,1:] = self.boundary_edge_multiIndex
        
        self.boundary_cell_multiIndex = boundary_cell_multiIndex



    def init_edge_to_dof(self):
        '''
        内部边有p+1个点
        边界边有p个点
        先排内部边，再排边界边
        '''
        p = self.p
        k = self.k
        mesh = self.mesh
        edge = mesh.entity('edge') #（NE,2）

        NN = self.NN
        NE = mesh.number_of_edges()


        boundary_edge_flag = mesh.ds.boundary_edge_flag()

        inner_edge_index, = np.nonzero(~boundary_edge_flag)
        boundary_edge_index, = np.nonzero(boundary_edge_flag)

        NEin = len(inner_edge_index)
        NEbd = len(boundary_edge_index)

        Inedge2dof = np.zeros((NEin,p+1),dtype=np.int_)


        if len(np.unique(edge[inner_edge_index].reshape(-1)))< mesh.number_of_nodes():
            raise ValueError('存在只依赖于边界边的点')
        Inedge2dof[:,[0,-1]] = self.index_node[edge[inner_edge_index]]


        

        if p > 1:
            Inedge2dof[:,1:-1] = NN + np.arange(NEin*(p-1)).reshape(NEin,p-1)



        Bdedge2dof = NN + NEin*(p-1) + np.arange(NEbd*(k+1)).reshape(NEbd,k+1) #只在边界内部定义


        self.Inedge2dof = Inedge2dof #(NEin,p+1)
        self.Bdedge2dof = Bdedge2dof #(NEbd,2*p)

 
    def init_cell_to_dof(self):
        '''
        内部单元为p次多项式
        边界单元为增广p次多项式
        规定每个边界单元只有一个边界边，且为第0条边
        '''
        p = self.p
        k = self.k
        mesh = self.mesh

        NC = mesh.number_of_cells()
        NE = mesh.number_of_edges()
        NN = self.NN

        boundary_edge_index = mesh.ds.boundary_edge_index()
        edge2cell = mesh.ds.edge_to_cell()[boundary_edge_index]
        NEbd = len(boundary_edge_index)
        if np.sum(edge2cell[:,2] == 0) < NEbd:
            raise ValueError('有边界边不为对应边界单元的第0条边')



        boundary_cell_flag = mesh.ds.boundary_cell_flag()

        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)

        NCin = len(inner_cell_index)
        NCbd = len(boundary_cell_index)

        ldof = self.number_of_local_dofs(doftype='cell')
        bdldof = self.number_of_local_dofs(doftype='cell',is_bd_dof=True)

        #内部单元

        boundary_edge_flag = mesh.ds.boundary_edge_flag()
        inner_edge_index, = np.nonzero(~boundary_edge_flag)
        
        NEin = len(inner_edge_index)
        NEbd = len(boundary_edge_index)


        ####################################################
        #内部边自由度安排
        cell2dof = np.zeros((NC,ldof),dtype=np.int_)
        edge2cell = mesh.ds.edge_to_cell()[inner_edge_index]
        index = self.inner_cell_multiIndex
        edge2dof = self.Inedge2dof
        cdof = ldof - 3*p 
        
        flag = edge2cell[:, 2] == 0
        cell2dof[edge2cell[flag, 0][:, None], index[:, 0] == 0] = edge2dof[flag]
        flag = edge2cell[:, 2] == 1
        cell2dof[edge2cell[flag, 0][:, None], index[:, 1] == 0] = edge2dof[flag, -1::-1]
        flag = edge2cell[:, 2] == 2
        cell2dof[edge2cell[flag, 0][:, None], index[:, 2] == 0] = edge2dof[flag]

        flag = (edge2cell[:, 3] == 0) & (edge2cell[:, 0] != edge2cell[:, 1])
        cell2dof[edge2cell[flag, 1][:, None], index[:, 0] == 0] = edge2dof[flag, -1::-1]
        flag = (edge2cell[:, 3] == 1) & (edge2cell[:, 0] != edge2cell[:, 1])
        cell2dof[edge2cell[flag, 1][:, None], index[:, 1] == 0] = edge2dof[flag]
        flag = (edge2cell[:, 3] == 2) & (edge2cell[:, 0] != edge2cell[:, 1])
        cell2dof[edge2cell[flag, 1][:, None], index[:, 2] == 0] = edge2dof[flag, -1::-1]


        if p > 2:
            flag = (index[:, 0] != 0) & (index[:, 1] != 0) & (index[:, 2] !=0)
            cell2dof[inner_cell_index[:,None],flag] = NN + NEin*(p-1) + NEbd*(k+1) + np.arange(NCin*cdof).reshape(NCin,cdof)
            cell2dof[boundary_cell_index[:,None],flag] = NN+NEin*(p-1)+NEbd*(k+1)+NCin*cdof+np.arange(NCbd*cdof).reshape(NCbd,cdof)



        
        self.Incell2dof = cell2dof[inner_cell_index]

        self.Bdcell2dof = np.zeros((NCbd,bdldof),dtype=np.int_)
        self.Bdcell2dof[:,:-(k+1)] = cell2dof[boundary_cell_index,:-(p+1)]

        

        

        ####################################################
        #边界边自由度安排
        
        edge2cell = mesh.ds.edge_to_cell()[boundary_edge_index]
        edge2dof = self.Bdedge2dof
        i,j = np.where(boundary_cell_index[:,None]==edge2cell[:,0])
        
        self.Bdcell2dof[:,-(k+1):] = edge2dof[j]




    def init_index_node(self):
        mesh = self.mesh
        boundary_node_index = mesh.ds.boundary_node_index()
        bdnode2cell = mesh.ds.node_to_cell()[boundary_node_index]
        index  = (np.sum(bdnode2cell,axis=-1) <= 2)

        index = np.where(index)[0]


        NN = mesh.number_of_nodes()

        index_node = np.ones(NN,dtype=np.bool8)

        index_node[boundary_node_index[index]] = False

        self.index_node = np.zeros(mesh.number_of_nodes(),dtype=np.int_) - 1 #-1表示没有算入自由度的号

        self.NN = np.sum(index_node)
        self.index_node[index_node] = np.arange(self.NN)




    def init_boundary_edge_point(self):
        #边界边上的积分点选取,也是插值点
        mesh = self.mesh
        k = self.k

        Gauss_type_quadrature = self.Gauss_type_quadrature

        boundary_edge_index = mesh.ds.boundary_edge_index()
        NEbd = len(boundary_edge_index)

        bdbcs = np.zeros((NEbd,k+1,2),dtype = np.float64)
        Weights = np.zeros((NEbd,k+1),dtype = np.float64)

        for i in range(NEbd):
            def W(x):
                x = np.array([x])
                shape = x.shape+(2,)
                bc = np.zeros(shape,dtype=np.float64)
                bc[...,0] = x
                bc[...,1] = 1.0-x
                J = mesh.jacobi_matrix(bc,index=[boundary_edge_index[i]])
                J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
                return J[0]

            Gaus_quad = Gauss_type_quadrature(W, k+1)
            #quadpts, weights = Gaus_quad.Gauss_Lobatto_Quadrature(k=k+1)
            quadpts, weights = Gaus_quad.Gauss_Quadrature(k=k+1)

            bdbcs[i,:,0] = 1.0 - quadpts
            bdbcs[i,:,1] = quadpts
            Weights[i] = weights[-1::-1]


        #也是积分点
        self.bdbcs = bdbcs #(NEbd,k+1,2)
        self.bdweights = Weights #(NEbd,k+1)
        #按照边界边来的
 

    def init_bdedge_basis_coefs(self):
        k = self.k #(NEbd,k+1)
        p = self.p 
        bdbcs = self.bdbcs
        phi0 = self.boundary_edge_space.basis(bdbcs)[...,0,:]#(NEbd,k+1,k+1)
        
        self.bdedge_coefs = np.linalg.inv(phi0)
        


    def init_bdcell_basis_coefs(self):
        mesh = self.mesh
        boundary_cell_index = mesh.ds.boundary_cell_index()
        bdedge2cell = mesh.ds.edge2cell[mesh.ds.boundary_edge_index()]
        i,j = np.where(boundary_cell_index[:,None]==bdedge2cell[:,0])
        self.bdcell_coefs = self.bdedge_coefs[j]





    @barycentric
    def edge_basis(self,bc,index=np.s_[:],is_bd_dof=False):
        #返回边上基函数，按照is_bd_dof来确定时返回边界边还是内部边

        if is_bd_dof:
            phi = self.boundary_edge_space.basis(bc) #(NQ,1,k+1)
            bdedge_coefs = self.bdedge_coefs[index] #(NEbd,k+1,k+1)
            phi = np.einsum('...ij,ijk->...ik',phi,bdedge_coefs) #(NQ,NEbd,k+1)
            return phi #(NQ,NEbd,k+1)

        else:
            phi = self.inner_edge_space.basis(bc) #(NQ,1,p+1)
            return phi #(NQ,1,p+1)


    @barycentric
    def basis(self,bc,index=np.s_[:],is_bd_dof=False):
        #返回单元的基函数，按照is_bd_dof来确定返回边界单元还是内部单元

        if is_bd_dof:
            p = self.p
            k = self.k


            bdcell_coefs = self.bdcell_coefs[index] #(NCbd,k+1,k+1)
            NCbd = bdcell_coefs.shape[0]

            bdldof = self.number_of_local_dofs(is_bd_dof=True)

            shape = bc.shape[:-1] + (NCbd,bdldof)
            phi = np.zeros(shape,dtype=np.float64)

            phi0 = self.boundary_cell_space.basis(bc) #(NQ,1,bdlodf)


            phi[...,:-(k+1)] = phi0[...,:-(k+1)] #(NQ,NCbd,bdldof)

            phi[...,-(k+1):] = np.einsum('...ij,ijk->...ik',phi0[...,-(k+1):],bdcell_coefs)

            
            
            return phi #(NQ,NCbd,bdldof)

            
        
        else:
            phi = self.inner_cell_space.basis(bc) #(NQ,1,ldof)
            return phi #(NQ,1,ldof)


    @barycentric
    def grad_basis(self,bc,index=np.s_[:],is_bd_dof=False):
        #返回单元的grad基函数，按照is_bd_dof来确定返回边界单元还是内部单元
        mesh = self.mesh
        boundary_cell_flag = mesh.ds.boundary_cell_flag()

        if is_bd_dof:
            k = self.k
            boundary_cell_index, = np.nonzero(boundary_cell_flag)
            boundary_cell_index = boundary_cell_index[index]

            bdcell_coefs = self.bdcell_coefs[index] #(NCbd,k+1,k+1)
            NCbd = len(boundary_cell_index)
            gphi = self.boundary_cell_space.grad_basis(bc,index=boundary_cell_index) #(NQ,NCbd,bdldof,gdim)

            gphi[...,-(k+1):,:] = np.einsum('...ijl,ijk->...ikl',gphi[...,-(k+1):,:],bdcell_coefs)

            return gphi #(NQ,NCbd,bdldof,gdim)

        
        else:
            inner_cell_index, = np.nonzero(~boundary_cell_flag)
            inner_cell_index = inner_cell_index[index]

            gphi = self.inner_cell_space.grad_basis(bc,index=inner_cell_index)

            return gphi #(NQ,NCin,ldof,gdim)


    def interpolation_points(self):
        mesh = self.mesh
        p = self.p
        k = self.k
        gdof = self.number_of_global_dofs()
        ipoint = np.zeros((gdof,2),dtype=np.float64)

        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)

        boundary_edge_flag = mesh.ds.boundary_edge_flag()
        inner_edge_index, = np.nonzero(~boundary_edge_flag)
        boundary_edge_index, = np.nonzero(boundary_edge_flag)

        #内部单元内部插值点
        idx, = np.nonzero(np.prod(self.inner_cell_multiIndex>0,axis=-1))
        bc = self.inner_cell_multiIndex[idx]
        bc = bc/(np.sum(bc,axis=-1)[:,None])
        ipoint[self.Incell2dof[:,idx]] = mesh.bc_to_point(bc,index=inner_cell_index).swapaxes(0,1)


        #边界单元内部插值点
        idx, = np.nonzero(np.prod(self.boundary_cell_multiIndex>0,axis=-1))
        bc = self.boundary_cell_multiIndex[idx]
        bc = bc/(np.sum(bc,axis=-1)[:,None])
        ipoint[self.Bdcell2dof[:,idx]] = mesh.bc_to_point(bc,index=boundary_cell_index).swapaxes(0,1)


        #内部边插值点
        bc = self.inner_edge_multiIndex
        bc = bc/(np.sum(bc,axis=-1)[:,None])
        pp = (mesh.bc_to_point(bc,index=inner_edge_index).swapaxes(0,1)).reshape(-1,2)
        Inedge2dof = self.Inedge2dof.reshape(-1)

        idx_temp, = np.nonzero(Inedge2dof >= 0)

        ipoint[Inedge2dof[idx_temp]] = pp[idx_temp]

        #边界边插值点
        NEbd = len(boundary_edge_index)
        if NEbd > 0:
            for i in range(NEbd):
                ipoint[self.Bdedge2dof[i]] = mesh.bc_to_point(self.bdbcs[i],index=[boundary_edge_index[i]])[...,0,:]

        return ipoint


    def number_of_global_dofs(self):
        p = self.p
        k = self.k
        mesh = self.mesh


        NC = mesh.number_of_cells()
        NE = mesh.number_of_edges()
        NN = np.sum(self.NN)

        NEbd = len(mesh.ds.boundary_node_index())
        NEin = NE - NEbd

        NCbd = len(mesh.ds.boundary_cell_index())
        NCin = NC - NCbd

        gdof = NN  #顶点
        #print(gdof)

        gdof += (p-1)*NEin #内部边
        #print(gdof)

        gdof += (k+1)*NEbd #边界边
        #print(gdof)

        gdof += (p-1)*(p-2)//2*NCin #内部单元
        #print(gdof)

        gdof += (p-1)*(p-2)//2*NCbd #边界单元
        #print(gdof)

        #print(NCbd,NCin,p,k,(p-1)*(p-2)//2*NCin + (k-1)*(k-2)//2*NCbd)
        return gdof
        
    def number_of_local_dofs(self, doftype='cell',is_bd_dof=False):
        p = self.p
        k = self.k
        if is_bd_dof:
            if doftype in {'cell', 2}:
                return 3 + 2*(p-1) + (k-1) + (p-1)*(p-2)//2
            elif doftype in {'face', 'edge', 1}:
                return k + 1
            elif doftype in {'node', 0}:
                return 1

        else:
            if doftype in {'cell', 2}:
                return (p+1)*(p+2)//2 
            elif doftype in {'face', 'edge',  1}:
                return self.p + 1
            elif doftype in {'node', 0}:
                return 1            
        
    @barycentric
    def value(self, uh, bc, index=np.s_[:]):
        mesh = self.mesh
        NC = mesh.number_of_cells()
        index = np.arange(NC)[index]
        NC = len(index)



        shape = bc.shape[:-1]+(NC,) #(NQ,NC)

        val = np.zeros(shape,dtype=np.float64) #(NQ,NC)

        boundary_cell_flag = mesh.ds.boundary_cell_flag()

        ####################################################
        ################内部单元###############
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        i,j = np.where(index[:,None]==inner_cell_index)

        if len(j) > 0:
            cell2dof = self.Incell2dof[j] #(NC,ldof)
            phi = self.basis(bc,index=j) #(NQ,1,ldof)
            val[...,i] = np.einsum('...ij,ij->...i',phi,uh[cell2dof])


        ####################################################
        ################边界单元###############
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        i,j = np.where(index[:,None]==boundary_cell_index)

        if len(j) > 0:
            cell2dof = self.Bdcell2dof[j] #(NC,bdldof)
            phi = self.basis(bc,index=j,is_bd_dof=True) #(NQ,1,bdldof)
            val[...,i] = np.einsum('...ij,ij->...i',phi,uh[cell2dof])


        return val #(NQ,NC)


        
    @barycentric
    def grad_value(self, uh, bc, index=np.s_[:]):
        mesh = self.mesh
        NC = mesh.number_of_cells()
        index = np.arange(NC)[index]
        NC = len(index)
        gdim = self.dim

        shape = bc.shape[:-1] + (NC,gdim)

        val = np.zeros(shape,dtype=np.float64) #(NQ,NC,gdim)

        boundary_cell_flag = mesh.ds.boundary_cell_flag()

        ####################################################
        ################内部单元###############
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        i,j = np.where(index[:,None]==inner_cell_index) 

        if len(j) > 0:
            cell2dof = self.Incell2dof[j] #(NC,ldof)
            gphi = self.grad_basis(bc,index=j) #(NQ,NC,ldof,gdim)
            val[...,i,:] = np.einsum('...ijk,ij->...ik',gphi,uh[cell2dof])

        
        ####################################################
        ################边界单元###############
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        i,j = np.where(index[:,None]==boundary_cell_index)

        if len(j) > 0:
            cell2dof = self.Bdcell2dof[j] #(NC,ldof)
            gphi = self.grad_basis(bc,index=j,is_bd_dof=True) #(NQ,NC,bdldof,gdim)
            val[...,i,:] = np.einsum('...ijk,ij->...ik',gphi,uh[cell2dof])

        return val #(NQ,NC,gdim)



    def interpolation(self,u):
        ipoint = self.interpolation_points()
        uI = u(ipoint)
        return self.function(array=uI)


    def function(self, dim=None, array=None):
        f = Function(self, dim=dim, array=array, coordtype='barycentric')
        return f
        

        


        































class CurveHuZhangFiniteElementSpace_text_ss():
    """
    Curve Hu-Zhang Mixed Finite Element Space 2D, 假设曲边充分光滑，没有角点
    """
    def __init__(self, mesh, p, q=None):
        self.space = CurveHZ_CLagrangeTriangleSpace(mesh,p) # the scalar space
        k = self.space.k
        self.mesh = mesh
        self.p = p
        self.k = k
        self.dof = self.space
        self.dim = self.space.dim


        self.in_edof = (p-1)
        self.bd_edof = k+1

        self.in_cdof = (p-1)*(p-2)//2
        self.bd_cdof = (p-1)*(p-2)//2


        self.init_cell_to_dof()
        self.init_edge_to_dof()
        
        self.init_orth_matrices()
        self.integralalg = self.space.integralalg
        self.integrator = self.integralalg.integrator

    def init_orth_matrices(self):
        """
        Initialize the othogonal symetric matrix basis.
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()
        gdof = self.number_of_global_dofs()
        in_edof = self.in_edof
        bd_edof = self.bd_edof
        in_cdof = self.in_cdof
        bd_cdof = self.bd_cdof
        dof = self.dof
        p = self.p
        k = self.k
        self.Tensor_Frame = np.zeros((gdof,tdim),dtype=np.float_) #self.Tensor_Frame[i,:]表示第i个基函数的标架


        NEin = self.Inedge2dof.shape[0]
        NEbd = self.Bdedge2dof.shape[0]



        idx = np.array([(0, 0), (1, 1), (0, 1)])
        self.T = np.array([[(1, 0), (0, 0)], [(0, 0), (0, 1)], [(0, 1), (1, 0)]])


        #边上标架
        boundary_edge_flag = mesh.ds.boundary_edge_flag()
        inner_edge_index, = np.nonzero(~boundary_edge_flag)
        boundary_edge_index, = np.nonzero(boundary_edge_flag)
        #内部边
        if in_edof > 0:
            In_TE = np.zeros((in_edof,NEin,gdim),dtype=np.float_)
            bc = self.dof.inner_edge_multiIndex[1:-1]
            bc = bc/(np.sum(bc,axis=-1)[:,None])

            In_TE = self.edge_orth_tensor(bc,index=inner_edge_index)
            In_TE = In_TE.swapaxes(0,1) #(NEin,in_edof,3,3)

        #边界边
        if bd_edof > 0:
            Bd_TE = np.zeros((bd_edof,NEbd,tdim,tdim),dtype=np.float_)
            for i in range(NEbd):
                bc = dof.bdbcs[i] #(edof,2)
                Bd_TE[:,i] = self.edge_orth_tensor(bc,index=[boundary_edge_index[i]])[:,0]
            Bd_TE = Bd_TE.swapaxes(0, 1) #(NEbd,bd_edof,3,3)
        

        base0 = 0

        #顶点标架
        T = np.eye(tdim,dtype=np.float_)
        T[gdim:] = T[gdim:]/np.sqrt(2)
        NN = self.space.NN
        shape = (NN,tdim,tdim)
        self.Tensor_Frame[:NN*tdim] = np.broadcast_to(T[None,:,:],shape).reshape(-1,tdim) #顶点标架
        base0 += tdim*NN

        #边内部连续标架
        if in_edof > 0: #内部边内部连续自由度标架
            self.Tensor_Frame[base0:base0+NEin*in_edof*(tdim-1)] = In_TE[...,1:,:].reshape(-1,tdim)
            base0 += NEin*in_edof*(tdim-1)

        if bd_edof > 0: #边界边内部连续自由度
            self.Tensor_Frame[base0:base0+NEbd*bd_edof*(tdim-1)] = Bd_TE[...,1:,:].reshape(-1,tdim)
            base0 += NEbd*bd_edof*(tdim-1)


        
        #单元内部标架
        NCin = self.Incell2dof.shape[0]
        NCbd = self.Bdcell2dof.shape[0]
        if in_cdof > 0:
            shape = (NCin,in_cdof,tdim,tdim)
            self.Tensor_Frame[base0:base0+NCin*in_cdof*tdim] = np.broadcast_to(T[None,None,:,:],shape).reshape(-1,tdim) #内部单元内部标架
            base0 += NCin*in_cdof*tdim

        if bd_cdof > 0:
            shape = (NCbd,bd_cdof,tdim,tdim)
            self.Tensor_Frame[base0:base0+NCbd*bd_cdof*tdim] = np.broadcast_to(T[None,None,:,:],shape).reshape(-1,tdim) #边界单元内部标架
            base0 += NCbd*bd_cdof*tdim



        if in_edof > 0: #内部边的不连续标价
            E = (gdim+1)*(gdim)//2
            In_TE = In_TE[:,:,0].reshape(-1,tdim) #(NEin*in_edof,tdim)

            in_multiIndex = dof.inner_cell_multiIndex

            in_node_idx, = np.nonzero(np.sum(in_multiIndex==p,axis=-1))
            in_edge_idx = np.sum(in_multiIndex == 0,axis=-1)
            in_edge_idx[in_node_idx] = False
            in_edge_idx, = np.nonzero(in_edge_idx)

            c2de = self.space.Incell2dof[:,in_edge_idx] - NN #(NCin,E*in_edof)

            self.Tensor_Frame[base0:base0+NCin*E*in_edof] = In_TE[c2de].reshape(-1,tdim)

            base0 += NCin*E*in_edof


            #内部边在边界单元
            bd_multiIndex = dof.boundary_cell_multiIndex

            bd_node_idx, = np.nonzero(np.sum(bd_multiIndex==p,axis=-1))
            bd_edge_idx = np.sum(bd_multiIndex[:,1:] ==0, axis=-1)
            bd_edge_idx[bd_node_idx] = False
            bd_edge_idx, = np.nonzero(bd_edge_idx)

            

            c2de = self.space.Bdcell2dof[:,bd_edge_idx] - NN #(NCbd,(E-1)*in_edof)

            TE = np.zeros((NCbd,(E-1)*in_edof+bd_edof,tdim),dtype=np.float_)
            TE[:,:(E-1)*in_edof] = In_TE[c2de]



 

        
        if bd_edof > 0: #边界边的不连续自由度
            Bd_TE = Bd_TE[:,:,0].reshape(-1,tdim) #(NEbd*bd_edof,tdim)
            bd_multiIndex = dof.boundary_cell_multiIndex

            bd_edge_idx = (bd_multiIndex[:,0] == 0)
            bd_edge_idx, = np.nonzero(bd_edge_idx)

            c2de = self.space.Bdcell2dof[:,bd_edge_idx] - NN - NEin*in_edof #（NCbd,bd_edof）

            TE[:,(E-1)*in_edof:] = Bd_TE[c2de]



            self.Tensor_Frame[base0:base0+NCbd*((E-1)*in_edof+bd_edof)] = TE.reshape(-1,tdim)
            base0 += NCbd*((E-1)*in_edof+bd_edof)

        
        '''
        #边界顶点，切法向是固定的
        boundary_edge_index = mesh.ds.boundary_edge_index()
        bdedge = mesh.entity('edge')[boundary_edge_index]
        bc = np.array([[1,0],[0,1]],dtype=np.float_)
        bdTE = self.edge_orth_tensor(bc,index=boundary_edge_index) #(2,NEbd,tdim,tdim)

        for i in range(2):
            bdedge_idx = tdim*bdedge[:,i,None]+np.arange(tdim) #(NEbd,tdim)
            self.Tensor_Frame[bdedge_idx] = bdTE[i]
        '''







    def edge_orth_tensor(self,bc,index=np.s_[:]):
        #构造边上的张量基
        #bc.shape = (NQ,2)
        mesh = self.mesh
        tdim = self.tensor_dimension()
        t = mesh.edge_unit_tangent(bc,index=index) #(NQ,NEbd,2)
        n = mesh.edge_unit_normal(bc,index=index) #(NQ,NEbd,2)
        shape = t.shape[:-1]+(tdim,tdim)  #(NQ,NEbd,tdim,tdim)
        Tensor_Frame = np.zeros(shape,dtype=np.float64)

        idx = np.array([[0,0],[1,1],[0,1]])
        Tensor_Frame[...,0,:] = np.prod(t[...,idx],axis=-1)
        Tensor_Frame[...,1,:] = np.prod(n[...,idx],axis=-1)
        Tensor_Frame[...,2,:] = (t[...,idx[:,0]]*n[...,idx[:,1]] + t[...,idx[:,1]]*n[...,idx[:,0]])/np.sqrt(2)

        return Tensor_Frame #(NQ,NEbd,tdim,tdim)

      
    def __str__(self):
        return "Curve Hu-Zhang mixed finite element space 2D!"

    def number_of_global_dofs(self):
        """
        """
        p = self.p
        k = self.k
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()

        mesh = self.mesh

        NC = mesh.number_of_cells()
        NE = mesh.number_of_edges()
        NN = self.space.NN

        NEbd = len(mesh.ds.boundary_node_index())
        NEin = NE - NEbd

        NCbd = len(mesh.ds.boundary_cell_index())
        NCin = NC - NCbd
        


        gdof = tdim*NN

        if p > 1:
            in_edof = self.in_edof
            bd_edof = self.bd_edof

            gdof += (tdim-1)*in_edof*NEin # 内部边内部连续自由度的个数
            gdof += (tdim-1)*bd_edof*NEbd # 边界边内部联系自由度的个数 
            E = mesh.number_of_edges_of_cells() # 单元边的个数
            gdof += NCin*E*in_edof #内部单元的边内不连续自由度的个数
            gdof += NCbd*((E-1)*in_edof+bd_edof) #边界单元的边内部不连续自由度个数，只有一条边界边

        if p > 2:
            in_cdof = self.in_cdof
            bd_cdof = self.bd_cdof

            gdof += tdim*in_cdof*NCin #内部单元内部自由度个数
            gdof += tdim*bd_cdof*NCbd #边界单元内部自由度个数

        return gdof 

    def number_of_local_dofs(self,is_bd_dof=False):
        ldof = self.dof.number_of_local_dofs(is_bd_dof=is_bd_dof)
        tdim = self.tensor_dimension()
        return ldof*tdim

    def cell_to_dof(self,is_bd_dof=False):
        if is_bd_dof:
            return self.Bdcell2dof
        else:
            return self.Incell2dof

    def edge_to_dof(self,is_bd_dof=False):
        if is_bd_dof:
            return self.Bdedge2dof
        else:
            return self.Inedge2dof


    def init_cell_to_dof(self):
        """
        构建局部自由度到全局自由度的映射矩阵

        Returns
        -------
        Incell2dof : ndarray with shape (NCin, ldof,tdim)
        Bdcell2dof : ndarray with shape (NCbd, bdldof,tdim)
            NC: 单元个数
            ldof: p 次标量空间局部自由度的个数
            tdim: 对称张量的维数
        """
        mesh = self.mesh
        NN = self.space.NN
        NE = mesh.number_of_edges()
        NC = mesh.number_of_cells()

        gdim = self.geo_dimension()
        tdim = self.tensor_dimension() # 张量维数
        p = self.p
        k = self.k
        dof = self.dof # 标量空间自由度管理对象 
       
        inc2d = dof.Incell2dof
        bdc2d = dof.Bdcell2dof


        shape = inc2d.shape+(tdim,)
        Incell2dof = np.zeros(shape,dtype=np.int_) #(NCin, ldof*tdim)

        shape = bdc2d.shape+(tdim,)
        Bdcell2dof = np.zeros(shape,dtype=np.int_) #(NCbd, bdldof,tdim)

        inc2d = inc2d[...,None]
        bdc2d = bdc2d[...,None]


        base0 = 0
        base1 = 0

        in_multiIndex = dof.inner_cell_multiIndex
        bd_multiIndex = dof.boundary_cell_multiIndex

        #顶点自由度
        in_node_idx, = np.nonzero(np.sum(in_multiIndex==p,axis=-1)) #内部单元顶点
        Incell2dof[:,in_node_idx,:] = tdim*inc2d[:,in_node_idx,:] + np.arange(tdim)

        bd_node_idx, = np.nonzero((np.sum(bd_multiIndex==p,axis=-1))[:-(k+1)])#边界单元顶点
        Bdcell2dof[:,bd_node_idx,:] = tdim*bdc2d[:,bd_node_idx,:] + np.arange(tdim)

    
        base0 += NN
        base1 += NN*tdim
        #边内部连续自由度
        if p > 1:
            #内部单元
            in_edge_idx = np.sum(in_multiIndex == 0,axis=-1)
            in_edge_idx[in_node_idx] = False
            in_edge_idx, = np.nonzero(in_edge_idx)

            Incell2dof[:,in_edge_idx,1:] = base1 + (tdim-1)*(inc2d[:,in_edge_idx,:] - base0) + np.arange(tdim-1)

            #边界单元
            bd_edge_idx = np.sum(bd_multiIndex == 0,axis=-1)
            bd_edge_idx[bd_node_idx] = False
            bd_edge_idx, = np.nonzero(bd_edge_idx)

            Bdcell2dof[:,bd_edge_idx,1:] = base1 + (tdim-1)*(bdc2d[:,bd_edge_idx,:] - base0) + np.arange(tdim-1)

            in_edof = self.in_edof
            bd_edof = self.bd_edof
            NEin = dof.Inedge2dof.shape[0]
            NEbd = dof.Bdedge2dof.shape[0]
            base0 += NEin*in_edof + NEbd*bd_edof
            base1 += NEin*in_edof*(tdim-1) + NEbd*bd_edof*(tdim-1)

        #单元内部自由度
        if p > 2:
            #内部单元
            in_cell_idx = np.nonzero(np.prod(in_multiIndex > 0,axis=-1))
            Incell2dof[:,in_cell_idx,:] = base1 + tdim*(inc2d[:,in_cell_idx,:]-base0) + np.arange(tdim)

            #边界单元
            bd_cell_idx = np.nonzero(np.prod(bd_multiIndex > 0, axis=-1))
            Bdcell2dof[:,bd_cell_idx,:] = base1 + tdim*(bdc2d[:,bd_cell_idx,:]-base0) + np.arange(tdim)

            NCin = dof.Incell2dof.shape[0]
            NCbd = dof.Bdcell2dof.shape[0]
            in_cdof = self.in_cdof
            bd_cdof = self.bd_cdof
            
            base1 += NCin*in_cdof*tdim + NCbd*bd_cdof*tdim


        if p > 1:
            #边界边不连续自由度编号
            Incell2dof[:,in_edge_idx,0] = base1 + np.arange(NCin*len(in_edge_idx)).reshape(NCin,len(in_edge_idx))

            base1 += NCin*len(in_edge_idx)

            Bdcell2dof[:,bd_edge_idx,0] = base1 + np.arange(NCbd*len(bd_edge_idx)).reshape(NCbd,len(bd_edge_idx))

            base1 += NCbd*len(bd_edge_idx)


        self.Incell2dof = Incell2dof
        self.Bdcell2dof = Bdcell2dof


    def init_edge_to_dof(self):
            mesh = self.mesh
            NN = self.space.NN
            NE = mesh.number_of_edges()
            NC = mesh.number_of_cells()                    
            gdim = self.geo_dimension()
            tdim = self.tensor_dimension() # 张量维数


            p = self.p
            k = self.k
            dof = self.dof # 标量空间自由度管理对象


            
            in_e2d = dof.Inedge2dof[...,None] #(NEin,ldof,1)
            bd_e2d = dof.Bdedge2dof[...,None] #(NEbd,bdldof,1)

            NEin = in_e2d.shape[0]
            NEbd = bd_e2d.shape[0]

            ldof = in_e2d.shape[1]
            bdldof = bd_e2d.shape[1]




            In_edge2dof = np.zeros((NEin,ldof,tdim),dtype=np.int_)-1# 内部边每个标量自由度变成 tdim 个自由度
            Bd_edge2dof = np.zeros((NEbd,bdldof,tdim),dtype=np.int_) #边界边每个标量自由度变成 tdim 个自由度


            #顶点标量自由度变成张量自由度
            idx = [0,-1]
            In_edge2dof[:,idx,:] = tdim*in_e2d[:,idx,:] + np.arange(tdim)

            #内部自由度
            base0 = 0
            base1 = 0
            if p > 1:
                base0 += NN # 这是标量编号的新起点
                base1 += tdim*NN # 这是张量自由度编号的新起点
                #0号局部自由度对应的是切向不连续的自由度, 内部边不确定，用-1表示，边界边可以确定

                In_edge2dof[:,1:-1,1:] = base1+(tdim-1)*(in_e2d[:,1:-1,:]-base0) + np.arange(tdim-1)
                Bd_edge2dof[:,:,1:] = base1+(tdim-1)*(bd_e2d[:,:,:]-base0) + np.arange(tdim-1)

                ## TODO
                #边界边0号自由度

                boundary_cell_index = mesh.ds.boundary_cell_index()
                bdedge2cell = mesh.ds.edge_to_cell()[mesh.ds.boundary_edge_index()]

                i,j = np.where(bdedge2cell[:,0][:,None]==boundary_cell_index)

                Bd_edge2dof[:,:,0] = self.Bdcell2dof[j,-(k+1):,0]
                

            


            self.Inedge2dof = In_edge2dof
            self.Bdedge2dof = Bd_edge2dof



    def geo_dimension(self):
        return self.dim

    def tensor_dimension(self):
        dim = self.dim
        return dim*(dim - 1)//2 + dim

    def interpolation_points(self):
        return self.dof.interpolation_points()


    
    @barycentric
    def edge_basis(self,bc,index=np.s_[:],is_bd_dof=False):
        phi0 = self.space.edge_basis(bc,index=index,is_bd_dof=is_bd_dof) #(NQ,NEbd,ldof) or (NQ,1,ldof)      
        if is_bd_dof:
            edge2dof = self.Bdedge2dof[index] #(NEbd,ldof,tdim)        
        else:
            edge2dof = self.Inedge2dof[index] #(NEin,ldof,tdim)

        phi = np.einsum('...ij,ijkl->...ijkl',phi0,self.Tensor_Frame[edge2dof]) #(NQ,NE,ldof,tdim,tdim)
        
        #在不连续标架算出的结果不对，但是不影响，因为其自由度就是定义在单元体上的
        #不连续标架有:边内部第0个标架
            
        return phi #(NQ,NE,ldof,tdim,tdim)  


    @barycentric
    def basis(self, bc, index=np.s_[:],is_bd_dof=False):
        """
        Parameters
        ----------
        bc : ndarray with shape (NQ, dim+1)
            bc[i, :] is i-th quad point
        index : ndarray
            有时我我们只需要计算部分单元上的基函数
        Returns
        -------
        phi : ndarray with shape (NQ, NC, ldof, tdim, 3 or 6)
            NQ: 积分点个数
            NC: 单元个数
            ldof: 标量空间的单元自由度个数
            tdim: 对称张量的维数
        """

        phi0 = self.space.basis(bc,index=index,is_bd_dof=is_bd_dof) #(NQ,NCbd,ldof) or (NQ,1,ldof)

        if is_bd_dof:
            cell2dof = self.Bdcell2dof[index] #(NCbd,ldof,tdim)
        else:
            cell2dof = self.Incell2dof[index] #(NCin,ldof,tdim)

        phi = np.einsum('...ni,nijk->...nijk',phi0,self.Tensor_Frame[cell2dof]) #(NQ,1,ldof),(NC,ldof,tdim,tdim)
        
                    
        return phi  #(NQ,NC,ldof,tdim,tdim) 最后一个维度表示tensor



    @barycentric
    def div_basis(self, bc, index=np.s_[:],is_bd_dof=False):

        gphi = self.space.grad_basis(bc,index=index,is_bd_dof=is_bd_dof) # the shape of `gphi` is (NQ, NC, ldof, gdim)
        tdim = self.tensor_dimension()
        
        if is_bd_dof:
            cell2dof = self.Bdcell2dof[index] #(NCbd,ldof)
        else:
            cell2dof = self.Incell2dof[index] #(NCin,ldof)

        shape = list(gphi.shape)
        shape.insert(-1, tdim)
        # the shape of `dphi` is (NQ, NC, ldof, tdim, gdim)

        VAL = np.einsum('iljk,kmn->iljmn',self.Tensor_Frame[cell2dof],self.T) #(NC,ldof,tdim,gdim,gdim)
        dphi = np.einsum('...ikm,ikjmn->...ikjn',gphi,VAL) #(NQ,NC,ldof,gdim), (NC,ldof,tdim,gdim,gdim)


        return dphi #(NQ,NC,ldof,tdim,gdim)



    @barycentric
    def value(self, uh, bc, index=np.s_[:]):
        NC = self.mesh.number_of_cells()
        index = np.arange(NC)[index]
        tdim = self.tensor_dimension()
        NC = len(index)
        shape = bc.shape[:-1]+(NC,tdim)
        val = np.zeros(shape,dtype=np.float64)

        boundary_cell_flag = self.mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)

        
        i,j = np.where(index[:,None]==inner_cell_index)
        if len(j)>0:
            phi = self.basis(bc,index=j) #(NQ,NCin,ldof,tdim,tdim)
            cell2dof = self.Incell2dof[j] #(NCin,ldof,tdim)
            val[...,i,:] = np.einsum('...ijkm,ijk->...im',phi,uh[cell2dof])
        
        i,j = np.where(index[:,None]==boundary_cell_index)
        if len(j)>0:
            phi = self.basis(bc,index=j,is_bd_dof=True) #(NQ,NCbd,ldof,tdim,tdim)
            cell2dof = self.Bdcell2dof[j] #(NCbd,ldof,tdim)
            val[...,i,:] = np.einsum('...ijkm,ijk->...im',phi,uh[cell2dof])

        val = np.einsum('...k,kmn->...mn',val,self.T)

        return val #(NQ,NC,gdim,gdim)    



    @barycentric
    def div_value(self, uh, bc, index=np.s_[:]):
        NC = self.mesh.number_of_cells()
        index = np.arange(NC)[index]
        gdim = self.geo_dimension()
        NC = len(index)
        shape = bc.shape[:-1]+(NC,gdim)
        val = np.zeros(shape,dtype=np.float64)

        boundary_cell_flag = self.mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)

        i,j = np.where(index[:,None]==inner_cell_index)
        if len(j) > 0:
            dphi = self.div_basis(bc, index=j) #(NQ,NC,ldof,tdim,gdim)
            cell2dof = self.Incell2dof[j] #(NCin,ldof,tdim)
            val[...,i,:] = np.einsum('...ijkm,ijk->...im',dphi,uh[cell2dof])

        i,j = np.where(index[:,None]==boundary_cell_index)
        if len(j) > 0:
            dphi = self.div_basis(bc, index=j, is_bd_dof=True) #(NQ,NC,ldof,tdim,gdim)
            cell2dof = self.Bdcell2dof[j] #(NCin,ldof,tdim)
            val[...,i,:] = np.einsum('...ijkm,ijk->...im',dphi,uh[cell2dof])


        return val #(NQ,NC,gdim)


    def compliance_tensor_matrix(self,mu=1,lam=1):
        #分为边界单元和内部单元处理，最后形成整体刚度矩阵
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        boundary_cell_flag = self.mesh.ds.boundary_cell_flag()
        Ms = 0
        #########################################
        #内部单元
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        if len(inner_cell_index) > 0:
            ldof = self.number_of_local_dofs()
            bcs, ws = self.integrator.quadpts, self.integrator.weights
            NCin = self.Incell2dof.shape[0]
            NQ = bcs.shape[0]
            phi = self.basis(bcs).reshape(NQ,NCin,-1,tdim)#(NQ,NC,ldof,tdim)
            #compliance_tensor
            aphi = phi.copy()
            t = np.sum(aphi[..., 0:gdim], axis=-1)
            aphi[..., 0:gdim] -= lam/(2*mu+gdim*lam)*t[..., np.newaxis]
            aphi /= 2*mu

            #construct matrix
            d = np.array([1, 1, 2])
            rm = self.mesh.reference_cell_measure()
            D = self.mesh.first_fundamental_form(bcs,index=inner_cell_index)
            D = np.sqrt(np.linalg.det(D)) #(NQ,NCin)

            aphi = phi
            M = np.einsum('i, ijkm, m, ijom, ij->jko', ws*rm, aphi, d, phi, D, optimize=True) #(NCin,ldof,ldof)

            I = np.einsum('ij, k->ijk', self.Incell2dof.reshape(NCin,-1), np.ones(ldof))
            J = I.swapaxes(-1, -2)
            tgdof = self.number_of_global_dofs()

            In_M = csr_matrix((M.flat, (I.flat, J.flat)), shape=(tgdof, tgdof))
            Ms = Ms + In_M

        #########################################
        #边界单元
        k = self.k
        boundary_cell_index, =  np.nonzero(boundary_cell_flag)
        if len(boundary_cell_index)>0:
            ldof = self.number_of_local_dofs(is_bd_dof=True)
            #qf = self.mesh.integrator(11,'cell')
            #bcs, ws = qf.get_quadrature_points_and_weights()
            bcs, ws = self.integrator.quadpts, self.integrator.weights
            NCbd = self.Bdcell2dof.shape[0]
            NQ = bcs.shape[0]
            phi = self.basis(bcs,is_bd_dof=True).reshape(NQ,NCbd,-1,tdim) #(NQ,NCbd,ldof,tdim)
            #compliance_tensor
            aphi = phi.copy()
            t = np.sum(aphi[..., 0:gdim], axis=-1)
            aphi[..., 0:gdim] -= lam/(2*mu+gdim*lam)*t[..., np.newaxis]
            aphi /= 2*mu

            #construct matrix
            d = np.array([1, 1, 2])
            rm = self.mesh.reference_cell_measure()
            D = self.mesh.first_fundamental_form(bcs,index=boundary_cell_index)
            D = np.sqrt(np.linalg.det(D)) #(NQ,NCbd)

            aphi = phi
            M = np.einsum('i, ijkm, m, ijom, ij->jko', ws*rm, aphi, d, phi, D, optimize=True) #(NCbd,ldof,ldof)

            I = np.einsum('ij, k->ijk', self.Bdcell2dof.reshape(NCbd,-1), np.ones(ldof))
            J = I.swapaxes(-1, -2)
            tgdof = self.number_of_global_dofs()

            Bd_M = csr_matrix((M.flat, (I.flat, J.flat)), shape=(tgdof, tgdof))
            Ms = Ms + Bd_M

        #M = In_M + Bd_M

        #Ms  = Ms + self.auxiliary_matrix_s()



        return Ms





    def div_matrix(self,vspace):
        '''

        Notes
        -----
        (div tau, v)

        gdim == 2
        v= [[phi,0],[0,phi]]

        [[B0],[B1]]

        '''
        gdim = self.geo_dimension()
        boundary_cell_flag = self.mesh.ds.boundary_cell_flag()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NCin = self.Incell2dof.shape[0]
        NQ = bcs.shape[0]
        B0s = 0
        B1s = 0
        ###################################
        ##内部单元
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        
        if len(inner_cell_index) > 0:
            tldof = self.number_of_local_dofs()
            vldof = vspace.number_of_local_dofs()
            tgdof = self.number_of_global_dofs()
            vgdof = vspace.number_of_global_dofs()
            dphi = self.div_basis(bcs).reshape(NQ,NCin,-1,gdim) #(NQ, NCin, tldof, gdim)
            vphi = vspace.basis(bcs)#(NQ,1,vldof)

            D = self.mesh.first_fundamental_form(bcs,index=inner_cell_index)
            D = np.sqrt(np.linalg.det(D)) #(NQ,NCin)
            rm = self.mesh.reference_cell_measure()


            B0 = np.einsum('i,ijk,ijo,ij->jko',ws*rm,vphi,dphi[...,0],D, optimize=True)
            B1 = np.einsum('i,ijk,ijo,ij->jko',ws*rm,vphi,dphi[...,1],D, optimize=True)


            I = np.einsum('ij, k->ijk', vspace.cell_to_dof()[inner_cell_index], np.ones(tldof,dtype=int))
            J = np.einsum('ij, k->ikj', self.Incell2dof.reshape(NCin,-1), np.ones(vldof,dtype=int))   
            


            In_B0 = csr_matrix((B0.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))
            In_B1 = csr_matrix((B1.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))

            B0s = B0s + In_B0
            B1s = B1s + In_B1


        ###################################
        ##边界单元
        k = self.k
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        if len(boundary_cell_index)>0:
            tldof = self.number_of_local_dofs(is_bd_dof=True)
            vldof = vspace.number_of_local_dofs()
            tgdof = self.number_of_global_dofs()
            vgdof = vspace.number_of_global_dofs()

            #qf = self.mesh.integrator(11,'cell')
            #bcs, ws = qf.get_quadrature_points_and_weights()
            NCbd = self.Bdcell2dof.shape[0]
            NQ = bcs.shape[0]

            dphi = self.div_basis(bcs,is_bd_dof=True).reshape(NQ,NCbd,-1,gdim) #(NQ, NCbd, tldof, gdim)
            vphi = vspace.basis(bcs)#(NQ,1,vldof)


            D = self.mesh.first_fundamental_form(bcs,index=boundary_cell_index)
            D = np.sqrt(np.linalg.det(D)) #(NQ,NCbd)
            rm = self.mesh.reference_cell_measure()


            B0 = np.einsum('i,ijk,ijo,ij->jko',ws*rm,vphi,dphi[...,0],D, optimize=True)
            B1 = np.einsum('i,ijk,ijo,ij->jko',ws*rm,vphi,dphi[...,1],D, optimize=True)


            I = np.einsum('ij, k->ijk', vspace.cell_to_dof()[boundary_cell_index], np.ones(tldof,dtype=int))
            J = np.einsum('ij, k->ikj', self.Bdcell2dof.reshape(NCbd,-1), np.ones(vldof,dtype=int))

            Bd_B0 = csr_matrix((B0.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))
            Bd_B1 = csr_matrix((B1.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))

            B0s = B0s + Bd_B0
            B1s = B1s + Bd_B1

        #B0 = In_B0 + Bd_B0
        #B1 = In_B1 + Bd_B1



        return B0s, B1s





    def auxiliary_div_matrix(self,vspace):
        '''

        Notes
        -----
        (tau*n, v)_e

        gdim == 2
        v= [[phi,0],[0,phi]]

        [[B0],[B1]]

        '''

        mesh = self.mesh
        tldof = self.number_of_local_dofs()
        vldof = vspace.number_of_local_dofs()
        tgdof = self.number_of_global_dofs()
        vgdof = vspace.number_of_global_dofs()

        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()


        idx = np.array([[1,2],[0,2],[0,1]],dtype=np.int8)
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)

        bdcell2edge = mesh.ds.cell_to_edge()[boundary_cell_index,1:]#最后一条边为边界边

        edge_index = np.unique(bdcell2edge)
        NE = len(edge_index)
        edge2cell = mesh.ds.edge_to_cell()[edge_index] #(NE,4)




        ldof = self.space.number_of_local_dofs()
        E2dof = np.zeros((NE,2*ldof,tdim))

        i,j = np.where(boundary_cell_index[:,None] == edge2cell[:,0])
        E2dof[j,:ldof] = self.Bdcell2dof[i]

        i,j = np.where(boundary_cell_index[:,None] == edge2cell[:,1])
        E2dof[j,ldof:] = self.Bdcell2dof[i]


        i,j = np.where(inner_cell_index[:,None] == edge2cell[:,0])
        E2dof[j,:ldof] = self.Incell2dof[i]

        i,j = np.where(inner_cell_index[:,None] == edge2cell[:,1])
        E2dof[j,ldof:] = self.Incell2dof[i]

        E2dof = E2dof.reshape(NE,-1) #(NE,2*ldof*tdim)





        e2dof = np.zeros((NE,2*vldof))
        e2dof[:,:vldof] = vspace.cell_to_dof()[edge2cell[:,0]]
        e2dof[:,vldof:] = vspace.cell_to_dof()[edge2cell[:,1]]





        qf = self.integralalg.faceintegrator
        bcs, ws = qf.get_quadrature_points_and_weights()
        NQ = bcs.shape[0]
        D = self.mesh.first_fundamental_form(bcs,index=edge_index)
        D = np.sqrt(np.linalg.det(D)) #(NQ,NE)
        rm = 1.0




        phi = np.zeros((NQ,NE,2*ldof,tdim,tdim),dtype=np.float64)
        vphi = np.zeros((NQ,NE,2*vldof),dtype=np.float64)


        for i in range(3):
            idx_temp, = np.where(edge2cell[:,2] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs

                vphi[:,idx_temp,:vldof] = vspace.basis(bc)


                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],:ldof,:,:] = self.basis(bc,index=k,is_bd_dof=True)

                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],:ldof,:,:] = self.basis(bc,index=k)


            idx_temp, = np.where(edge2cell[:,3] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs[:,-1::-1]

                vphi[:,idx_temp,vldof:] = vspace.basis(bc)


                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],ldof:,:,:] = -self.basis(bc,index=k,is_bd_dof=True)

                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],ldof:,:,:] = -self.basis(bc,index=k)

        phin = np.zeros((NQ,NE,2*ldof,tdim,gdim),dtype=np.float64)
        n = mesh.edge_unit_normal(bcs,index=edge_index) #(NQ,NE,2)

        phin[...,0] = np.einsum('...ijk,...k->...ij',phi[...,[0,2]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim)
        phin[...,1] = np.einsum('...ijk,...k->...ij',phi[...,[2,1]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim)

        phin = phin.reshape(NQ,NE,-1,gdim)

        print(phin.shape,E2dof.shape,e2dof.shape,vphi.shape)

        shape = e2dof.shape + E2dof.shape[1:]

        I = np.broadcast_to(e2dof[:,:,None],shape)
        J = np.broadcast_to(E2dof[:,None,:],shape)

        M = np.einsum('i,ijk,ijl,ij->jkl',ws,ephi,phin[...,0],rm*D) ##(NQ,1,eldof), (NQ,NE,2*ldof*tdim)->(NE,eldof,2*ldof*tdim)

        C0 = csr_matrix((M.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))

        



        sss




    def auxiliary_div_matrix_s(self,vspace):
        '''

        Notes
        -----
        (tau*n, v)_e

        gdim == 2
        v= [[phi,0],[0,phi]]

        [[B0],[B1]]

        '''

        mesh = self.mesh
        tldof = self.number_of_local_dofs()
        vldof = vspace.number_of_local_dofs()
        tgdof = self.number_of_global_dofs()
        vgdof = vspace.number_of_global_dofs()

        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()


        idx = np.array([[1,2],[0,2],[0,1]],dtype=np.int8)
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)

        bdcell2edge = mesh.ds.cell_to_edge()[boundary_cell_index,1:]#最后一条边为边界边

        edge_index = np.unique(bdcell2edge)
        NE = len(edge_index)
        edge2cell = mesh.ds.edge_to_cell()[edge_index] #(NE,4)




        ldof = self.space.number_of_local_dofs('edge')
        E2dof = np.zeros((NE,2*ldof,tdim))

        i,j = np.where(boundary_cell_index[:,None] == edge2cell[:,0])
        E2dof[j,:ldof] = self.Bdcell2dof[i]

        i,j = np.where(boundary_cell_index[:,None] == edge2cell[:,1])
        E2dof[j,ldof:] = self.Bdcell2dof[i]


        i,j = np.where(inner_cell_index[:,None] == edge2cell[:,0])
        E2dof[j,:ldof] = self.Incell2dof[i]

        i,j = np.where(inner_cell_index[:,None] == edge2cell[:,1])
        E2dof[j,ldof:] = self.Incell2dof[i]

        E2dof = E2dof.reshape(NE,-1) #(NE,2*ldof*tdim)





        e2dof = np.zeros((NE,2*vldof))
        e2dof[:,:vldof] = vspace.cell_to_dof()[edge2cell[:,0]]
        e2dof[:,vldof:] = vspace.cell_to_dof()[edge2cell[:,1]]





        qf = self.integralalg.faceintegrator
        bcs, ws = qf.get_quadrature_points_and_weights()
        NQ = bcs.shape[0]
        D = self.mesh.first_fundamental_form(bcs,index=edge_index)
        D = np.sqrt(np.linalg.det(D)) #(NQ,NE)
        rm = 1.0




        phi = np.zeros((NQ,NE,2*ldof,tdim,tdim),dtype=np.float64)
        vphi = np.zeros((NQ,NE,2*vldof),dtype=np.float64)


        for i in range(3):
            idx_temp, = np.where(edge2cell[:,2] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs

                vphi[:,idx_temp,:vldof] = vspace.basis(bc)


                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],:ldof,:,:] = self.basis(bc,index=k,is_bd_dof=True)

                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],:ldof,:,:] = self.basis(bc,index=k)


            idx_temp, = np.where(edge2cell[:,3] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs[:,-1::-1]

                vphi[:,idx_temp,vldof:] = vspace.basis(bc)


                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],ldof:,:,:] = -self.basis(bc,index=k,is_bd_dof=True)

                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],ldof:,:,:] = -self.basis(bc,index=k)

        phin = np.zeros((NQ,NE,2*ldof,tdim,gdim),dtype=np.float64)
        n = mesh.edge_unit_normal(bcs,index=edge_index) #(NQ,NE,2)

        phin[...,0] = np.einsum('...ijk,...k->...ij',phi[...,[0,2]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim)
        phin[...,1] = np.einsum('...ijk,...k->...ij',phi[...,[2,1]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim)

        phin = phin.reshape(NQ,NE,-1,gdim)

        shape = e2dof.shape + E2dof.shape[1:]

        I = np.broadcast_to(e2dof[:,:,None],shape)
        J = np.broadcast_to(E2dof[:,None,:],shape)

        M = np.einsum('i,ijk,ijl,ij->jkl',ws,vphi,phin[...,0],rm*D) ##(NQ,1,eldof), (NQ,NE,2*ldof*tdim)->(NE,eldof,2*ldof*tdim)

        C0 = csr_matrix((M.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))


        M = np.einsum('i,ijk,ijl,ij->jkl',ws,vphi,phin[...,1],rm*D) ##(NQ,1,eldof), (NQ,NE,2*ldof*tdim)->(NE,eldof,2*ldof*tdim)

        C1 = csr_matrix((M.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))


        
        return C0, C1






    def auxiliary_matrix(self):
        #增加边界单元边上的矩阵以保持相容性
        pe = self.p
        mesh = self.mesh
        idx = np.array([[1,2],[0,2],[0,1]],dtype=np.int8)
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        inner_edge_index, = np.nonzero(~mesh.ds.boundary_edge_flag())


        bdcell2edge = mesh.ds.cell_to_edge()[boundary_cell_index,1:]#最后一条边为边界边

        edge_index = np.unique(bdcell2edge)
        NE = len(edge_index)
        edge2cell = mesh.ds.edge_to_cell()[edge_index] #(NE,4)
        edge2node = mesh.ds.edge_to_node()[edge_index] #(NE,2)
        boundary_node_index = mesh.ds.boundary_node_index()


        

        qf = self.integralalg.faceintegrator
        bcs, ws = qf.get_quadrature_points_and_weights()
        NQ = len(ws)
        eldof = pe+1
        ephi = mesh.shape_function(bcs,pe) #(NQ,1,pe+1)
        egdof = NE*eldof

        e2dof = np.arange(egdof).reshape(NE,eldof)


        gdof = self.number_of_global_dofs()

        n = mesh.edge_unit_normal(bcs,index=edge_index) #(NQ,NE,2)

        ldof = self.space.number_of_local_dofs('edge')
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()



        

        E2dof = np.zeros((NE,2*ldof,tdim))

        phi = np.zeros((NQ,NE,2*ldof,tdim,tdim),dtype=np.float64)

        for i in range(3):
            idx_temp, = np.where(edge2cell[:,2] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs

                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],:ldof,:,:] = self.basis(bc,index=k,is_bd_dof=True)[...,-ldof:,:,:]
                E2dof[idx_temp[l],:ldof] = self.Bdcell2dof[k,-ldof:]

                
                
                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,0])
                if len(l)>0:
                    idx_temp = idx_temp[l]
                    edge_index_temp = edge_index[idx_temp]
                    edge2node_temp = edge2node[idx_temp]

                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,0])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],0,:,:] = self.edge_basis(bcs,index=k)[...,0,:,:]
                        E2dof[idx_temp[l],0] = self.Inedge2dof[k,0]


                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,1])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],ldof-1,:,:] = self.edge_basis(bcs,index=k)[...,-1,:,:]
                        E2dof[idx_temp[l],ldof-1] = self.Inedge2dof[k,-1]

                    




            idx_temp, = np.where(edge2cell[:,3] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs[:,-1::-1]

                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,1])
                phi[:,idx_temp[l],ldof:,:,:] = -self.basis(bc,index=k,is_bd_dof=True)[...,-ldof:,:,:]
                E2dof[idx_temp[l],ldof:] = self.Bdcell2dof[k,-ldof:]



                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,1])
                if len(l)>0:
                    idx_temp = idx_temp[l]
                    edge_index_temp = edge_index[idx_temp]
                    edge2node_temp = edge2node[idx_temp]

                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,0])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],ldof,:,:] = -self.edge_basis(bcs,index=k)[...,0,:,:]
                        E2dof[idx_temp[l],ldof] = self.Inedge2dof[k,0]
                        


                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,1])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],-1,:,:] = -self.edge_basis(bcs,index=k)[...,-1,:,:]
                        E2dof[idx_temp[l],-1] = self.Inedge2dof[k,-1]



    

        
        
        phin = np.zeros((NQ,NE,2*ldof,tdim,gdim),dtype=np.float64)

        phin[...,0] = np.einsum('...ijk,...k->...ij',phi[...,[0,2]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim)
        phin[...,1] = np.einsum('...ijk,...k->...ij',phi[...,[2,1]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim)

        phin = phin.reshape(NQ,NE,-1,gdim)
        E2dof = E2dof.reshape(NE,-1)


        shape = e2dof.shape + E2dof.shape[1:]

        I = np.broadcast_to(e2dof[:,:,None],shape)
        J = np.broadcast_to(E2dof[:,None,:],shape)


        M = np.einsum('i,ijk,ijl->jkl',ws,ephi,phin[...,0]) ##(NQ,1,eldof), (NQ,NE,2*ldof*tdim)->(NE,eldof,2*ldof*tdim)
        C0 = csr_matrix((M.flat, (I.flat, J.flat)), shape=(egdof, gdof))

        M = np.einsum('i,ijk,ijl->jkl',ws,ephi,phin[...,1]) ##(NQ,1,eldof), (NQ,NE,2*ldof*tdim)->(NE,eldof,2*ldof*tdim)
        C1 = csr_matrix((M.flat, (I.flat, J.flat)), shape=(egdof, gdof))


        return C0, C1






    def auxiliary_matrix_s(self):
        #增加边界单元边上的矩阵以保持相容性
        gamma = 1e5
        mesh = self.mesh
        idx = np.array([[1,2],[0,2],[0,1]],dtype=np.int8)
        

        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        inner_edge_index, = np.nonzero(~mesh.ds.boundary_edge_flag())

        bdcell2edge = mesh.ds.cell_to_edge()[boundary_cell_index,1:]#第一条边为边界边

        

        edge_index = np.unique(bdcell2edge)
        NE = len(edge_index)
        edge2cell = mesh.ds.edge_to_cell()[edge_index] #(NE,4)
        edge2node = mesh.ds.edge_to_node()[edge_index] #(NE,2)
        boundary_node_index = mesh.ds.boundary_node_index()

        h_e = mesh.edge_length(index=edge_index)
        mu = gamma/h_e

        

        qf = self.integralalg.faceintegrator
        bcs, ws = qf.get_quadrature_points_and_weights()
        NQ = len(ws)
        

        

        gdof = self.number_of_global_dofs()

        n = mesh.edge_unit_normal(bcs,index=edge_index) #(NQ,NE,2)

        ldof = self.space.number_of_local_dofs('edge')
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()








        E2dof = np.zeros((NE,2*ldof,tdim))
        

        phi = np.zeros((NQ,NE,2*ldof,tdim,tdim),dtype=np.float64)

        for i in range(3):
            idx_temp, = np.where(edge2cell[:,2] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs

                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,0])
                phi[:,idx_temp[l],:ldof,:,:] = self.basis(bc,index=k,is_bd_dof=True)[...,-ldof:,:,:]
                E2dof[idx_temp[l],:ldof] = self.Bdcell2dof[k,-ldof:]

                
                
                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,0])
                if len(l)>0:
                    idx_temp = idx_temp[l]
                    edge_index_temp = edge_index[idx_temp]
                    edge2node_temp = edge2node[idx_temp]

                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,0])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],0,:,:] = self.edge_basis(bcs,index=k)[...,0,:,:]
                        E2dof[idx_temp[l],0] = self.Inedge2dof[k,0]


                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,1])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],ldof-1,:,:] = self.edge_basis(bcs,index=k)[...,-1,:,:]
                        E2dof[idx_temp[l],ldof-1] = self.Inedge2dof[k,-1]

                    




            idx_temp, = np.where(edge2cell[:,3] == i)
            if len(idx_temp) > 0:
                bc = np.zeros((NQ,3),dtype=np.float64)
                bc[:,idx[i]] = bcs[:,-1::-1]

                k,l = np.where(boundary_cell_index[:,None] == edge2cell[idx_temp,1])
                phi[:,idx_temp[l],ldof:,:,:] = -self.basis(bc,index=k,is_bd_dof=True)[...,-ldof:,:,:]
                E2dof[idx_temp[l],ldof:] = self.Bdcell2dof[k,-ldof:]



                k,l = np.where(inner_cell_index[:,None] == edge2cell[idx_temp,1])
                if len(l)>0:
                    idx_temp = idx_temp[l]
                    edge_index_temp = edge_index[idx_temp]
                    edge2node_temp = edge2node[idx_temp]

                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,0])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],ldof,:,:] = -self.edge_basis(bcs,index=k)[...,0,:,:]
                        E2dof[idx_temp[l],ldof] = self.Inedge2dof[k,0]
                        


                    k,l = np.where(boundary_node_index[:,None] == edge2node_temp[:,1])
                    if len(l)>0: 
                        k,s = np.where(inner_edge_index[:,None] == edge_index_temp[l])
                        phi[:,idx_temp[l],-1,:,:] = -self.edge_basis(bcs,index=k)[...,-1,:,:]
                        E2dof[idx_temp[l],-1] = self.Inedge2dof[k,-1]




                        

        
        
        phin = np.zeros((NQ,NE,2*ldof,tdim,gdim),dtype=np.float64)

        phin[...,0] = np.einsum('...ijk,...k->...ij',phi[...,[0,2]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim,gdim)
        phin[...,1] = np.einsum('...ijk,...k->...ij',phi[...,[2,1]],n)  #(NQ,NE,2*ldof,tdim,2),(NQ,NE,2)->(NQ,NE,2*ldof,tdim,gdim)

       

        phin = phin.reshape(NQ,NE,-1,gdim)
        E2dof = E2dof.reshape(NE,-1)


        shape = E2dof.shape+(E2dof.shape[-1],)

        
        I = np.broadcast_to(E2dof[:,:,None],shape)
        J = np.broadcast_to(E2dof[:,None,:],shape)


        M = np.einsum('i,ijkm,ijlm->jkl',ws,phin,phin) ##(NQ,NE,2*ldof*tdim), (NQ,NE,2*ldof*tdim)->(NE,2*ldof*tdim,2*ldof*tdim)
        M = M*mu[:,None,None]
        M = csr_matrix((M.flat, (I.flat, J.flat)), shape=(gdof, gdof))



        

        return M






        


        

           

        
    

        

        
        



    def interpolation(self, u):
        gdof = self.number_of_global_dofs()
        ipoint = self.interpolation_points()

        val = u(ipoint) #(cgdof,gdim,gdim)
        uI = np.zeros(gdof,dtype=np.float64)

        Tensor_Frame = np.einsum('...k,kmn->...mn',self.Tensor_Frame,self.T) #(gdof,gdim,gdim)

        #内部单元插值
        c2d = self.dof.Incell2dof #(NCin,ldof)
        cell2dof = self.Incell2dof #(NCin,ldof,tdim)
        uI[cell2dof] = np.einsum('...mn,...mn->...',Tensor_Frame[cell2dof],val[c2d][:,:,None])

        #边界单元插值
        c2d = self.dof.Bdcell2dof #(NCbd,ldof)
        cell2dof = self.Bdcell2dof #(NCbd,ldof,tdim)
        uI[cell2dof] = np.einsum('...mn,...mn->...',Tensor_Frame[cell2dof],val[c2d][:,:,None])


        return self.function(array=uI)


    def function(self, array=None):
        f = Function(self, array=array, coordtype='barycentric')
        return f


    def array(self, dim=None):
        gdof = self.number_of_global_dofs()
        return np.zeros(gdof, dtype=np.float)



    def set_essential_bc(self, uh, gN, threshold=None):
        """
        初始化压力的本质边界条件，插值一个边界sigam,使得sigam*n=gN
        由face2bddof 形状为(NFbd,ldof,tdim)
        2D case 时face2bddof[...,0]--切向标架， face2bddof[...,1]--法向标架， face2bddof[...,2]--切法向组合标架
        """
        mesh = self.mesh
        gdof = self.number_of_global_dofs()
        

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = mesh.entity_barycenter('face',index=index)
                flag = threshold(bc) #(2,gNEbd), 第0行表示给的法向投影，第1行分量表示给的切向投影
                flag_idx = (np.sum(flag,axis=0)>0) #(gNFbd,)
                index = index[flag_idx]#(NFbd,)
                NEbd = len(index)

                bd_index_type = np.zeros((2,NEbd),dtype=np.bool)
                bd_index_type[0] = flag[0][flag_idx] #第0个分量表示给的法向投影
                bd_index_type[1] = flag[1][flag_idx] #第1个分量表示给的切向投影

        p = self.p
        boundary_edge_index = mesh.ds.boundary_edge_index()

        i,j = np.where(index[:,None]==boundary_edge_index)

        bdbcs = self.bdbcs[j]
        eldof = bdbcs.shape[1]
        n = np.zeros((NEbd,eldof,2))
        t = np.zeros((NEbd,eldof,2))

        for i in range(NEbd):
            bc = bdbcs[i] #(eldof,2)
            n[i] = mesh.edge_unit_normal(bc,index=[index[i]])[:,0] #(eldof,NEbd,2)
            t[i] = mesh.edge_unit_tangent(bc,index=[index[i]])[:,0] #(eldof,NEbd,2)

        
        
        isBdDof = np.zeros(gdof,dtype=np.bool)        
        f2dbd = self.dof.face_to_dof()[index]
        ipoint = self.interpolation_points()[f2dbd] #(NEbd,eldof)


        facebd2dof = self.face2dof[index,:,1:] #(NEbd,eldof,tdim-1) 第一个是t*t^T,为自由边界

        val = gN(ipoint,n,t=t) #(NEbd,eldof,gdim)，可能是法向，也可能是切向，或者两者的线性组合
        bdTensor_Frame = self.Tensor_Frame[facebd2dof] #(NEbd,eldof,tdim-1,tdim)
        bdTensor_Frame = np.einsum('ijkl,lmn,ijn->ijkm',bdTensor_Frame,self.T,n)#(NEbd,eldof,tdim-1,gdim)

        

        ##法向分量
        bd_index_temp, = np.nonzero(bd_index_type[0]) 
        if len(bd_index_temp)>0:
            bdTensor_Frame_projection = np.einsum('ijk,ijk->ij',bdTensor_Frame[bd_index_temp,:,0],n[bd_index_temp])#(NEbd,eldof)
            val_projection = np.einsum('ijk,ijk->ij',val[bd_index_temp],n[bd_index_temp]) #(NEbd,eldof)
            #print(uh[facebd2dof[bd_index_temp,:,0]].shape)
            uh[facebd2dof[bd_index_temp,:,0]] = val_projection/bdTensor_Frame_projection
            isBdDof[facebd2dof[bd_index_temp,:,0]] = True

        ##切向分量
        
        bd_index_temp, = np.nonzero(bd_index_type[1])
        if len(bd_index_temp)>0:
            bdTensor_Frame_projection = np.einsum('ijk,ijk->ij',bdTensor_Frame[bd_index_temp,:,1],t[bd_index_temp])#(NEbd,eldof)
            val_projection = np.einsum('ijk,ijk->ij',val[bd_index_temp],t[bd_index_temp]) #(NEbd,eldof)
            uh[facebd2dof[bd_index_temp,:,1]] = val_projection/bdTensor_Frame_projection
            isBdDof[facebd2dof[bd_index_temp,:,1]] = True

             

        return isBdDof





    
    def set_nature_bc(self, gD, threshold=None, q=None):
        """
        设置 natural边界条件到右端项中，由于是混合元，故此时为u的gD自由边界条件
        若对应应力边界未设置，则默认该边界方向u为0
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        gdof = self.number_of_global_dofs()

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = self.mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = self.mesh.entity_barycenter('face',index=index)
                flag = threshold(bc)
                flag = (np.sum(flag,axis=0)>0)
                index = index[flag]


        bd2dof = self.edge_to_dof()[index] #(NEbd,ldof,tdim)

        #qf = self.integralalg.faceintegrator if q is None else mesh.integrator(q, 'face')
        #qf = mesh.integrator(self.p+1,'face')
        from fealpy.quadrature.GaussLobattoQuadrature import GaussLobattoQuadrature
        qf = GaussLobattoQuadrature(np.ceil((3*self.p+1)/2))
        bcs, ws = qf.get_quadrature_points_and_weights()




        phi = self.face_basis(bcs,index=index) #(NQ,NEbd,eldof,tdim,tdim)

        

        shape = list(phi.shape)
        shape[-1] = gdim
        phin = np.zeros(shape,dtype=float) #sigam*n, (NQ,NEbd,ldof,tdim,gdim)

        n = mesh.edge_unit_normal(bcs,index=index) #(NQ,NEbd,gdim)

        phin[...,0] = np.einsum('...ijlk,...ik->...ijl',phi[...,[0,2]],n)
        phin[...,1] = np.einsum('...ijlk,...ik->...ijl',phi[...,[2,1]],n)



        pp = mesh.bc_to_point(bcs,index=index) #(NQ,NEbd,gdim)

        t = mesh.edge_unit_tangent(bcs,index=index) #(NQ,NEbd,gidm)

        val = gD(pp,n=n,t=t) #(NQ,NFbd,gdim) 此时gD函数,可能给法向分量，也可能给切向分量，具体形式在gD中体现


        rm = mesh.reference_cell_measure(TD=1)
        D = mesh.first_fundamental_form(bcs,index=index)
        D = np.sqrt(np.linalg.det(D))#(NQ,NEbd)
        bb = np.einsum('m,mil,mijkl,mi->ijk', ws*rm, val, phin, D) #(NFbd,ldof,tdim)
        
        F = np.zeros(gdof,dtype=mesh.ftype)
        np.add.at(F,bd2dof,bb)


        #数值积分计算正确性检验
        '''
        boundary_edge_index = mesh.ds.boundary_edge_index()
        i = 0
        def W(x):
            x = np.array([x])
            shape = x.shape+(2,)
            bc = np.zeros(shape,dtype=np.float64)
            bc[...,0] = x
            bc[...,1] = 1.0-x
            J = mesh.jacobi_matrix(bc,index=[boundary_edge_index[i]])
            J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
            return J[0]

        k = self.p+1
        Gaus_quad = Gauss_type_quadrature(W, k)
        

        def f(x):
            x = np.array([x])
            shape = x.shape+(2,)
            bc = np.zeros(shape,dtype=np.float64)
            bc[...,0] = x
            bc[...,1] = 1.0-x
            phi = self.face_basis(bc,index=[boundary_edge_index[i]]) #(NQ,NEbd,ldof,tdim,tdim)
            n =  mesh.edge_unit_normal(bc,index=[boundary_edge_index[i]]) #(NQ,NEbd,gdim)
            t = mesh.edge_unit_tangent(bc,index=[boundary_edge_index[i]]) #(NQ,NEbd,gidm)

            shape = list(phi.shape)
            shape[-1] = gdim
            phin = np.zeros(shape,dtype=float) #sigam*n, (NQ,NEbd,ldof,tdim,gdim)

            phin[...,0] = np.einsum('...ijlk,...ik->...ijl',phi[...,[0,2]],n)
            phin[...,1] = np.einsum('...ijlk,...ik->...ijl',phi[...,[2,1]],n)

            phin = phin[...,0,0,:] #(NQ,NEbd,gdim)

            pp = mesh.bc_to_point(bc,index=[boundary_edge_index[i]])
            val = gD(pp,n=n,t=t) #(NQ,NFbd,gdim)

            val = np.einsum('...j,...j->...',phin,val)


            return val[0,0]

        val = Gaus_quad.integrate_weight(f)
        print(val)

        ss
        '''

        





       
        return F








                





















if __name__ == '__main__':
    from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
    from fealpy.quadrature.Gauss_type_quadrature import Gauss_type_quadrature
    from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D import CurveHuZhangFiniteElementSpace
    from fealpy.mesh import TriangleMesh
    from fealpy.pde.poisson_Curved_2d import curve_circle
    import matplotlib.pyplot as plt
    import scipy.io as sio



    if True:
        #node and cell
        N = 3
        theta = np.arange(N)*2*np.pi/N
        node = np.zeros((N+1,2),dtype=np.float_)
        node[1:,0] = np.cos(theta)
        node[1:,1] = np.sin(theta)
        cell = np.zeros((N,3),dtype=np.int_)
        cell[:,1] = np.arange(N)+1
        cell[:,2] = np.arange(N)+2
        cell[-1,2] = 1
        #cell[-1,:] = cell[-1,[0,2,1]] #对cell有要求，此改法会出bug
    else: 
        i = 3
        Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
        Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']

        node = Node[i,0]
        cell = np.array(Cell[i,0],dtype=np.int_)

    curve_circle = curve_circle()

    
   
    
#######################################################################
#胡张元空间
    if False:
        #胡张元空间自由度测试
        mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
        mesh.uniform_refine(n=2)
        p = 3

        space = CurveHuZhangFiniteElementSpace_text_ss(mesh,p=p)
        k = space.k



        NN = space.space.NN
        NE = mesh.number_of_edges()
        NC = mesh.number_of_cells()

        tdim = space.tensor_dimension()

        #print(space.number_of_global_dofs())
        #print(space.number_of_local_dofs())
        #print(space.number_of_local_dofs(is_bd_dof=True))


        if False:
        #测试TensorFrame是否正确
            if True:
            #边界单元的TensorFrame
                cell = mesh.entity('cell')
                boundary_edge_index = mesh.ds.boundary_edge_index()
                boundary_cell_index = mesh.ds.boundary_cell_index()

                bdedeg2cell = mesh.ds.edge_to_cell()[boundary_edge_index]
                bdcell2edge = mesh.ds.cell_to_edge()[boundary_cell_index]

                i,j = np.where(boundary_cell_index[:,None]==bdedeg2cell[:,0])
                j_edge_idx = boundary_edge_index[j]
        

                in_bc = np.array([[1/3,2/3],
                                [2/3,1/3]],dtype=np.float_)
                
                


                
                for i in range(len(boundary_cell_index)):
                    bc = space.dof.bdbcs[j[i]]
                    TE = space.edge_orth_tensor(bc,index=[j_edge_idx[i]])[...,0,:,:]

            
                    Bdcell2dof = space.Bdcell2dof[i,-(k+1):]
                    Tensor_Frame = space.Tensor_Frame[Bdcell2dof]

                    err = np.max(np.abs(TE-Tensor_Frame))
                    if err > 1e-14:
                        stop
                    else:
                        print(err)

                    
                    TE = space.edge_orth_tensor(in_bc,index=[bdcell2edge[i,2]])[...,0,:,:]
                    Bdcell2dof = space.Bdcell2dof[i,[1,3]]
                    Tensor_Frame = space.Tensor_Frame[Bdcell2dof]

                    err = np.max(np.abs(TE-Tensor_Frame))
                    if err > 1e-14:
                        stop
                    else:
                        print(err)


                    TE = space.edge_orth_tensor(in_bc,index=[bdcell2edge[i,1]])[...,0,:,:]
                    Bdcell2dof = space.Bdcell2dof[i,[2,5]]
                    Tensor_Frame = space.Tensor_Frame[Bdcell2dof]

                    err = np.max(np.abs(TE-Tensor_Frame))
                    if err > 1e-14:
                        stop
                    else:
                        print(err)

                print(space.Tensor_Frame[space.Bdcell2dof[1,4]])
                print(space.Tensor_Frame[space.Bdcell2dof[1,0]])
                print(space.Tensor_Frame[space.Bdcell2dof[1,6]])
                print(space.Tensor_Frame[space.Bdcell2dof[1,12]])




                
            if False:
            #内部单元的TensorFrame
                boundary_cell_flag = mesh.ds.boundary_cell_flag()
                inner_cell_index, = np.nonzero(~boundary_cell_flag)
                incell2edge = mesh.ds.cell_to_edge()[inner_cell_index]

                in_bc = np.array([[1/3,2/3],
                                [2/3,1/3]],dtype=np.float_)
                
                

                for i in range(len(inner_cell_index)):

                    TE = space.edge_orth_tensor(in_bc,index=[incell2edge[i,0]])[...,0,:,:]
                    Incell2dof = space.Incell2dof[i,[7,8]]
                    Tensor_Frame = space.Tensor_Frame[Incell2dof]

                    err = np.max(np.abs(TE-Tensor_Frame))
                    print(err)
                    if err > 1e-14:
                        stop


                    

                    TE = space.edge_orth_tensor(in_bc,index=[incell2edge[i,1]])[...,0,:,:]
                    Incell2dof = space.Incell2dof[i,[2,5]]
                    Tensor_Frame = space.Tensor_Frame[Incell2dof]

                    err = np.max(np.abs(TE-Tensor_Frame))
                    print(err)
                    if err > 1e-14:
                        stop




                    TE = space.edge_orth_tensor(in_bc,index=[incell2edge[i,2]])[...,0,:,:]
                    Incell2dof = space.Incell2dof[i,[1,3]]
                    Tensor_Frame = space.Tensor_Frame[Incell2dof]

                    err = np.max(np.abs(TE-Tensor_Frame))
                    print(err)
                    if err > 1e-14:
                        stop


                print(space.Tensor_Frame[space.Incell2dof[0,4]])
                print(space.Tensor_Frame[space.Incell2dof[0,0]])
                print(space.Tensor_Frame[space.Incell2dof[0,6]])
                print(space.Tensor_Frame[space.Incell2dof[0,9]])



        if True:
        #测试基函数,插值是否正确




            from fealpy.pde.linear_elasticity_model2D import Circle_GenLinearElasticitymodel2D
            from fealpy.tools.show import showmultirate, show_error_table
            import sympy as sp
            

            pi = sp.pi
            sin = sp.sin
            cos = sp.cos
            exp = sp.exp
            ln = sp.ln

            lam = 1
            mu = 1

            maxit = 4

            x = sp.symbols('x0:2')
            u = [sin(2*pi*x[0])*sin(2*pi*x[1]),sin(2*pi*x[0])*sin(2*pi*x[1])]
            #u = [sin(2*pi*(1-(x[0]**2+x[1]**2))),sin(2*pi*(1-(x[0]**2+x[1]**2)))]
            #u = [sin(2*pi*(1-(x[0]**2+x[1]**2)))*exp(x[0]),sin(2*pi*(1-(x[0]**2+x[1]**2)))*exp(x[1])]

            pde = Circle_GenLinearElasticitymodel2D(u,x,lam=lam,mu=mu,
                            Dirichletbd_n='(0<=theta)&(theta<=2*np.pi)',
                            Dirichletbd_t='(0<=theta)&(theta<=2*np.pi)')


            errorType = ['$||\sigma - \sigma_I ||_{0}$',
                        '$||div(\sigma - \sigma_I)||_{0}$']

            Ndof = np.zeros((maxit,))
            errorMatrix = np.zeros((len(errorType), maxit), dtype=np.float64)

            for i in range(maxit):
                print("The {}-th computation:".format(i))

                space = CurveHuZhangFiniteElementSpace_text_ss(mesh,p=p)
                #space = CurveHuZhangFiniteElementSpace(mesh,p=p)

                sI = space.interpolation(pde.stress)

                errorMatrix[0,i] = space.integralalg.error(pde.stress,sI.value)
                errorMatrix[1,i] = space.integralalg.error(pde.div_stress,sI.div_value)

                Ndof[i] = space.number_of_global_dofs()

                if i < maxit-1:
                    mesh.uniform_refine()


            show_error_table(Ndof, errorType, errorMatrix)
            showmultirate(plt, 0, Ndof, errorMatrix, errorType)
            plt.show()

            

        if False:
        #测试compliance_tensor_matrix
            #space.compliance_tensor_matrix()
            space.auxiliary_matrix()







     





        if  False:
            gdim = 2
            bc = np.array([[1/2,1/2],
                            [1,0],
                            [0,1],
                            [1/3,2/3]])
            phie = space.edge_basis(bc,index=[3,4,5]) #(NQ,NE,ldof,tdim,tdim)
            print(phie.shape)
            n = mesh.edge_unit_normal(bc,index=[3,4,5]) #(NQ,NE,gdim)
    
            shape = phie.shape[:-1]+(gdim,) #(NQ,NE,ldof,tdim,gdim)

            phin = np.zeros(shape,dtype=np.float64) #(NQ,NE,ldof,tdim,gdim)

            phin[...,0] = np.einsum('...ijk,...k->...ij',phie[...,[0,2]],n)
            phin[...,1] = np.einsum('...ijk,...k->...ij',phie[...,[2,1]],n)

            print(np.max(np.abs(phin[...,0,:])))


        
#########################################################################
#带权积分
    if False:

        def W(x):
            if type(x) is float:
                x = np.array([x])
            shape = x.shape+(2,)
            bc = np.zeros(shape,dtype=np.float64)
            bc[...,0] = x
            bc[...,1] = 1.0-x
            J = mesh.jacobi_matrix(bc,index=[5])
            J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
            return J[0] 

        def p(x):
            return x**9


        k = 6
        Gaus_quad = Gauss_type_quadrature(W, k)
        quadpts, weights = Gaus_quad.Gauss_Lobatto_Quadrature(k=6)

        print(quadpts)
        print(Gaus_quad.integrate_weight(p)-np.sum(p(quadpts)*weights))


        if False:
            x = 0.7236067977499793
            space.interpolation_points()
            bc = np.array([[0.0, 1.0]],dtype=np.float64)
            print(space.edge_basis(bc).shape)

            bc = np.array([[0.0,x,1-x]],dtype=np.float64)
            print(space.basis(bc).shape)
            print(space.div_basis(bc).shape)

#########################################################################
#标量空间
    if False:
        #检查标量空间是否正确
        mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
        mesh.uniform_refine(n=3)

        boundary_cell_index = mesh.ds.boundary_cell_index()
        bdedge2cell = mesh.ds.edge_to_cell()[mesh.ds.boundary_edge_index()]

        i,j = np.where(boundary_cell_index[:,None]==bdedge2cell[:,0])
        p = 3
        space = CurveHZ_CLagrangeTriangleSpace(mesh,p)
        p = space.p
        k = space.k


        if False:
            # 检查基函数是否正确
            bdldof = space.number_of_local_dofs('cell',is_bd_dof=True)
            ldof = space.number_of_local_dofs('cell')


            bc = space.boundary_cell_multiIndex/(np.sum(space.boundary_cell_multiIndex,axis=-1)[:,None])

            bc[-(k+1):,1:] = space.bdbcs[j[0]] #(bdldof,bdldof)

            #边界单元基函数函数
            #print(np.max(np.abs(space.basis(bc,index=[0],is_bd_dof=True)[...,0,:]-np.eye(bdldof))))




            #内部单元基函数
            bc = space.inner_cell_multiIndex/(np.sum(space.inner_cell_multiIndex,axis=-1)[:,None])
            #print(np.max(np.abs(space.basis(bc)[...,0,:]-np.eye(ldof))))

 
        
            bc = space.inner_edge_multiIndex/(np.sum(space.inner_edge_multiIndex,axis=-1)[:,None])
            #print(np.max(np.abs(space.edge_basis(bc)[...,0,:]-np.eye(p+1))))


            bc = space.bdbcs[0]
            #print(np.max(np.abs(space.edge_basis(bc,is_bd_dof=True)[...,0,:]-np.eye(k+1))))

            #随机选点check
            bce = np.random.random((50,2))
            bce = bc/(np.sum(bc,axis=-1)[:,None])

            bcc = np.zeros(bc.shape[:-1]+(3,),dtype=np.float64)
            bcc[:,1:] = bc


            bdphi = space.basis(bcc,is_bd_dof=True)
            bdphie = space.edge_basis(bce,index=j,is_bd_dof=True)
            print(np.max(np.abs(bdphi[...,-(k+1):]-bdphie)))

            inphi = space.basis(bcc)
            inphie = space.edge_basis(bce)
            print(np.max(np.abs(inphi[...,-(p+1):]-inphie)))

            
        if False:
            #检查基函数是否正确
            bdbcs = space.bdbcs
            NEbd = bdbcs.shape[0]
            k = space.k

            bc = space.boundary_cell_multiIndex
            bc = bc/(np.sum(bc,axis=-1)[:,None])
            bc[-(k+1):] = 0*bc[-(k+1):]
            bd_ldof = space.number_of_local_dofs(is_bd_dof=True)

            for i in range(NEbd):
                #phi = space.edge_basis(bdbcs[i],index=[i],is_bd_dof=True)[...,0,:]
                #print(np.max(np.abs(phi-np.eye(k+1))))

                bc[-(k+1):,1:] = bdbcs[j[i]]
                phi = space.basis(bc,index=[i],is_bd_dof=True)[...,0,:]

                print(np.max(np.abs(phi-np.eye(bd_ldof))),bd_ldof)


            


        if False:
            #检查赋值是否正确
            gdof = space.number_of_global_dofs()
            uh = np.zeros(gdof,dtype=np.float64)+1.0
            
            bc = np.array([[0,2/3,1/3],
                            [2/3,0,1/3],
                            [1/3,2/3,0],
                            [1/3,1/3,1/3],
                            [1/6,1/6,2/3],
                            [1/5,2/5,2/5]],dtype=np.float64)



            boundary_cell_flag = mesh.ds.boundary_cell_flag()
            inner_cell_index, = np.nonzero(~boundary_cell_flag)
            boundary_cell_index, = np.nonzero(boundary_cell_flag)

            if len(inner_cell_index)>0:
                print(np.max(np.abs(space.value(uh,bc)[:,inner_cell_index]-1.0)))
            print(np.max(np.abs(space.value(uh,bc)[:,boundary_cell_index]-1.0)))

            if len(inner_cell_index)>0:
                print(np.max(np.abs(space.value(uh,bc)[:,inner_cell_index]-1.0)))
            print(np.max(np.abs(space.grad_value(uh, bc)[:,boundary_cell_index])))

        if True:
            #检查插值逼近是否正确
            from fealpy.pde.poisson_Curved_2d import CircleSinSinData  as PDE
            from fealpy.tools.show import showmultirate, show_error_table

            mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
            mesh.uniform_refine(n=1)
            space = CurveHZ_CLagrangeTriangleSpace(mesh, p)

            pde = PDE()
            

            if False:
                #TODO do it right.
                uI = space.interpolation(pde.solution)
                bc = np.array([[1/3,1/3,1/3]])
                pp = mesh.bc_to_point(bc)
                boundary_cell_flag = mesh.ds.boundary_cell_flag()
                inner_cell_index, = np.nonzero(~boundary_cell_flag)
                boundary_cell_index = mesh.ds.boundary_cell_index()
                uI_val = uI.value(bc)
                u_val = pde.solution(pp)
                print(uI_val[...,boundary_cell_index]-u_val[...,boundary_cell_index])

                print(uI[space.Bdcell2dof[:,4]],u_val[0,boundary_cell_index])



            if True:
                maxit = 4
                errorType = ['$|| u - u_I||_{\Omega,0}$',
                '$||\\nabla u - \\nabla u_I||_{\Omega, 0}$']


                errorMatrix = np.zeros((len(errorType), maxit), dtype=np.float64)
                NDof = np.zeros(maxit, dtype=np.int_)

                for i in range(maxit):
                    print("The {}-th computation:".format(i))

                    space = CurveHZ_CLagrangeTriangleSpace(mesh,p)
                    NDof[i] = space.number_of_global_dofs()

                    uI = space.interpolation(pde.solution)

                    errorMatrix[0,i] = space.integralalg.error(pde.solution, uI.value)
                    errorMatrix[1,i] = space.integralalg.error(pde.gradient, uI.grad_value)

                    if i < maxit-1:
                        mesh.uniform_refine()

                show_error_table(NDof, errorType, errorMatrix)
                showmultirate(plt, 0, NDof, errorMatrix,  errorType, propsize=20)
                plt.show()






#########################################################################
#绘制网格



    if True:
        #绘制网格
        mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
        mesh.uniform_refine(n=1)
        fig = plt.figure()
        axes = fig.gca()
        mesh.add_plot(axes)
        mesh.find_edge(axes,showindex=True)
        mesh.find_node(axes,showindex=True)
        mesh.find_cell(axes,showindex=True)
        fig.add_axes(axes)
        plt.show()











