import numpy as np
from numpy.linalg import inv
from scipy.sparse import csr_matrix

from fealpy.functionspace.Function import Function
from fealpy.functionspace.multindex import Multindex
from fealpy.quadrature import FEMeshIntegralAlg
from fealpy.quadrature import GaussLegendreQuadrature

# 导入默认的坐标类型, 这个空间基函数的相关计算，输入参数是重心坐标 
from fealpy.decorator import barycentric 

class HMDof3d:
    def __init__(self,mesh,p):
        """
        Parameters
        ----------
        mesh : TetrahedronMesh object
        p : the space order, p>=1

        Notes
        -----

        1. 这里不显式的存储 cell2dof, 需要的时候再构建，这样可以节约内存。

        Reference
        ------
        """
        self.mesh = mesh
        self.p = p # 默认的空间次数 p >= 0

    @property
    def cell2dof(self):
        return self.cell_to_dof()

    def boundary_dof(self, threshold=None):
        return self.is_boundary_dof(threshold=threshold)

    def is_boundary_dof(self, threshold=None):
        """

        Notes
        -----
        标记需要的边界自由度, 可用于边界条件处理。 threshold 用于处理混合边界条
        件的情形
        """
        gdof = self.number_of_global_dofs()
        isBdDof = np.zeros(gdof, dtype=np.bool_)
        if threshold is None:
            flag = self.mesh.ds.boundary_edge_flag()# 全部的边界边编号
            face2dof = self.edge_to_dof(threshold=flag)
        elif type(threshold) is np.ndarray: 
            face2dof = self.edge_to_dof(threshold=threshold)
        elif callable(threshold):
            index = self.mesh.ds.boundary_edge_index()
            bc = self.mesh.entity_barycenter('edge', index=index)
            index = index[threshold(bc)]
            face2dof = self.edge_to_dof(threshold=index)
        isBdDof[face2dof] = True
        return isBdDof


    def face_to_dof(self, threshold=None):
        '''

        Notes
        -----
        获取网格面上的自由度全局编号。
        如果 threshold 不是 None 的话，则只获取一部分边上的自由度全局编号，这部
        分边由 threshold 来决定。
        '''

        mesh = self.mesh
        fdof = self.number_of_local_dofs(doftype='face')
        if threshold is None: #所有边上的自由度
            NF = mesh.number_of_faces()
            face2dof = np.arange(NF*fdof).reshape(NF,fdof)

            return face2dof
        else: # 只获取一部分边上的自由度, 例如在混合边界条件的情形下，你只需要拿部分边界边
            if type(threshold) is np.ndarray: 
                if threshold.dtype == np.bool_:
                    index, = np.nonzero(threshold)
                else: # 否则为整数编号 
                    index = threshold
            elif callable(threshold):
                bc = mesh.entity_barycenter('face')
                index, = np.nonzero(threshold(bc))
            face2dof = fdof*index.reshape(-1, 1) + np.arange(fdof)
            return face2dof


    def cell_to_dof(self, threshold=None):
        """
        Notes
        -----
        获取每个单元元上的自由度全局编号。
        """
        p = self.p
        mesh = self.mesh

        NF = mesh.number_of_faces()
        NC = mesh.number_of_cells()
        ldof = self.number_of_local_dofs(doftype='all')  # 单元上的所有自由度
        cdof = self.number_of_local_dofs(doftype='cell') # 单元内部的自由度
        fdof = self.number_of_local_dofs(doftype='face') # 面内部的自由度
        cell2dof = np.zeros((NC, ldof), dtype=np.int_)

        face2dof = self.face_to_dof()#(NF,fdof)
        face2cell = mesh.ds.face_to_cell()#(NF,4)
       
        cell2dof[face2cell[:, [0]], face2cell[:, [2]]*fdof + np.arange(fdof)] = face2dof
        cell2dof[face2cell[:, [1]], face2cell[:, [3]]*fdof + np.arange(fdof)] = face2dof

        cell2dof[:,4*fdof:] = NF*fdof+ np.arange(NC*cdof).reshape(NC,cdof)
        return cell2dof #(NC,ldof)


    def number_of_local_dofs(self, doftype='all'):
        p = self.p
        if doftype == 'all':
            return (p+1)*(p+2)*(p+6)
        elif doftype in {'cell',3}:
            return p*(p+1)*(p+2)
        elif doftype in {'face',2}:
            return 3*(p+1)*(p+2)//2
        elif doftype in {'edge',1}:
            return 0
        elif doftype in {'node',0}:
            return 0

    def number_of_global_dofs(self):
        p = self.p
        NF = self.mesh.number_of_faces()
        NC = self.mesh.number_of_cells()
        fdof = self.number_of_local_dofs(doftype='face')
        cdof = self.number_of_local_dofs(doftype='cell')
        gdof = NF*fdof + NC*cdof
        return gdof






class HuMaFiniteElementSpace3D:
    '''
    Hu Ma Nonconforming Finite Element Space 3D.
    '''
    def __init__(self,mesh,p,q=None):
        self.mesh = mesh
        self.cellmeasure = mesh.entity_measure('cell')
        self.p = p
        self.dof = HMDof3d(mesh,p)

        self.init_matrices()
        dim = mesh.geo_dimension()
        self.mul = Multindex(dim=dim+1,p=p)

        q = q if q is not None else p+3
        self.integralalg = FEMeshIntegralAlg(
                self.mesh, q,
                cellmeasure=self.cellmeasure)
        self.integrator = self.integralalg.integrator

        self.itype = self.mesh.itype
        self.ftype = self.mesh.ftype

        self.bcoefs = self.basis_coefficients()


        

    def init_matrices(self):
        """
        Initialize the symetric matrix basis.
        """
        mesh = self.mesh
        tdim = self.tensor_dimension()
        NE = mesh.number_of_edges()
        t = mesh.edge_unit_tangent() #(NE,gdim)

        idx = np.array([(0, 0), (1, 1), (2, 2), (1, 2), (0, 2), (0, 1)])
        self.TE = np.zeros((NE, tdim), dtype=np.float)
        for i, (j, k) in enumerate(idx):
            self.TE[:,i] = t[:,j]*t[:,k]

        self.T = np.array([
            [(1, 0, 0), (0, 0, 0), (0, 0, 0)], 
            [(0, 0, 0), (0, 1, 0), (0, 0, 0)],
            [(0, 0, 0), (0, 0, 0), (0, 0, 1)],
            [(0, 0, 0), (0, 0, 1), (0, 1, 0)],
            [(0, 0, 1), (0, 0, 0), (1, 0, 0)],
            [(0, 1, 0), (1, 0, 0), (0, 0, 0)]])

        


    def tensor_dimension(self):
        gdim = self.geo_dimension()
        return gdim*(gdim+1)//2

    def geo_dimension(self):
        return self.mesh.geo_dimension()

    def basis_coefficients(self):
        """

        Notes
        ----
        4*3*(p+1)*(p+2)/2 + 6*(p+2)*(p+1)*p/6 = (p+1)*(p+2)*(p+6)
        """
        p = self.p
        #单元上全部自由度个数
        ldof = self.number_of_local_dofs(doftype='all')

        cdof = self.number_of_local_dofs(doftype='cell')
        fdof = self.number_of_local_dofs(doftype='face')

        mesh = self.mesh
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()
        NC = mesh.number_of_cells()

        LM,RM = self.face_cell_mass_matrix() #(NE,3(p+1)*(p+2)/2,ldof)
        A = np.zeros((NC,ldof,ldof),dtype=self.ftype)#(NC,ldof,ldof)

        face2cell = mesh.ds.face_to_cell()#(NC,4)


        idx0 = face2cell[:,0,None]
        idx1 = face2cell[:,2,None]
        A[idx0,idx1*fdof+np.arange(fdof)] = LM

        idx0 = face2cell[:,1,None]
        idx1 = face2cell[:,3,None]
        A[idx0,idx1*fdof+np.arange(fdof)] = RM

        A[:,(gdim+1)*fdof:,:] = self.cell_cell_mass_matrix()

        A = inv(A).reshape(NC,tdim,ldof//tdim,ldof)

        return A #(NC,tdim,ldof//tdim,ldof)

    def __str__(self):
        return "Hu-Ma mixed finite element space 3D!"

    @barycentric
    def face_basis(self, bc, index=np.s_[:],left=True):
        """

        Notes
        -----
        计算每个面左边单元上的基函数， 在该面上的取值
        """
        mesh = self.mesh
        face2cell = mesh.ds.face_to_cell()[index]
        ldof = self.number_of_local_dofs(doftype='all')
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        fdof = self.number_of_local_dofs(doftype='face')
        if left:
            idx0 = face2cell[:,0]
            idx2 = face2cell[:,2]
        else:
            idx0 = face2cell[:,1]
            idx2 = face2cell[:,2]

        NF = idx0.shape[0]

        bcoefs = np.zeros((NF,tdim,ldof//tdim,fdof),dtype=float)
        for i in range(gdim+1):
            idx_temp, = np.nonzero(idx2==i)
            bcoefs[idx_temp] = self.bcoefs[idx0[idx_temp]][...,i*fdof+np.arange(fdof)]

        val = self.face_shape_function_basis(bc,index=index,left=left)#(...,NF,tdim,ldof//tdim,tdim)
        phi = np.einsum('...ijkl,ijkm->...iml',val,bcoefs)#(...,NF,tdim,ldof//tdim,tdim),(NF,tdim,ldof//tdim,fdof)->(...,NF,fdof,tdim)

        return phi #(...,NF,fdof,tdim)

        
        

      
        

    @barycentric
    def basis(self, bc, index=np.s_[:]):
        #self.bcoefs.shape = (NC,tdim,ldof,ldof*tdim)
        #bc.shape = (...,4)
        tdim = self.geo_dimension()
        ldof = self.number_of_local_dofs(doftype='all')//tdim
        mesh = self.mesh

        val = self.shape_function_basis(bc) #(...,tdim,ldof)
        phi = np.einsum('...kl,iklj->...ikj',val,self.bcoefs[index])#(...,tdim,ldof),(NC,tdim,ldof,ldof*tdim)->(...,NC,tdim,ldof*tdim)

        T = self.TE[mesh.ds.cell_to_edge()]#(NC,tdim,tdim)
        phi = np.einsum('...ijk,ijl->...ikl',phi,T)#(...,NC,tdim,ldof*tdim),(NC,tdim,tdim)->(...,NC,ldof*tdim,tdim)

        return phi #(...,NC,ldof*tdim,tdim)
        
        


        

    @barycentric
    def div_basis(self, bc, index=np.s_[:]):
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()
        ldof = self.number_of_local_dofs(doftype='all')//tdim
        dphi = self.grad_shape_function_basis(bc,index=index)#(...,NC,tdim,ldof,gdim)
        cell2edge = self.mesh.ds.cell_to_edge()[index]#(NC,tdim)
        T = np.einsum('ijk,kmn->ijmn',self.TE[cell2edge],self.T)#(NC,tdim,gdim,gdim)
        dphi = np.einsum('...ijln,ijmn->...ijlm',dphi,T)#(...,NC,tdim,ldof,gdim)


        dphi = np.einsum('...ijkl,ijkm->...iml',dphi,self.bcoefs[index])#(...,NC,tdim,ldof,gdim),(NC,tdim,ldof,ldof*tdim)->(...,NC,ldof*tdim,gdim)

        

       

        return dphi #(...,NC,ldof*tdim,gdim)
        

    @barycentric
    def grad_basis(self, bc):
        pass

    def cell_to_dof(self):
        return self.dof.cell2dof

    def face_to_dof(self,threshold=None):
        return self.dof.face_to_dof(threshold=threshold)

    


    def boundary_dof(self,threshold=None):
        return self.dof.is_boundary_dof(threshold=threshold)

    def number_of_global_dofs(self):
        return self.dof.number_of_global_dofs()

    def number_of_local_dofs(self, doftype='all'):
        return self.dof.number_of_local_dofs(doftype)

    @barycentric
    def value(self, uh, bc, index=np.s_[:]):
        phi = self.basis(bc,index=index)#(NQ,NC,ldof,tdim)
        cell2dof = self.cell_to_dof()[index]
        uh = uh[cell2dof]#(NC,ldof)
        val = np.einsum('ij,...ijk->...ik',uh,phi)#(NQ,NC,tdim)
        val = np.einsum('...k,kmn->...mn',val,self.T)
        return val #(NQ,NC,gdim,gdim)

    @barycentric
    def div_value(self, uh, bc, index=np.s_[:]):
        dphi = self.div_basis(bc,index=index) #(NQ,NC,ldof,gdim)
        cell2dof = self.cell_to_dof()[index]
        uh = uh[cell2dof]#(NC,ldof)
        val = np.einsum('ij,...ijk->...ik',uh,dphi)#(NQ,NC,gdim)
        return val



    @barycentric
    def grad_value(self, uh, bc, index=np.s_[:]):
        pass

    @barycentric
    def edge_value(self, uh, bc, index=np.s_[:]):
        pass

    @barycentric
    def face_value(self, uh, bc, index=np.s_[:]):
        pass

    def function(self, dim=None, array=None):
        f = Function(self, dim=dim, array=array, coordtype='barycentric')
        return f

    def project(self, u):
        return self.interpolation(u)

    def interpolation(self, u):
        pass


    def array(self,dim=None):
        gdof = self.number_of_global_dofs()
        return np.zeros(gdof, dtype=np.float)

    def compliance_tensor_matrix(self,mu=1,lam=1):
        ldof = self.number_of_local_dofs(doftype='all')
        tdim =self.tensor_dimension()
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        NQ = bcs.shape[0]

        phi = self.basis(bcs) #(NQ,NC,ldof,tdim)
        #compliance_tensor
        aphi = phi.copy()
        t = np.sum(aphi[..., 0:gdim], axis=-1)
        aphi[..., 0:gdim] -= lam/(2*mu+gdim*lam)*t[..., np.newaxis]
        aphi /= 2*mu

        #construct matrix
        d = np.array([1, 1, 1, 2, 2, 2])
        M = np.einsum('i, ijkm, m, ijom, j->jko', ws, aphi, d, phi, self.mesh.entity_measure(), optimize=True) #(NC,ldof,ldof)

        gdof = self.number_of_global_dofs()

        cell2dof = self.cell_to_dof()
        I = np.broadcast_to(cell2dof[:,:,None],shape=M.shape)
        J = np.broadcast_to(cell2dof[:,None,:],shape=M.shape)

        M = csr_matrix((M.flat,(I.flat,J.flat)),shape=(gdof,gdof))

        return M



    def  parallel_compliance_tensor_matrix(self,mu=1,lam=1):
        pass

    def div_matrix(self,vspace):
        '''

        Notes
        -----
        (div tau, v)

        gdim == 3
        v= [[phi,0,0],[0,phi,0],[0,0,phi]]

        [[B0],[B1],[B2]]

        '''
        tldof = self.number_of_local_dofs()
        vldof = vspace.number_of_local_dofs()
        tgdof = self.number_of_global_dofs()
        vgdof = vspace.number_of_global_dofs()

        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        NQ = bcs.shape[0]

        dphi = self.div_basis(bcs) #(NQ,NC,ldof,gdim)
        vphi = vspace.basis(bcs)#(NQ,1,vldof)

        B0 = np.einsum('i,ijk,ijo,j->jko',ws,vphi,dphi[...,0],self.cellmeasure, optimize=True)
        B1 = np.einsum('i,ijk,ijo,j->jko',ws,vphi,dphi[...,1],self.cellmeasure, optimize=True)
        B2 = np.einsum('i,ijk,ijo,j->jko',ws,vphi,dphi[...,2],self.cellmeasure, optimize=True)

        c2d0 = vspace.cell_to_dof()
        c2d1 = self.cell_to_dof()

        I = np.broadcast_to(c2d0[:,:,None],shape=B0.shape)
        J = np.broadcast_to(c2d1[:,None,:],shape=B0.shape)

        B0 = csr_matrix((B0.flat,(I.flat,J.flat)),shape=(vgdof,tgdof))
        B1 = csr_matrix((B1.flat,(I.flat,J.flat)),shape=(vgdof,tgdof))
        B2 = csr_matrix((B2.flat,(I.flat,J.flat)),shape=(vgdof,tgdof))
        return B0, B1, B2

                

    def parallel_div_matrix(self,vspace):
        pass

    
    def set_essential_bc(self,uh,gN,threshold=None,q=None):
        '''
        初始化压力的本质边界条件
        '''
        mesh = self.mesh
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()
        fdof = self.number_of_local_dofs(doftype='face')
        gdof = self.number_of_global_dofs()
        p = self.p

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            if threshold is not None:
                index = mesh.ds.boundary_face_index()
                bc = mesh.entity_barycenter('face',index)
                flag = threshold(bc) #(3,gNFbd) 第0行表示给的法向投影,第1行分量表示给的切向投影,第2行分量表示给的切向投影
                flag_idx = (np.sum(flag,axis=0)>0) #(gNFbd,)
                index = index[flag_idx] #(NFbd,)
                NFbd = len(index)

                bd_index_type = np.zeros((3,NFbd),dtype=np.bool)
                bd_index_type[0] = flag[0][flag_idx] #第0个分量表示给的法向投影
                bd_index_type[1] = flag[1][flag_idx] #第1个分量表示给的切向投影
                bd_index_type[2] = flag[2][flag_idx] #第2个分量表示给的切向投影


        n = mesh.face_unit_normal()[index]#(NFbd,gdim)
        _, _, frame = np.linalg.svd(n[:, np.newaxis, :]) # get the axis frame on the face by svd
        frame[:,0] = n
        t0 = frame[:,1]
        t1 = frame[:,2]

        isBdDof = np.zeros(gdof,dtype=np.bool)#判断是否为固定顶点
        bd2dof = self.face_to_dof(threshold=index).reshape(NFbd,gdim,fdof//gdim) #(NFbd,3,fdof), bd2dof[:,0,:]表示法向, bd2dof[:,1,:]表示切向,bd2dof[:,2,:]表示切向


        qf = mesh.integrator(p+3,'face') if q is None else mesh.integrator(q,'face')
        bcs, ws = qf.quadpts, qf.weights #bcs.shape=(NQ,3)
        pcs = mesh.bc_to_point(bcs)[:,index] #(NQ,NFbd,3)

        #做边上的投影
        multindex = self.mul.multindex(dim=2+1,p=p)#((p+1)*(p+2)//2,3)
        phi0 = np.prod(np.power(bcs[...,None,:],multindex),axis=-1)#(NQ,(p+1)*(p+2)//2)
        val = gN(pcs,n,t0=t0,t1=t1)#(NQ,NFbd,gdim)


        for i in range(gdim):
            bd_index, = np.nonzero(bd_index_type[i])
            uh[bd2dof[bd_index,i]] = np.einsum('i,ik,ijl,jl->jk',ws,phi0,val,frame[:,i,:],optimize=True)[bd_index] #(NFbd,(p+1)*(p+2)//2)
            isBdDof[bd2dof[bd_index,i]] = True


        return isBdDof

        






















    def set_nature_bc(self,gD,threshold=None,q=None):
        """
        设置 natural边界条件到右端项中，由于是混合元，故此时为u的gD自由边界条件
        若对应应力边界未设置，则默认该边界方向u为0
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        gdof = self.number_of_global_dofs()
        p = self.p

        if type(threshold) is np.array:
            index = threshold
        else:
            index = self.mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = self.mesh.entity_barycenter('face',index=index)
                flag = threshold(bc)
                flag = (np.sum(flag,axis=0)>0)
                index = index[flag]

            bd2dof = self.face_to_dof(threshold=index) #(NFbd,fdof)

            n = mesh.face_unit_normal(index=index)#(NFbd,gdim)
            measure = mesh.entity_measure('face',index=index)
            qf = mesh.integrator(p+3,'face') if q is None else mesh.integrator(q,'face')
            bcs, ws = qf.quadpts, qf.weights #bcs.shape=(NQ,3)
            phi = self.face_basis(bcs,index=index,left=True) #(NQ,NFbd,fdof,tdim)
            phin = np.einsum('...jkl,lmn,jn->...jkm',phi,self.T,n)#(NQ,NFbd,fdof,tdim),(tdim,gdim,gdim),(NFbd,gdim)->(NQ,NFbd,fdof,gdim)

            pp = mesh.bc_to_point(bcs,index=index)
            _, _, frame = np.linalg.svd(n[:, np.newaxis, :]) # get the axis frame on the face by svd
            t0 = frame[:,1]
            t1 = frame[:,2]
            val = gD(pp,n=n,t0=t0,t1=t1) #(NQ,NFbd,gdim) 此时gD函数,可能给法向分量，也可能给切向分量，具体形式在gD中体现
            bb = np.einsum('i,ijk,ijlk,j->jl',ws,val,phin,measure)#(NFbd,fdof)
            F = np.zeros(gdof,dtype=np.float)
            np.add.at(F,bd2dof,bb)
            return F

















    def shape_function_basis(self,bc,index=np.s_[:],p=None):
        '''
        shape function
        '''
        if p is None:
            p = self.p
        
        tdim = self.tensor_dimension()
        ldof = self.number_of_local_dofs(doftype='all')//tdim
        shape = bc.shape[:-1]+(tdim,ldof) #(...,tdim,ldof)
        phi = np.zeros(shape,dtype=np.float)

        idx,idx_r = self.cell_to_edge_node_index()

        mulidx_p = self.mul.multindex()
        mulidx_p_1 = self.mul.multindex(p=p-1)
        for i in range(tdim):
            base_idx = 0
            l = (p+1)**2
            mulidx, = np.nonzero((mulidx_p[:,idx[i,0]]==0)|(mulidx_p[:,idx[i,1]]==0))
            mulidx = mulidx_p[mulidx]
            phi[...,i,base_idx:base_idx+l] = np.prod(np.power(bc[...,None,:],mulidx),-1)
            base_idx+=l

            l=p+1
            mulidx = self.mul.multindex(dim=1+1,p=p)
            lai_laj = (bc[...,idx[i,0]]-bc[...,idx[i,1]])[...,None]
            phi[...,i,base_idx:base_idx+l] = lai_laj*np.prod(np.power(bc[...,None,idx_r[i]],mulidx),axis=-1)
            base_idx+=l

            l = p*(p+1)*(p+2)//6 #p-1次多项式次数
            lai_laj=(bc[...,idx[i,0]]*bc[...,idx[i,1]])[...,None]
            phi[...,i,base_idx:] = lai_laj*np.prod(np.power(bc[...,None,:],mulidx_p_1),-1)
            base_idx+=l

        return phi #(...,tdim,ldof)

    def face_shape_function_basis(self,bc,index=np.s_[:],left=True,p=None):
        '''
        计算shape function basis在每条边左边单元上的基函数， 在该边上的取值
        '''
        p = self.p if p is None else p
        mesh = self.mesh
        face2cell = mesh.ds.face_to_cell()[index]
        
        if left:
            idx0 = face2cell[:,0] #(NF,)----所在单元编号
            #idx2 = face2cell[:,2] #(NF,)----为对应单元的面的序号
        
        else:
            idx0 = face2cell[:,1]
            #idx2 = face2cell[:,3]

        #idx2 = self.cell_to_face_node_index()[idx2] #(NF,3)

        face = mesh.ds.face[index]#(NF,3)
        cell = mesh.ds.cell[idx0]#(NF,4) 
        NF = len(idx0)
        shape = bc.shape[:-1]+(NF,4)
        bcs = np.zeros(shape,dtype=np.float)#(...,NF,4)



        for i in range(3):
            idx = np.where(cell==face[:,i,None])
            bcs[...,idx[0],idx[1]] = bc[...,None,i]
        

        phi = self.shape_function_basis(bcs,p=p) #(...,NF,tdim,ldof)

        cell2edge = mesh.ds.cell_to_edge()[idx0] #(NF,tdim)
        T = self.TE[cell2edge] #(NF,tdim,tdim)
        phi = np.einsum('...ijk,ijl->...ijkl',phi,T) #(...,NF,tdim,ldof,tdim)


        return phi #(...,NF,tdim,ldof,tdim)

    def grad_shape_function_basis(self,bc,index=np.s_[:],p=None):
        '''
        grad shape function
        '''
        if p is None:
            p = self.p

        mesh = self.mesh
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        ldof = self.number_of_local_dofs(doftype='all')//tdim
        grad_lam = mesh.grad_lambda()[index] #(NC,gdim+1,gdim)
        NC = grad_lam.shape[0]
        shape = bc.shape[:-1]+(NC,tdim,ldof,gdim)
        phi = np.zeros(shape,dtype=self.ftype)#(...,NC,tdim,ldof,gdim)

        idx,idx_r = self.cell_to_edge_node_index()

        mulidx_p = self.mul.multindex()
        mulidx_p_1 = self.mul.multindex(p=p-1)
        mulidx_1d_p = self.mul.multindex(dim=1+1,p=p)


        for i in range(tdim):
            base_idx = 0
            l = (p+1)**2
            mulidx, = np.nonzero((mulidx_p[:,idx[i,0]]==0)|(mulidx_p[:,idx[i,1]]==0))
            mulidx = mulidx_p[mulidx]
            for j in range(gdim+1):
                idx_nozero, = np.nonzero(mulidx[:,j]!=0)
                mulidx_temp = mulidx[idx_nozero]
                mulidx_temp[:,j] -= 1
                phi[...,i,base_idx+idx_nozero,:]+= np.einsum('i,...i,jk->...jik',mulidx[idx_nozero,j],
                            np.prod(np.power(bc[...,None,:],mulidx_temp),axis=-1),grad_lam[:,j])
            base_idx+=l

            l=p+1
            phi[...,i,base_idx:base_idx+l,:]+=np.einsum('...i,jk->...jik',
                            np.prod(np.power(bc[...,None,idx_r[i]],mulidx_1d_p),axis=-1),grad_lam[:,idx[i,0]]-grad_lam[:,idx[i,1]])
            for j in range(2):
                idx_nozero, = np.nonzero(mulidx_1d_p[:,j]!=0)
                mulidx_temp = mulidx_1d_p[idx_nozero]
                mulidx_temp[:,j] -= 1
                phi[...,i,base_idx+idx_nozero,:]+= np.einsum('...,i,...i,jk->...jik',(bc[...,idx[i,0]]-bc[...,idx[i,1]]),mulidx_1d_p[idx_nozero,j],
                                                    np.prod(np.power(bc[...,None,idx_r[i]],mulidx_temp),axis=-1),grad_lam[:,idx_r[i,j]])
            base_idx+=l


            l = p*(p+1)*(p+2)//6
            mulidx = np.copy(mulidx_p_1)
            mulidx[:,idx[i]]+=1
            for j in range(gdim+1):
                idx_nozero, = np.nonzero(mulidx[:,j]!=0)
                mulidx_temp = mulidx[idx_nozero]
                mulidx_temp[:,j] -= 1
                phi[...,i,base_idx+idx_nozero,:]+= np.einsum('i,...i,jk->...jik',mulidx[idx_nozero,j],
                            np.prod(np.power(bc[...,None,:],mulidx_temp),axis=-1),grad_lam[:,j])
            base_idx+=l
        return phi #(...,NC,tdim,ldof,gdim)






    
    def cell_to_edge_node_index(self):
        '''
        idx[i,:] 表示cell的第i条边为cell[idx[i,0],idx[i,1]]
        idx_r[i,:] 表示cell的第i条边的对边为cell[idx[i,0],idx[i,1]]
        '''
        idx = np.array([(0,1),(0,2),(0,3),(1,2),(1,3),(2,3)])
        idx_r = np.array([(2,3),(1,3),(1,2),(0,3),(0,2),(0,1)])
        return idx,idx_r

    def cell_to_face_node_index(self):
        '''
        idx[i,:] 表示cell的第i个面为cell[idx[i,0],idx[i,1],idx[i,2]]
        '''
        idx = np.array([[1, 2, 3], [0, 2, 3], [0, 1, 3], [0, 1, 2]])
        return idx


    def face_cell_mass_matrix(self,p=None):
        '''
        面上自由度决定的积分
        '''
        p = self.p if p is None else p
        mesh = self.mesh
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        ldof = self.number_of_local_dofs(doftype='all')//tdim

    
        NF = mesh.number_of_faces()
        qf = mesh.integrator(p+3,'face')
        bcs, ws = qf.quadpts, qf.weights #bcs.shape=(NQ,2)
        NQ = len(ws)

        multindex = self.mul.multindex(dim=2+1,p=p)#((p+1)*(p+2)//2,3)

        phi0 = np.prod(np.power(bcs[...,None,:],multindex),axis=-1)#(NQ,(p+1)*(p+2)//2)
        phi1 = self.face_shape_function_basis(bcs,left=True,p=p) #(NQ,NF,tdim,ldof,tdim)
        phi2 = self.face_shape_function_basis(bcs,left=False,p=p) #(NQ,NF,tdim,ldof,tdim)

        n = mesh.face_unit_normal()
        _, _, frame = np.linalg.svd(n[:, np.newaxis, :]) # get the axis frame on the face by svd
        frame[:, 0, :] = n

        phi1 = np.einsum('...ijkl,lmn,in->...ijkm',phi1,self.T,n).reshape(NQ,NF,-1,gdim)#(NQ,NF,tdim*ldof,gdim)
        phi2 = np.einsum('...ijkl,lmn,in->...ijkm',phi2,self.T,n).reshape(NQ,NF,-1,gdim)#(NQ,NF,tdim*ldof,gdim)

        Np_2d = (p+1)*(p+2)//2
        LM = np.zeros(shape=(NF,3*Np_2d,tdim*ldof),dtype=self.ftype)#(NF,3*Np_2d,tdim*ldof)
        RM = np.zeros(shape=(NF,3*Np_2d,tdim*ldof),dtype=self.ftype)#(NF,3*Np_2d,tdim*ldof)

        for i in range(gdim):
            LM[:,i*Np_2d:(i+1)*Np_2d,:] += np.einsum('i,ij,iklm,km->kjl',ws,phi0,phi1,frame[:,i,:],optimize=True) #(NF,Np_2d,tdim*ldof)
            RM[:,i*Np_2d:(i+1)*Np_2d,:] += np.einsum('i,ij,iklm,km->kjl',ws,phi0,phi2,frame[:,i,:],optimize=True) #(NF,Np_2d,tdim*ldof)
            

        return LM,RM #(NF,3*Np_2d,tdim*ldof)
        


    def cell_cell_mass_matrix(self,p=None,q=None):
        '''
        单元上自由度决定的积分
        '''
        p = self.p if p is None else p
        qf = self.integrator if q is None else self.mesh.integrator(q,'cell')
        bcs, ws = qf.get_quadrature_points_and_weights()
        NQ = len(ws)
        Np_1 = (p+2)*(p+1)*p//6 #p-1次多项式全体个数
        tdim = self.tensor_dimension()
        NC = self.mesh.number_of_cells()

        phi1 = self.shape_function_basis(bcs,p=p) #(NQ,tdim,ldof)
        phi0 = phi1[...,-Np_1:]#(NQ,tdim,Np_1)

        T = self.TE[self.mesh.ds.cell_to_edge()]#(NC,tdim,tdim)

        phi1 = np.einsum('ikl,jkm->ijklm',phi1,T).reshape(NQ,NC,-1,tdim) #(NQ,NC,tdim*ldof,tdim)
        phi0 = np.einsum('ikl,jkm->ijklm',phi0,T).reshape(NQ,NC,-1,tdim) #(NQ,NC,tdim*Np_1,tdim)

        d = np.array([1,1,1,2,2,2])
        M = np.einsum('i,ijkm,m,ijlm->jkl',ws,phi0,d,phi1)#(NC,tdim*Np_1,tdim*ldof)

        return M #(NC,tdim*Np_1,tdim*ldof)

        



    def edge_bc_to_cell_bcs(self,bc,index=np.s_[:],left=True):
        '''
        Notes
        将边上的重心坐标变成面上的重心坐标
        '''
        face2cell = self.mesh.ds.edge_to_cell()[index]
        if left:
            idx0 = face2cell[:,0] #(NE,)----所在单元编号
            idx2 = face2cell[:,2] #(NE,)----为对应单元的边的序号
        
        else:
            idx0 = face2cell[:,1]
            idx2 = face2cell[:,3]

        NE = len(idx0)
        cell2edgeSign = mesh.ds.cell_to_edge_sign()[idx0,idx2]
        shape = bc.shape[:-1]+(NE,3)
        bcs = np.zeros(shape,dtype=np.float)#(...,NE,3)

        idx2 = self.cell_to_edge_node_index()[idx2]

        for i in range(2):
            bcs[...,cell2edgeSign,idx2[cell2edgeSign,i]] += bc[...,None,i]
            bcs[...,~cell2edgeSign,idx2[~cell2edgeSign,i]] += bc[...,None,1-i]

        return bcs



    def edge_projection(self,u,index=np.s_[:],p=None):
        '''
        计算u向边上的投影
        '''
        p = self.p if p is None else p
        mesh = self.mesh
        fdof = self.number_of_local_dofs(doftype='face')

        measure = mesh.entity('edge')[index] #(NE,)
        NE = len(measure)
        
        qf = GaussLegendreQuadrature(p+3)
        bcs, ws = qf.quadpts, qf.weights #bcs.shape=(NQ,2)
        pcs = mesh.edge_bc_to_point(bcs) #pcs.shape=(NQ,NE,2)
        NQ = len(ws)

        multindex = self.mul.multindex(dim=1+1,p=p)#(p+1,2)

        phi0 = np.prod(np.power(bcs[...,None,:],multindex),axis=-1)#(NQ,p+1)

        phi1 = u(pcs)#(NQ,NE,gdim,gdim)

        n = mesh.edge_unit_normal()[index] #(NE,gdim)
        t = mesh.edge_unit_tangent()[index]#(NE,gdim)

        phi1 = np.einsum('ijkl,jl->ijk',phi1,n)#(NQ,NE,gdim)

        M = np.zeros((NE,2*(p+1)),dtype=self.ftype) #(NE,2*(p+1))

        M[:,:p+1] += np.einsum('i,ij,ikl,l,j->jk',ws,phi0,phi1,n,measure,optimize=True)#(NE,(p+1))
        M[:,p+1:] += np.einsum('i,ij,ikl,l,j->jk',ws,phi0,phi1,t,measure,optimize=True)#(NE,(p+1))

        return M














        




if __name__ == '__main__':
    import fealpy.mesh.MeshFactory as mf
    import matplotlib.pyplot as plt
    from scipy.sparse import csr_matrix
    import numpy as np
    import sys
    

    p = int(sys.argv[1])
    box = [0, 1, 0, 1, 0, 1]
    mesh = mf.boxmesh3d(box, nx=1, ny=1, nz=1, meshtype='tet')
    #mesh.uniform_refine(n=3)
    #mesh = mf.one_tetrahedron_mesh()
    dof = HMDof3d(mesh,p)
    space = HuMaFiniteElementSpace3D(mesh,p)

    

    if True:
        #bc = np.array([1/2,1/4,1/4,0])
        bc = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
        #print(space.shape_function_basis(bc).shape)
        #print(space.basis(bc).shape) #(NQ,NC,ldof,tdim)
        #print(space.grad_shape_function_basis(bc)[0,0,3])
        #print(mesh.grad_lambda())
        print(space.div_basis(bc).shape)

    if False:
        bc = np.array([[[1,0,0],[0,1,0],[0,0,1]]])
        #bc = np.array([[0,0,1]])
        #print(space.face_shape_function_basis(bc,left=False).shape)
        print(space.face_basis(bc).shape)


    if False:
        LM,RM = space.face_cell_mass_matrix()
        M = space.cell_cell_mass_matrix()
        print(LM.shape)
        print(M.shape)
        #bcoefs = space.basis_coefficients()
        #idx = np.abs(bcoefs)<1e-15
        #bcoefs[idx]=0
        #print(bcoefs[0,0])
        #print(space.TE)

    if False:
        print(space.compliance_tensor_matrix().shape,space.number_of_global_dofs())

    if False:
        print('cell:')
        print(mesh.entity('cell'),'\n')

        print('face:')
        print(mesh.entity('face'),'\n')

        print('edge')
        print(mesh.entity('edge'),'\n')

        print('cell2face:')
        print(mesh.ds.cell_to_face(),'\n')
        
        print('cell2edge:')
        print(mesh.ds.cell_to_edge(),'\n')

        print('face2cell:')
        print(mesh.ds.face_to_cell(),'\n')

        print('cell2dof:')
        print(dof.cell_to_dof(),'\n')





    if False:
        cell2dof = dof.cell_to_dof()
        print('cell2dof:')
        for i, val in enumerate(cell2dof):
            print(i, ": ", val)

    if False:
        fig = plt.figure()
        axes = fig.gca()
        mesh.add_plot(axes)
        mesh
        mesh.find_node(axes, showindex=True, fontsize=24)
        mesh.find_edge(axes, showindex=True, fontsize=22)
        mesh.find_cell(axes, showindex=True, fontsize=20)
        plt.show()

    if False:
        '''
        check the cell basis
        '''
        p = space.p
        qf = space.integrator
        bcs, ws = qf.get_quadrature_points_and_weights()
        cellmeasure = space.cellmeasure
        NQ = len(ws)
        Np_1 = (p+1)*p//2 #p-1次多项式全体个数
        tdim = space.tensor_dimension()
        ldof = space.number_of_local_dofs()//tdim
        NC = space.mesh.number_of_cells()


        phi0 = space.shape_function_basis(bcs)[...,-Np_1:]#(NQ,tdim,Np_1)
        phi1 = space.shape_function_basis(bcs)#(NQ,tdim,ldof)
        phi2 = space.basis(bcs) #(NQ,NC,tdim,ldof,tdim)
        


        #print(phi0[0])
        T = space.TE[space.mesh.ds.cell_to_edge()]#(NC,tdim,tdim)

        phi1 = np.einsum('ikl,jkm->ijklm',phi1,T).reshape(NQ,NC,-1,tdim) #(NQ,NC,tdim*ldof,tdim)
        phi0 = np.einsum('ikl,jkm->ijklm',phi0,T).reshape(NQ,NC,-1,tdim) #(NQ,NC,tdim*Np_1,tdim)

        #print(phi0[0,0])

        d = np.array([1,1,2])
        M = np.einsum('i,ijkm,m,ijlm,j->jkl',ws,phi0,d,phi1,cellmeasure) #(NC,tdim*Np_1,tdim*ldof)

        bcoefs = space.bcoefs.reshape(NC,ldof*tdim,-1) #(NC,tdim*ldof,tdim*ldof)

        S = np.einsum('ijk,ikl->ijl',M,bcoefs)#(NQ,tdim*Np_1,ldof*tdim)
        #print(S)

        SS = np.einsum('i,ijkm,m,ijlm,j->jkl',ws,phi0,d,phi2,cellmeasure)

        print(SS)







    
