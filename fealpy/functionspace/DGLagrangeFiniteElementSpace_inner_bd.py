import numpy as np
from scipy.sparse import csr_matrix



from fealpy.functionspace.BarycentricSpace import BarycentricSpace
from fealpy.decorator import barycentric
from fealpy.functionspace.Function import Function
from fealpy.mesh.core import multi_index_matrix
from fealpy.quadrature import FEMeshIntegralAlg



class DGLagrangeFiniteElementSpace_inner_bd():
    '''
    间断有限元空间，内部单元多项式次数和边界单元多项式次数可以不同
    '''
    def __init__(self, mesh, inner_p, boundary_p=None, spacetype='D'):
        """
        Notes 
        -----
            内部单元用inner_p次元，边界单元用boundary_p次元
            用于胡张元位移空间的构建
        """
        boundary_p = inner_p if boundary_p is None else boundary_p
        self.inner_p = inner_p
        self.boundary_p = boundary_p

        self.mesh = mesh
        self.dim = mesh.geo_dimension()
        self.cellmeasure = mesh.entity_measure('cell')

        self.init_multiIndex()
        self.init_number_of_cell()
        self.init_cell_to_dof()

        self.inner_cell_space = BarycentricSpace(mesh, self.inner_cell_multiIndex)
        self.boundary_cell_space = BarycentricSpace(mesh, self.boundary_cell_multiIndex)

        q = 2*np.max([inner_p,boundary_p])
        q = np.min([q,11])
        self.integralalg = FEMeshIntegralAlg(
                self.mesh, q,
                cellmeasure=self.cellmeasure)
        
        self.integrator = self.integralalg.integrator

        self.ftype = mesh.ftype
        self.itype = mesh.itype

    
    def init_multiIndex(self):
        self.inner_cell_multiIndex = multi_index_matrix[2](self.inner_p)
        self.boundary_cell_multiIndex = multi_index_matrix[2](self.boundary_p)
    

    def init_number_of_cell(self):
        mesh = self.mesh
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        self.NCin = np.sum(~boundary_cell_flag)
        self.NCbd = np.sum(boundary_cell_flag)


    def init_cell_to_dof(self):
        #先排内部单元，后排边界单元
        in_ldof = self.number_of_local_dofs()
        bd_ldof = self.number_of_local_dofs(is_bd_dof=True)

        NCin = self.NCin
        NCbd = self.NCbd

        self.Incell2dof = np.arange(NCin*in_ldof).reshape(NCin,in_ldof)
        self.Bdcell2dof = (np.arange(NCbd*bd_ldof)+NCin*in_ldof).reshape(NCbd,bd_ldof)


    def cell_to_dof(self):
        #在inner_p==boundary_p时会用
        ldof = self.number_of_local_dofs()
        mesh = self.mesh
        NC = mesh.number_of_cells()
        cell2dof = np.zeros((NC,ldof),dtype=self.itype)
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        inner_cell_index, = np.nonzero(~boundary_cell_flag)

        cell2dof[inner_cell_index] = self.Incell2dof
        cell2dof[boundary_cell_index] = self.Bdcell2dof

        return cell2dof

    @barycentric
    def basis(self,bc,index=np.s_[:],is_bd_dof=False):

        if is_bd_dof:
            return self.boundary_cell_space.basis(bc) #(NQ,1,bd_ldof)
        
        else:
            return self.inner_cell_space.basis(bc) #(NQ,1,in_ldof)

    


    def interpolation_points(self):
        mesh = self.mesh
        gdof = self.number_of_global_dofs()
        ipoint = np.zeros((gdof,2),dtype=np.float64)

        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        boundary_cell_index, = np.nonzero(boundary_cell_flag)


        #内部单元插值点
        bc = self.inner_cell_multiIndex
        bc = bc/np.sum(bc,axis=-1)[:,None]
        ipoint[self.Incell2dof] = mesh.bc_to_point(bc,index=inner_cell_index).swapaxes(0,1)


        #边界单元插值点
        bc = self.boundary_cell_multiIndex
        bc = bc/np.sum(bc,axis=-1)[:,None]
        ipoint[self.Bdcell2dof] = mesh.bc_to_point(bc,index=boundary_cell_index).swapaxes(0,1)


        return ipoint




    def number_of_inner_cell(self):
        return np.copy(self.NCin)

    def number_of_boundary_cell(self):
        return np.copy(self.NCbd)


    def number_of_global_dofs(self):
        NCin = self.NCin
        NCbd = self.NCbd

        in_ldof = self.number_of_local_dofs()
        bd_ldof = self.number_of_local_dofs(is_bd_dof=True)

        return NCin*in_ldof + NCbd*bd_ldof



    def number_of_local_dofs(self,doftype='cell',is_bd_dof=False):
        if doftype in {'cell',2}:
            if is_bd_dof:
                return self.boundary_cell_multiIndex.shape[0]
            else:
                return self.inner_cell_multiIndex.shape[0]
        else:
            return 0


        
    @barycentric
    def value(self, uh, bc, index=np.s_[:]):
        dim = len(uh.shape) - 1

        mesh = self.mesh
        NC = mesh.number_of_cells()
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        
        shape = bc.shape[:-1] + (NC,) + uh.shape[1:]
        val = np.zeros(shape,dtype=np.float64)

        ####################################################
        ################内部单元###############
        inner_cell_index, = np.nonzero(~boundary_cell_flag)
        if len(inner_cell_index) > 0:
            phi = self.basis(bc) #(NQ,1,ldof)
            cell2dof = self.Incell2dof
            if dim == 0:
                val[...,inner_cell_index] = np.einsum('...ij,ij->...i',phi,uh[cell2dof])
            elif dim == 1:
                val[...,inner_cell_index,:] = np.einsum('...ij,ijk->...ik',phi,uh[cell2dof])
            
        ####################################################
        ################边界单元###############
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        if len(boundary_cell_index) > 0:
            phi = self.basis(bc,is_bd_dof=True)
            cell2dof = self.Bdcell2dof
            if dim == 0:
                val[...,boundary_cell_index] = np.einsum('...ij,ij->...i',phi,uh[cell2dof])
            elif dim == 1:
                val[...,boundary_cell_index,:] = np.einsum('...ij,ijk->...ik',phi,uh[cell2dof])
       
        if index is not None: 
           val = val[...,index,:]

        return val



    def interpolation(self,u):
        ipoint = self.interpolation_points()
        uI = u(ipoint)
        return self.function(array=uI)


    def function(self, dim=None, array=None):
        f = Function(self, dim=dim, array=array, coordtype='barycentric')
        return f
        

    def array(self, dim=None):
        gdof = self.number_of_global_dofs()
        if dim in {None, 1}:
            shape = gdof
        elif type(dim) is int:
            shape = (gdof, dim)
        elif type(dim) is tuple:
            shape = (gdof, ) + dim
        return np.zeros(shape, dtype=self.ftype)



    def integral_basis(self):
        gdof = self.number_of_global_dofs()
        c = np.zeros(gdof, dtype=self.ftype)
        boundary_cell_flag = self.mesh.ds.boundary_cell_flag()
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        inner_cell_index, = np.nonzero(~boundary_cell_flag)


        cell2dof = self.Incell2dof 
        qf = self.mesh.integrator(self.inner_p,etype='cell')
        bcs, ws = qf.get_quadrature_points_and_weights()
        phi = self.basis(bcs)
        cc = np.einsum('m,mik,i->ik', ws,phi,self.cellmeasure[inner_cell_index])
        np.add.at(c, cell2dof, cc)



        cell2dof = self.Bdcell2dof
        qf = self.mesh.integrator(self.boundary_p,etype='cell')
        bcs, ws = qf.get_quadrature_points_and_weights()
        phi = self.basis(bcs,is_bd_dof=True)
        cc = np.einsum('m,mik,i->ik', ws,phi,self.cellmeasure[boundary_cell_index])
        np.add.at(c, cell2dof, cc)
        return c

    def source_vector(self, f, celltype=False, q=None):
        """

        Notes
        -----

        TODO:
            1. 处理 f 为向量函数的情形
        """
        mesh = self.mesh
        boundary_cell_flag = mesh.ds.boundary_cell_flag()
        boundary_cell_index, = np.nonzero(boundary_cell_flag)
        inner_cell_index, = np.nonzero(~boundary_cell_flag)



        qf = self.integrator if q is None else mesh.integrator(q, etype='cell')
        bcs, ws = qf.get_quadrature_points_and_weights()
        NQ = len(ws) # 积分点个数
        NC = self.mesh.number_of_cells() # 单元个数
        GD = self.mesh.geo_dimension() # 空间维数，这里假定 NC > GD  

        rm = self.mesh.reference_cell_measure()
        G = self.mesh.first_fundamental_form(bcs)
        d = np.sqrt(np.linalg.det(G))
        ps = mesh.bc_to_point(bcs, etype='cell')


        in_phi = self.basis(bcs)
        bd_phi = self.basis(bcs,is_bd_dof=True)
        d_in = d[:,inner_cell_index]
        d_bd = d[:,boundary_cell_index]

        if callable(f):
            if f.coordtype == 'cartesian':
                f = f(ps)
            elif f.coordtype == 'barycentric':
                f = f(bcs)
        
        

        if isinstance(f, (int, float)): #TODO: 考虑更多的情形
            bb_in = f*np.einsum('q, qci, qc->ci', ws*rm, in_phi, d_in)
            bb_bd = f*np.einsum('q, qci, qc->ci', ws*rm, bd_phi, d_bd)

        elif isinstance(f, np.ndarray): 
            if f.shape == (NC, ):  # (NC, ), 分片常数
                f_in = f[inner_cell_index]
                f_bd = f[boundary_cell_index]
                bb_in = np.einsum('q, c, qci, qc->ci', ws*rm, f_in, in_phi, d_in)
                bb_bd = np.einsum('q, c, qci, qc->ci', ws*rm, f_bd, bd_phi, d_bd)
            elif f.shape == (NQ, NC): # (NQ, NC)
                f_in = f[:,inner_cell_index]
                f_bd = f[:,boundary_cell_index]
                bb_in = np.einsum('q, qc, qci, qc->ci', ws*rm, f_in, in_phi, d_in)
                bb_bd = np.einsum('q, qc, qci, qc->ci', ws*rm, f_bd, bd_phi, d_bd)
            elif f.shape == (NQ, NC, 2): #(NQ, NC, 2)
                f_in = f[:,inner_cell_index,:]
                f_bd = f[:,boundary_cell_index,:]
                bb_in = np.einsum('q, qcj, qci, qc->cij', ws*rm, f_in, in_phi, d_in)
                bb_bd = np.einsum('q, qcj, qci, qc->cij', ws*rm, f_bd, bd_phi, d_bd)
            else:
                raise ValueError("I can not deal with f.shape!")
        else:
            raise ValueError("f is not callable, and is not int, float, or np.ndarray")

        Incell2dof = self.Incell2dof
        Bdcell2dof = self.Bdcell2dof

        gdof = self.number_of_global_dofs()
        if len(bb_bd.shape) == 2:
            F = np.zeros(gdof, dtype=self.ftype)
            np.add.at(F, Incell2dof, bb_in)
            np.add.at(F, Bdcell2dof, bb_bd)
        else:
            shape = (gdof,) + bb_bd.shape[2:]
            F = np.zeros(shape, dtype=self.ftype)
            np.add.at(F, (Incell2dof, np.s_[:]), bb_in)
            np.add.at(F, (Bdcell2dof, np.s_[:]), bb_bd)
        


        return F 

 



if __name__ == '__main__':
    from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
    from fealpy.mesh import TriangleMesh
    from fealpy.pde.linear_elasticity_model2D import Circle_GenLinearElasticitymodel2D
    from fealpy.pde.poisson_Curved_2d import curve_circle
    from fealpy.tools.show import showmultirate, show_error_table
    import matplotlib.pyplot as plt
    import scipy.io as sio
    import sympy as sp


    pi = sp.pi
    sin = sp.sin
    cos = sp.cos
    exp = sp.exp
    ln = sp.ln

    #给定应力算例
    lam = 1
    mu = 1
    x = sp.symbols('x0:2')

    u = [exp(x[0]*x[1])*cos(x[0]),exp(x[1])*sin(x[0]+x[1])]


    pde = Circle_GenLinearElasticitymodel2D(u,x,lam=lam,mu=mu,
            Dirichletbd_n='(0<=theta)&(theta<=2*np.pi)',
            Dirichletbd_t='(0<=theta)&(theta<=2*np.pi)')




    #生成网格
    N = 10
    theta = np.arange(N)*2*np.pi/N
    node = np.zeros((N+1,2),dtype=np.float_)
    node[1:,0] = np.cos(theta)
    node[1:,1] = np.sin(theta)
    cell = np.zeros((N,3),dtype=np.int_)
    cell[:,1] = np.arange(N)+1
    cell[:,2] = np.arange(N)+2
    cell[-1,2] = 1


    curve_circle = curve_circle()

    inner_p = 3
    boundary_p = 5

    maxit = 4
    nrefine = 2


    mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
    mesh.uniform_refine(n=nrefine)


    errorType = ['$||u - u_I||_{0}$']
    Ndof = np.zeros((maxit,))
    errorMatrix = np.zeros((len(errorType), maxit), dtype=np.float64)
    gdim = 2

    for i in range(maxit):
        print("The {}-th computation:".format(i))
        vspace = DGLagrangeFiniteElementSpace_inner_bd(mesh, inner_p, boundary_p)
        vgdof = vspace.number_of_global_dofs()  
        uI = vspace.interpolation(pde.displacement)
        F1 =  -vspace.source_vector(pde.source)

        errorMatrix[0,i] = vspace.integralalg.error(pde.displacement,uI.value)
        Ndof[i] = 3*vgdof

        if (i < maxit-1):
            mesh.uniform_refine()

    show_error_table(Ndof, errorType, errorMatrix)
    showmultirate(plt, 0, Ndof, errorMatrix, errorType)
    plt.show()
    








    










 




    
