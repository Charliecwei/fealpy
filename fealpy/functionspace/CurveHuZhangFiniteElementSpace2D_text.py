import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool as Pool
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve, cg, lgmres, LinearOperator
import pyamg


from fealpy.functionspace.Function import Function
from fealpy.functionspace import ParametricLagrangeFiniteElementSpace
from fealpy.decorator import barycentric
from fealpy.quadrature.Gauss_type_quadrature import Gauss_type_quadrature

class CurveHuZhangFiniteElementSpace_text():
    """
    Curve Hu-Zhang Mixed Finite Element Space 2D, 假设曲边充分光滑，没有角点
    """
    def __init__(self, mesh, p, q=None):
        self.space = ParametricLagrangeFiniteElementSpace(mesh, p, q=q) # the scalar space
        self.mesh = mesh
        self.p = p
        self.dof = self.space.dof
        self.dim = self.space.GD
        self.Gauss_type_quadrature = Gauss_type_quadrature


        self.edof = (p-1)
        self.fdof = (p-1)*(p-2)//2
        self.cdof = (p-1)*(p-2)//2

        self.init_boundary_edge_point()
        self.init_bdedge_basis_coefs()
        self.init_bdcell_basis_coefs()
        self.init_cell_to_dof()
        self.init_edge_to_dof()
        self.init_face_to_dof()
        
        self.init_orth_matrices()
        self.integralalg = self.space.integralalg
        self.integrator = self.integralalg.integrator


    def init_boundary_edge_point(self):
        #边界边上的积分点选取
        mesh = self.mesh
        p = self.p
        k = p + 1

        Gauss_type_quadrature = self.Gauss_type_quadrature

        boundary_edge_index = mesh.ds.boundary_edge_index()
        NEbd = len(boundary_edge_index)

        bdbcs = np.zeros((NEbd,k,2),dtype = np.float64)
        Weights = np.zeros((NEbd,k),dtype = np.float64)

        for i in range(NEbd):
            def W(x):
                x = np.array([x])
                shape = x.shape+(2,)
                bc = np.zeros(shape,dtype=np.float64)
                bc[...,0] = x
                bc[...,1] = 1.0-x
                J = mesh.jacobi_matrix(bc,index=[boundary_edge_index[i]])
                J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
                return J[0]

            Gaus_quad = Gauss_type_quadrature(W, k)
            quadpts, weights = Gaus_quad.Gauss_Lobatto_Quadrature(k=k)

            bdbcs[i,:,0] = 1.0 - quadpts
            bdbcs[i,:,1] = quadpts
            Weights[i] = weights[np.arange(k-1,-1,-1)]

        #也是积分点
        self.bdbcs = bdbcs #(NEbd,p+1,2)
        self.bdweights = Weights #(NEbd,p+1)



        '''
        from fealpy.quadrature.GaussLobattoQuadrature import GaussLobattoQuadrature

        Gauss_Lobatto = GaussLobattoQuadrature(k)
        quadpts, weights = Gauss_Lobatto.get_quadrature_points_and_weights()
        '''

    
    def init_bdedge_basis_coefs(self):
        bdbcs = self.bdbcs #(NEbd,p+1)
        phi0 = self.mesh.shape_function(bdbcs,p=self.p)[...,0,:] #(NEbd,p+1,p+1)
        self.bdedge_coefs = np.linalg.inv(phi0)


    def init_bdcell_basis_coefs(self):
        mesh = self.mesh
        boundary_edge_index = mesh.ds.boundary_edge_index() #(NEbd,)
        p = self.p
        ldof = self.space.number_of_local_dofs()
        bdbcs = self.bdbcs #(NEbd,2)
        NEbd = len(boundary_edge_index)
        isEdgeDof = self.dof.is_on_edge_local_dof()

        idx = np.array([[1,2],[0,2],[0,1]],dtype=np.int8)

        bdbc = mesh.multi_index_matrix[2](self.p)/p
        Tbdbcs = np.zeros((NEbd,ldof,3),dtype=np.float64)
        Tbdbcs += bdbc[None,...]
        


        bdedeg2cell = mesh.ds.edge_to_cell()[boundary_edge_index] #(NEbd,4)

        bdedge = mesh.entity('edge')[boundary_edge_index] #(NEbd,2)
        bdcell = mesh.entity('cell')[bdedeg2cell[:,0]] #(NEbd,3)

        for i in range(3):
            idx_temp, = np.nonzero(bdedeg2cell[:,3] == i)
            if len(idx_temp)>0:
                edge_dof, = np.nonzero(isEdgeDof[:,i])

                Tbdbcs[idx_temp[:,None,None],edge_dof[:,None],idx[i]] =  bdbcs[idx_temp]
                idx_temp1 = np.nonzero(bdcell[idx_temp,idx[i,0]]==bdedge[idx_temp,1])

                if len(idx_temp1)>0:
                    idx_temp2 = idx_temp[idx_temp1]
                    Tbdbcs[idx_temp2][:,edge_dof[:,None],idx[i,[1,0]]] = bdbcs[idx_temp2]

        
        phi0 = self.mesh.shape_function(Tbdbcs,p=self.p)[...,0,:] #(NEbd,ldof,lodf)
        self.bdcell_coefs = np.linalg.inv(phi0) #(NEbd,ldof,ldof)


    def init_orth_matrices(self):
        """
        Initialize the othogonal symetric matrix basis.
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()
        gdof = self.number_of_global_dofs()
        edof = self.edof
        cdof = self.cdof
        p = self.p
        self.Tensor_Frame = np.zeros((gdof,tdim),dtype=np.float_) #self.Tensor_Frame[i,:]表示第i个基函数第标架


        NE = mesh.number_of_edges()
        idx = np.array([(0, 0), (1, 1), (0, 1)])
        TE = np.zeros((p-1,NE, 3, 3), dtype=np.float_)
        self.T = np.array([[(1, 0), (0, 0)], [(0, 0), (0, 1)], [(0, 1), (1, 0)]])

        
        if edof > 0:
            boundary_edge_flag = mesh.ds.boundary_edge_flag()
            inner_edge_index, = np.nonzero(~boundary_edge_flag)
            bc = self.space.multi_index_matrix[1](p)/p #(p+1,2)
            bc = bc[1:-1] #(edof,2)
            TE[:,inner_edge_index] = self.edge_orth_tensor(bc,index=inner_edge_index)

            boundary_edge_index, = np.nonzero(boundary_edge_flag)
            NEbd = len(boundary_edge_index)

            for i in range(NEbd):
                bc = self.bdbcs[i,1:-1] #(edof,2)
                TE[:,[boundary_edge_index[i]]] = self.edge_orth_tensor(bc,index=[boundary_edge_index[i]])
            TE = TE.swapaxes(0, 1) #(NE,edof,3,3)

        base0 = 0

        #顶点标架
        T = np.eye(tdim,dtype=np.float_)
        T[gdim:] = T[gdim:]/np.sqrt(2)
        NN = mesh.number_of_nodes()
        shape = (NN,tdim,tdim)
        self.Tensor_Frame[:NN*tdim] = np.broadcast_to(T[None,:,:],shape).reshape(-1,tdim) #顶点标架
        base0 += tdim*NN

        if edof > 0: #边内部连续自由度标架
            NE = mesh.number_of_edges()
            self.Tensor_Frame[base0:base0+NE*edof*(tdim-1)] = TE[...,1:,:].reshape(-1,tdim)
            base0 += NE*edof*(tdim-1)
        
        if cdof > 0:
            NC = mesh.number_of_cells()
            shape = (NC,cdof,tdim,tdim)
            self.Tensor_Frame[base0:base0+NC*cdof*tdim] = np.broadcast_to(T[None,None,:,:],shape).reshape(-1,tdim) #内部标架
            base0 += NC*cdof*tdim

        #print(base0)
        if edof > 0: #边上不连续标架
            E = (gdim+1)*(gdim)//2
            NC = mesh.number_of_cells()
            TE = TE[:,:,0].reshape(-1,tdim) #(NE*edof,tdim)
            idx, = np.nonzero(self.dof_flags_1()[1])
            c2de = self.space.cell_to_dof()[:,idx] - NN #(NC,E*edof)
            self.Tensor_Frame[base0:base0+NC*E*edof] = TE[c2de].reshape(-1,tdim)


        if True:
            #边界顶点，切法向是固定的
            boundary_edge_index = mesh.ds.boundary_edge_index()
            bdedge = mesh.entity('edge')[boundary_edge_index]
            bc = np.array([[1,0],[0,1]],dtype=np.float_)
            bdTE = self.edge_orth_tensor(bc,index=boundary_edge_index) #(2,NEbd,tdim,tdim)

            for i in range(2):
                bdedge_idx = tdim*bdedge[:,i,None]+np.arange(tdim) #(NEbd,tdim)
                self.Tensor_Frame[bdedge_idx] = bdTE[i]
        

    def edge_orth_tensor(self,bc,index=np.s_[:]):
        #构造边上的张量基
        #bc.shape = (NQ,2)
        mesh = self.mesh
        tdim = self.tensor_dimension()
        t = mesh.edge_unit_tangent(bc,index=index) #(NQ,NEbd,2)
        n = mesh.edge_unit_normal(bc,index=index) #(NQ,NEbd,2)
        shape = t.shape[:-1]+(tdim,tdim)  #(NQ,NEbd,tdim,tdim)
        Tensor_Frame = np.zeros(shape,dtype=np.float64)

        idx = np.array([[0,0],[1,1],[0,1]])
        Tensor_Frame[...,0,:] = np.prod(t[...,idx],axis=-1)
        Tensor_Frame[...,1,:] = np.prod(n[...,idx],axis=-1)
        Tensor_Frame[...,2,:] = (t[...,idx[:,0]]*n[...,idx[:,1]] + t[...,idx[:,1]]*n[...,idx[:,0]])/np.sqrt(2)

        return Tensor_Frame #(NQ,NEbd,tdim,tdim)

      
    def __str__(self):
        return "Hu-Zhang mixed finite element space 2D!"

    def number_of_global_dofs(self):
        """
        """
        p = self.p
        gdim = self.geo_dimension()
        tdim = self.tensor_dimension()

        mesh = self.mesh

        NC = mesh.number_of_cells()
        NN = mesh.number_of_nodes()
        gdof = tdim*NN

        if p > 1:
            edof = self.edof
            NE = mesh.number_of_edges()
            gdof += (tdim-1)*edof*NE # 边内部连续自由度的个数 
            E = mesh.number_of_edges_of_cells() # 单元边的个数
            gdof += NC*E*edof # 边内部不连续自由度的个数 

        if p > 2:
            fdof = self.fdof # 面内部自由度的个数
            if gdim == 2:
                gdof += tdim*fdof*NC

        return gdof 

    def number_of_local_dofs(self):
        ldof = self.dof.number_of_local_dofs()
        tdim = self.tensor_dimension()
        return ldof*tdim

    def cell_to_dof(self):
        return self.cell2dof

    def face_to_dof(self):
        return self.face2dof

    def edge_to_dof(self):
        return self.edge2dof


    def init_cell_to_dof(self):
        """
        构建局部自由度到全局自由度的映射矩阵

        Returns
        -------
        cell2dof : ndarray with shape (NC, ldof*tdim)
            NC: 单元个数
            ldof: p 次标量空间局部自由度的个数
            tdim: 对称张量的维数
        """
        mesh = self.mesh
        NN = mesh.number_of_nodes()
        NE = mesh.number_of_edges()
        NC = mesh.number_of_cells()

        gdim = self.geo_dimension()
        tdim = self.tensor_dimension() # 张量维数
        p = self.p
        dof = self.dof # 标量空间自由度管理对象 
       
        c2d = dof.cell2dof[..., np.newaxis]
        ldof = dof.number_of_local_dofs() # ldof : 标量空间单元上自由度个数
        cell2dof = np.zeros((NC, ldof, tdim), dtype=np.int_) # 每个标量自由度变成 tdim 个自由度
        base0 = 0
        base1 = 0

        dofFlags = self.dof_flags_1() # 把不同类型的自由度区分开来
        idx, = np.nonzero(dofFlags[0]) # 局部顶点自由度的编号
        cell2dof[:, idx, :] = tdim*c2d[:, idx] + np.arange(tdim)
        base1 += tdim*NN # 这是张量自由度编号的新起点
        base0 += NN # 这是标量编号的新起点

        idx, = np.nonzero(dofFlags[1]) # 边内部自由度的编号
        if len(idx) > 0:
            # 0号局部自由度对应的是切向不连续的自由度, 留到后面重新编号
            cell2dof[:, idx, 1:] = base1 + (tdim-1)*(c2d[:, idx] - base0) + np.arange(tdim - 1)
            edof = self.edof
            base1 += (tdim-1)*edof*NE
            base0 += edof*NE


        idx, = np.nonzero(dofFlags[2])
        if len(idx) > 0:           
            cell2dof[:, idx, :] = base1 + tdim*(c2d[:, idx] - base0) + np.arange(tdim)
            cdof = self.cdof
            base1 += tdim*cdof*NC
 
        idx, = np.nonzero(dofFlags[1])
        if len(idx) > 0:
            cell2dof[:, idx, 0] = base1 + np.arange(NC*len(idx)).reshape(NC, len(idx))
            base1+=NC*len(idx)

        self.cell2dof = cell2dof

          
    def init_face_to_dof(self):
        """
        构建局部自由度到全局自由度的映射矩阵
        Returns
        -------
        face2dof : ndarray with shape (NF, ldof,tdim)
            NF: 单元个数
            ldof: p 次标量空间局部自由度的个数
            tdim: 对称张量的维数
        """
        self.face2dof = np.copy(self.edge_to_dof())

    def init_edge_to_dof(self):
            mesh = self.mesh
            NN = mesh.number_of_nodes()
            NE = mesh.number_of_edges()
            NC = mesh.number_of_cells()                    
            gdim = self.geo_dimension()
            tdim = self.tensor_dimension() # 张量维数
            p = self.p
            dof = self.dof # 标量空间自由度管理对象
            e2d = dof.edge_to_dof()[...,np.newaxis]
            ldof = dof.number_of_local_dofs(doftype='edge')
            edge2dof = np.zeros((NE,ldof,tdim),dtype=np.int_)-1# 每个标量自由度变成 tdim 个自由度

            dofFlags = self.edge_dof_falgs_1() # 把不同类型的自由度区分开来
            idx, = np.nonzero(dofFlags[0])# 局部顶点自由度的编号
            edge2dof[:,idx,:] = tdim*e2d[:,idx] + np.arange(tdim)

            base0 = 0
            base1 = 0
            idx, = np.nonzero(dofFlags[1]) # 边内部自由度的编号
            if len(idx)>0:
                base0 += NN # 这是标量编号的新起点
                base1 += tdim*NN # 这是张量自由度编号的新起点
                #  0号局部自由度对应的是切向不连续的自由度, 此部分点乘外法向量为0,不算是边界，可以不用编号，已经被标记为-1
                edge2dof[:,idx,1:] = base1+(tdim-1)*(e2d[:, idx] - base0) + np.arange(tdim - 1)

                #边界边第0号自由度
                boundary_edge_index = mesh.ds.boundary_edge_index()
                bdedge2cell = mesh.ds.edge_to_cell()[boundary_edge_index]
                bdcell2dof = self.cell_to_dof()[bdedge2cell[:,0],:,0]

                dofFlags, = np.nonzero(self.dof_flags_1()[1])
                bdcell2dof = bdcell2dof[:,dofFlags] #(NEbd,edof*3)

                dofFlags = self.dof_flags()[1][dofFlags]

                NEbd = len(boundary_edge_index)
                bdedge = mesh.entity('edge')[boundary_edge_index] #(NEbd,2)
                bdcell = mesh.entity('cell')[bdedge2cell[:,0]] #(NEbd,3)
                index_temp = np.array([[1,2],[0,2],[0,1]],dtype=np.int8)



                
                idx_inv = idx[np.arange(len(idx),0,-1)-1]


                for i in range(NEbd):
                    idx_temp, = np.nonzero(dofFlags[:,bdedge2cell[i,2]])
                    dof_temp = bdcell2dof[i,idx_temp]
                    if np.prod(bdedge[i] == bdcell[i,index_temp[bdedge2cell[i,2]]]):
                        edge2dof[boundary_edge_index[i],idx,0] = dof_temp
                    elif np.prod(bdedge[i,[1,0]] == bdcell[i,index_temp[bdedge2cell[i,2]]]):
                        edge2dof[boundary_edge_index[i],idx_inv,0] = dof_temp
                    else:
                        raise ValueError('边和面不对应')

                
                #print(bdcell2dof)

                #print(edge2dof[boundary_edge_index[:,None],idx[None,:],0])


            self.edge2dof = edge2dof

    def geo_dimension(self):
        return self.dim

    def tensor_dimension(self):
        dim = self.dim
        return dim*(dim - 1)//2 + dim

    def interpolation_points(self):

        ipoints = self.dof.interpolation_points()
        mesh = self.mesh
        boundary_edge_index = mesh.ds.boundary_edge_index()
        bde2d = self.space.edge_to_dof()[boundary_edge_index]
        NEbd = len(boundary_edge_index)

        bdbcs = self.bdbcs

        for i in range(NEbd):
            ipoints[bde2d[i],None] = mesh.bc_to_point(bdbcs[i],index=[boundary_edge_index[i]])



        return ipoints

    def dof_flags(self):
        """ 对标量空间中的自由度进行分类, 分为边内部自由度, 面内部自由度(如果是三维空间的话)及其它自由度 

        Returns
        -------

        isOtherDof : ndarray, (ldof,)
            除了边内部和面内部自由度的其它自由度
        isEdgeDof : ndarray, (ldof, 3) or (ldof, 6) 
            每个边内部的自由度
        isFaceDof : ndarray, (ldof, 4)
            每个面内部的自由度
        -------

        """
        dim = self.geo_dimension()
        dof = self.dof 
        
        isPointDof = dof.is_on_node_local_dof()
        isEdgeDof = dof.is_on_edge_local_dof()
        isEdgeDof[isPointDof] = False
        
        isEdgeDof0 = np.sum(isEdgeDof, axis=-1) > 0 # 
        isOtherDof = (~isEdgeDof0) # 除了边内部自由度之外的其它自由度
                                   # dim = 2: 包括点和面内部自由度
        if dim == 2:
            return isOtherDof, isEdgeDof
        else:
            raise ValueError('`dim` should be 2 !')

    def dof_flags_1(self):
        """ 
        对标量空间中的自由度进行分类, 分为:
            点上的自由由度
            边内部的自由度
            面内部的自由度
            体内部的自由度

        Returns
        -------

        """
        gdim = self.geo_dimension() # the geometry space dimension
        dof = self.dof 
        isPointDof = dof.is_on_node_local_dof()
        isEdgeDof = dof.is_on_edge_local_dof()
        isEdgeDof[isPointDof] = False
        isEdgeDof0 = np.sum(isEdgeDof, axis=-1) > 0
        if gdim == 2:
            return isPointDof, isEdgeDof0, ~(isPointDof | isEdgeDof0)
        else:
            raise ValueError('`dim` should be 2!')

    def face_dof_falgs(self):
        """
        对标量空间中面上的基函数自由度进行分类，分为：
            点上的自由由度
            边内部的自由度
            面内部的自由度        
        """
        p = self.p
        gdim = self.geo_dimension()
        if gdim == 2:
            return self.edge_dof_falgs()
        else:
            raise ValueError('`dim` should be 2!')

    def face_dof_falgs_1(self):
        """
        对标量空间中面上的基函数自由度进行分类，分为：
            点上的自由由度
            边内部的自由度
            面内部的自由度        
        """
        p = self.p
        gdim = self.geo_dimension()
        if gdim == 2:
            return self.edge_dof_falgs_1()
        else:
            raise ValueError('`dim` should be 2!')

    def edge_dof_falgs(self):
        """
        对标量空间中边上的基函数自由度进行分类，分为：
            点上的自由由度 
        """ 
        p = self.p
        TD = 1
        multiIndex = self.space.multi_index_matrix[TD](p)#(ldof,2)
        isPointDof = (np.sum(multiIndex == p, axis=-1) > 0)    
        isEdgeDof0  = ~isPointDof
        return isPointDof, isEdgeDof0    

    def edge_dof_falgs_1(self):
        """
        对标量空间中边上的基函数自由度进行分类，分为：
            点上的自由由度 
        """ 
        p = self.p
        TD = 1
        multiIndex = self.space.multi_index_matrix[TD](p)#(ldof,2)
        isPointDof = (np.sum(multiIndex == p, axis=-1) > 0)    
        isEdgeDof0  = ~isPointDof
        return isPointDof, isEdgeDof0    




    @barycentric
    def face_basis(self,bc,index=np.s_[:]):
        gdim = self.geo_dimension()
        if gdim == 2:
            return self.edge_basis(bc,index=index)
        else:
            raise ValueError('`dim` should be 2!')


    @barycentric
    def edge_basis(self,bc,index=np.s_[:]):
        phi0 = self.space.face_basis(bc) #(NQ,1,ldof)      
        if False:
            edge2dof = self.edge2dof[index] #(NE,ldof,tdim)
            phi = np.einsum('nijk,...ni->...nijk',self.Tensor_Frame[edge2dof],phi0) #(NE,ldof,tdim,tdim), (NQ,1,ldof)
            #在不连续标架算出的结果不对，但是不影响，因为其自由度就是定义在单元体上的
            #不连续标架有:边内部第0个标架
        else:
            NE = self.mesh.number_of_edges()
            index = np.arange(NE)[index] #(NE,)
            edge2dof = self.edge2dof[index] #(NE,ldof,tdim)
            
            Tensor_Frame = self.Tensor_Frame[edge2dof] #(NE,ldof,tdim,tdim)
            phi = np.einsum('nijk,...ni->...nijk',Tensor_Frame,phi0) #(NQ,NEbd,ldof,tdim,tdim)

            
            boundary_edge_index = self.mesh.ds.boundary_edge_index()
            i,j = np.where(index[:,None]==boundary_edge_index)

            if len(j)>0:
                bdedge_coefs = self.bdedge_coefs[j] #(NEbd,ldof,ldof)
                phi0 = np.einsum('...ni,nil->...nl',phi0,bdedge_coefs) #(NQ,NEbd,ldof)
                phi[...,i,:,:,:] = np.einsum('nijk,...ni->...nijk',Tensor_Frame[i],phi0)

                
        return phi #(NQ,NE,ldof,tdim,tdim)  


    @barycentric
    def basis(self, bc, index=np.s_[:],p=None):
        """
        Parameters
        ----------
        bc : ndarray with shape (NQ, dim+1)
            bc[i, :] is i-th quad point
        index : ndarray
            有时我我们只需要计算部分单元上的基函数
        Returns
        -------
        phi : ndarray with shape (NQ, NC, ldof, tdim, 3 or 6)
            NQ: 积分点个数
            NC: 单元个数
            ldof: 标量空间的单元自由度个数
            tdim: 对称张量的维数
        """
        mesh = self.mesh

        gdim = self.geo_dimension() 
        tdim = self.tensor_dimension()
        phi0 = self.space.basis(bc) #(NQ,1,ldof)

        if False:
            cell2dof = self.cell2dof[index] #(NC,ldof,tdim)
            phi = np.einsum('nijk,...ni->...nijk',self.Tensor_Frame[cell2dof],phi0) #(NC,ldof,tdim,tdim), (NQ,1,ldof)
        else:
            NC = self.mesh.number_of_cells()
            index = np.arange(NC)[index]
            cell2dof = self.cell2dof[index] #(NC,ldof,tdim)
            Tensor_Frame = self.Tensor_Frame[cell2dof] #(NC,ldof,tdim,tdim)

            phi = np.einsum('nijk,...ni->...nijk',Tensor_Frame,phi0)

            boundary_cell_index = mesh.ds.edge_to_cell()[mesh.ds.boundary_edge_index(),0] #(NEbd,)

            i, j = np.where(index[:,None]==boundary_cell_index)

            if len(j)>0:
                bdcell_coefs = self.bdcell_coefs[j] #(NEbd,ldof,ldof)
                phi0 = np.einsum('...ni,nil->...nl',phi0,bdcell_coefs) #(NQ,NEbd,ldof)
                phi[...,i,:,:,:] = np.einsum('nijk,...ni->...nijk',Tensor_Frame[i],phi0)

                    
        return phi  #(NQ,NC,ldof,tdim,tdim) 最后一个维度表示tensor



    @barycentric
    def div_basis(self, bc, index=np.s_[:]):
        mesh = self.mesh

        gdim = self.geo_dimension()
        tdim = self.tensor_dimension() 

        # the shape of `gphi` is (NQ, NC, ldof, gdim)
        if False:
            gphi = self.space.grad_basis(bc, index=index) 
            cell2dof = self.cell2dof[index]
            shape = list(gphi.shape)
            shape.insert(-1, tdim)
            # the shape of `dphi` is (NQ, NC, ldof, tdim, gdim)

            VAL = np.einsum('iljk,kmn->iljmn',self.Tensor_Frame[cell2dof],self.T) #(NC,ldof,tdim,gdim,gdim)
            dphi = np.einsum('...ikm,ikjmn->...ikjn',gphi,VAL) #(NQ,NC,ldof,gdim), (NC,ldof,tdim,gdim,gdim)
            #print(dphi.shape)
        else:
            NC = self.mesh.number_of_cells()
            index = np.arange(NC)[index]
            cell2dof = self.cell2dof[index] #(NC,ldof,tdim)

            gphi = self.space.grad_basis(bc, index=index) #(NQ, NC, ldof, gdim)
            Tensor_Frame = np.einsum('iljk,kmn->iljmn',self.Tensor_Frame[cell2dof],self.T)
            dphi = np.einsum('...ikm,ikjmn->...ikjn',gphi,Tensor_Frame)

            boundary_cell_index = mesh.ds.edge_to_cell()[mesh.ds.boundary_edge_index(),0] #(NEbd,)
            i, j = np.where(index[:,None]==boundary_cell_index)

            if len(j)>0:
                bdcell_coefs = self.bdcell_coefs[j] #(NEbd,ldof,ldof)
                gphi = np.einsum('...nik,nij->...njk',gphi[...,i,:,:],bdcell_coefs) #(NQ,NEbd,ldof,gdim)
                dphi[...,i,:,:,:] = np.einsum('...ikm,ikjmn->...ikjn',gphi,Tensor_Frame[i])



        return dphi #(NQ,NC,ldof,tdim,gdim)



    @barycentric
    def value(self, uh, bc, index=np.s_[:]):
        phi = self.basis(bc, index=index)
        cell2dof = self.cell_to_dof()
        uh = uh[cell2dof[index]]
        val = np.einsum('...ijkm, ijk->...im', phi, uh) #(NQ,NC,tdim)
        val = np.einsum('...k, kmn->...mn', val, self.T)
        return val #(NQ,NC,gdim,gdim)    



    @barycentric
    def div_value(self, uh, bc, index=np.s_[:]):
        dphi = self.div_basis(bc, index=index) #(NQ,NC,ldof,tdim,gdim)
        cell2dof = self.cell_to_dof()
        uh = uh[cell2dof[index]]
        val = np.einsum('...ijkm, ijk->...im', dphi, uh)
        return val #(NQ,NC,gdim)


    def compliance_tensor_matrix(self,mu=1,lam=1):
        ldof = self.number_of_local_dofs()
        tdim = self.tensor_dimension()
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        NQ = bcs.shape[0]

        phi = self.basis(bcs).reshape(NQ,NC,-1,tdim)#(NQ,NC,ldof,tdim)
        #compliance_tensor
        aphi = phi.copy()
        t = np.sum(aphi[..., 0:gdim], axis=-1)
        aphi[..., 0:gdim] -= lam/(2*mu+gdim*lam)*t[..., np.newaxis]
        aphi /= 2*mu

        #construct matrix
        d = np.array([1, 1, 2])
        rm = self.mesh.reference_cell_measure()
        D = self.mesh.first_fundamental_form(bcs)
        D = np.sqrt(np.linalg.det(D)) #(NQ,NC)

        M = np.einsum('i, ijkm, m, ijom, ij->jko', ws*rm, aphi, d, phi, D, optimize=True)

        I = np.einsum('ij, k->ijk', self.cell2dof.reshape(NC,-1), np.ones(ldof))
        J = I.swapaxes(-1, -2)
        tgdof = self.number_of_global_dofs()

        M = csr_matrix((M.flat, (I.flat, J.flat)), shape=(tgdof, tgdof))

        return M    



    def div_matrix(self,vspace):
        '''

        Notes
        -----
        (div tau, v)

        gdim == 2
        v= [[phi,0],[0,phi]]

        [[B0],[B1]]

        '''
        tldof = self.number_of_local_dofs()
        vldof = vspace.number_of_local_dofs()
        tgdof = self.number_of_global_dofs()
        vgdof = vspace.number_of_global_dofs()
        
        gdim = self.geo_dimension()
        bcs, ws = self.integrator.quadpts, self.integrator.weights
        NC = self.mesh.number_of_cells()
        NQ = bcs.shape[0]

        dphi = self.div_basis(bcs).reshape(NQ,NC,-1,gdim) #(NQ, NC, ldof*tdim, gdim)
        vphi = vspace.basis(bcs)#(NQ,1,vldof)

        D = self.mesh.first_fundamental_form(bcs)
        D = np.sqrt(np.linalg.det(D))
        rm = self.mesh.reference_cell_measure()


        B0 = np.einsum('i,ijk,ijo,ij->jko',ws*rm,vphi,dphi[...,0],D, optimize=True)
        B1 = np.einsum('i,ijk,ijo,ij->jko',ws*rm,vphi,dphi[...,1],D, optimize=True)


        I = np.einsum('ij, k->ijk', vspace.cell_to_dof(), np.ones(tldof,dtype=int))
        J = np.einsum('ij, k->ikj', self.cell_to_dof().reshape(NC,-1), np.ones(vldof,dtype=int))   

        B0 = csr_matrix((B0.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))
        B1 = csr_matrix((B1.flat, (I.flat, J.flat)), shape=(vgdof, tgdof))

        return B0, B1



    def interpolation(self, u):
        ipoint = self.interpolation_points()
        c2d = self.dof.cell2dof
        val = u(ipoint)[c2d][:,:,None] #u 是一个tensor val.shape = (NC,ldof,1,gdim,gidm)
        cell2dof = self.cell2dof #(NC,ldof,tdim)

        uI = Function(self)
        Tensor_Frame = np.einsum('...k,kmn->...mn',self.Tensor_Frame,self.T) #(gdof,gdim,gdim)
        uI[cell2dof] = np.einsum('...mn,...mn->...',Tensor_Frame[cell2dof],val)
        return uI


    def function(self, dim=None):
        f = Function(self)
        return f


    def array(self, dim=None):
        gdof = self.number_of_global_dofs()
        return np.zeros(gdof, dtype=np.float)



    def set_essential_bc(self, uh, gN, threshold=None):
        """
        初始化压力的本质边界条件，插值一个边界sigam,使得sigam*n=gN
        由face2bddof 形状为(NFbd,ldof,tdim)
        2D case 时face2bddof[...,0]--切向标架， face2bddof[...,1]--法向标架， face2bddof[...,2]--切法向组合标架
        """
        mesh = self.mesh
        gdof = self.number_of_global_dofs()
        

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = mesh.entity_barycenter('face',index=index)
                flag = threshold(bc) #(2,gNEbd), 第0行表示给的法向投影，第1行分量表示给的切向投影
                flag_idx = (np.sum(flag,axis=0)>0) #(gNFbd,)
                index = index[flag_idx]#(NFbd,)
                NEbd = len(index)

                bd_index_type = np.zeros((2,NEbd),dtype=np.bool)
                bd_index_type[0] = flag[0][flag_idx] #第0个分量表示给的法向投影
                bd_index_type[1] = flag[1][flag_idx] #第1个分量表示给的切向投影

        p = self.p
        boundary_edge_index = mesh.ds.boundary_edge_index()

        i,j = np.where(index[:,None]==boundary_edge_index)

        bdbcs = self.bdbcs[j]
        eldof = bdbcs.shape[1]
        n = np.zeros((NEbd,eldof,2))
        t = np.zeros((NEbd,eldof,2))

        for i in range(NEbd):
            bc = bdbcs[i] #(eldof,2)
            n[i] = mesh.edge_unit_normal(bc,index=[index[i]])[:,0] #(eldof,NEbd,2)
            t[i] = mesh.edge_unit_tangent(bc,index=[index[i]])[:,0] #(eldof,NEbd,2)

        
        
        isBdDof = np.zeros(gdof,dtype=np.bool)        
        f2dbd = self.dof.face_to_dof()[index]
        ipoint = self.interpolation_points()[f2dbd] #(NEbd,eldof)


        facebd2dof = self.face2dof[index,:,1:] #(NEbd,eldof,tdim-1) 第一个是t*t^T,为自由边界

        val = gN(ipoint,n,t=t) #(NEbd,eldof,gdim)，可能是法向，也可能是切向，或者两者的线性组合
        bdTensor_Frame = self.Tensor_Frame[facebd2dof] #(NEbd,eldof,tdim-1,tdim)
        bdTensor_Frame = np.einsum('ijkl,lmn,ijn->ijkm',bdTensor_Frame,self.T,n)#(NEbd,eldof,tdim-1,gdim)

        

        ##法向分量
        bd_index_temp, = np.nonzero(bd_index_type[0]) 
        if len(bd_index_temp)>0:
            bdTensor_Frame_projection = np.einsum('ijk,ijk->ij',bdTensor_Frame[bd_index_temp,:,0],n[bd_index_temp])#(NEbd,eldof)
            val_projection = np.einsum('ijk,ijk->ij',val[bd_index_temp],n[bd_index_temp]) #(NEbd,eldof)
            #print(uh[facebd2dof[bd_index_temp,:,0]].shape)
            uh[facebd2dof[bd_index_temp,:,0]] = val_projection/bdTensor_Frame_projection
            isBdDof[facebd2dof[bd_index_temp,:,0]] = True

        ##切向分量
        
        bd_index_temp, = np.nonzero(bd_index_type[1])
        if len(bd_index_temp)>0:
            bdTensor_Frame_projection = np.einsum('ijk,ijk->ij',bdTensor_Frame[bd_index_temp,:,1],t[bd_index_temp])#(NEbd,eldof)
            val_projection = np.einsum('ijk,ijk->ij',val[bd_index_temp],t[bd_index_temp]) #(NEbd,eldof)
            uh[facebd2dof[bd_index_temp,:,1]] = val_projection/bdTensor_Frame_projection
            isBdDof[facebd2dof[bd_index_temp,:,1]] = True

             

        return isBdDof



    def Penalty_essential_bc(self,gN,F,threshold=None):
        """
        初始化压力的本质边界条件，mu^{-1}u+sigma*n=g_n, 当mu区域正无穷时，
        得到sigma*n=g_n
        """
        gama = 1.0
        sigma = 4


        mesh = self.mesh
        gdof = self.number_of_global_dofs()
        gdim = self.geo_dimension()
        

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = self.mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = self.mesh.entity_barycenter('face',index=index)
                flag = threshold(bc)
                flag = (np.sum(flag,axis=0)>0)
                index = index[flag]
                NEbd = len(index)

        if NEbd > 0:
            bd2dof = self.edge_to_dof()[index] #(NEbd,eldof,tdim)
            eldof = bd2dof.shape[1]
            tdim = self.tensor_dimension()

            from fealpy.quadrature.GaussLegendreQuadrature import GaussLegendreQuadrature
            qf = GaussLegendreQuadrature(self.p+1)

            bcs, ws = qf.get_quadrature_points_and_weights()
            NQ = len(ws)

            phi = self.edge_basis(bcs,index=index) #(NQ,NEbd,eldof,tdim,tdim)


            shape = list(phi.shape)
            shape[-1] = gdim
            phin = np.zeros(shape,dtype=float) #sigam*n, (NQ,NEbd,ldof,tdim,gdim)

            n = mesh.edge_unit_normal(bcs,index=index) #(NQ,NEbd,gdim)

            phin[...,0] = np.einsum('...ijlk,...ik->...ijl',phi[...,[0,2]],n)
            phin[...,1] = np.einsum('...ijlk,...ik->...ijl',phi[...,[2,1]],n)


            pp = mesh.bc_to_point(bcs,index=index) #(NQ,NEbd,gdim)
            t = mesh.edge_unit_tangent(bcs,index=index) #(NQ,NEbd,gidm)




            val = gN(pp,n,t=t) #(NQ,NEbd,gdim)


            phin = phin.reshape(NQ,NEbd,-1,gdim)
            bd2dof = bd2dof.reshape(NEbd,-1)

            rm = mesh.reference_cell_measure(TD=1)
            D = mesh.first_fundamental_form(bcs,index=index)
            D = np.sqrt(np.linalg.det(D))#(NQ,NEbd)
            bb = np.einsum('m,mil,mijl,mi->ij', ws*rm, val, phin, D) #(NFbd,ldof)

            he = mesh.edge_length(index=index)
            print(np.max(he))
            he = he**sigma
            print(np.max(he))
            mu = gama/he[:,None]

            bb = mu*bb

            np.add.at(F,bd2dof,bb)

        
            
            
            M = np.einsum('m,mijk,milk,mi->ijl',ws*rm,phin,phin,D)
            mu = mu[:,None]
            M = mu*M
            shape = bd2dof.shape+(bd2dof.shape[-1],)

            I = np.broadcast_to(bd2dof[:,:,None],shape=shape)
            J = np.broadcast_to(bd2dof[:,None,:],shape=shape)


            M = csr_matrix((M.flat, (I.flat, J.flat)), shape=(gdof, gdof))

        else:
            M = 0

        return M



    def set_essential_bc_Penalty(self, gN,F, M, threshold=None):
        """
        初始化压力的本质边界条件，插值一个边界sigam,使得sigam*n=gN
        由face2bddof 形状为(NFbd,ldof,tdim)
        2D case 时face2bddof[...,0]--切向标架， face2bddof[...,1]--法向标架， face2bddof[...,2]--切法向组合标架
        """
        mesh = self.mesh
        gdof = self.number_of_global_dofs()

        M += self.Penalty_essential_bc(gN, F,threshold=threshold)
        

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = mesh.entity_barycenter('face',index=index)
                flag = threshold(bc) #(2,gNEbd), 第0行表示给的法向投影，第1行分量表示给的切向投影
                flag_idx = (np.sum(flag,axis=0)>0) #(gNFbd,)
                index = index[flag_idx]#(NFbd,)
                NEbd = len(index)

                bd_index_type = np.zeros((2,NEbd),dtype=np.bool)
                bd_index_type[0] = flag[0][flag_idx] #第0个分量表示给的法向投影
                bd_index_type[1] = flag[1][flag_idx] #第1个分量表示给的切向投影

        p = self.p
        boundary_edge_index = mesh.ds.boundary_edge_index()

        i,j = np.where(index[:,None]==boundary_edge_index)

        bdbcs = self.bdbcs[j]
        eldof = bdbcs.shape[1]
        n = np.zeros((NEbd,eldof,2))
        t = np.zeros((NEbd,eldof,2))

        for i in range(NEbd):
            bc = bdbcs[i] #(eldof,2)
            n[i] = mesh.edge_unit_normal(bc,index=[index[i]])[:,0] #(eldof,NEbd,2)
            t[i] = mesh.edge_unit_tangent(bc,index=[index[i]])[:,0] #(eldof,NEbd,2)

        
        
        isBdDof = np.zeros(gdof,dtype=np.bool)        
        f2dbd = self.dof.face_to_dof()[index]
        ipoint = self.interpolation_points()[f2dbd] #(NEbd,eldof)


        facebd2dof = self.face2dof[index,:,1:] #(NEbd,eldof,tdim-1) 第一个是t*t^T,为自由边界

        val = gN(ipoint,n,t=t) #(NEbd,eldof,gdim)，可能是法向，也可能是切向，或者两者的线性组合
        bdTensor_Frame = self.Tensor_Frame[facebd2dof] #(NEbd,eldof,tdim-1,tdim)
        bdTensor_Frame = np.einsum('ijkl,lmn,ijn->ijkm',bdTensor_Frame,self.T,n)#(NEbd,eldof,tdim-1,gdim)

        

        ##法向分量
        bd_index_temp, = np.nonzero(bd_index_type[0]) 
        if len(bd_index_temp)>0:
            bdTensor_Frame_projection = np.einsum('ijk,ijk->ij',bdTensor_Frame[bd_index_temp,:,0],n[bd_index_temp])#(NEbd,eldof)
            val_projection = np.einsum('ijk,ijk->ij',val[bd_index_temp],n[bd_index_temp]) #(NEbd,eldof)
            #print(uh[facebd2dof[bd_index_temp,:,0]].shape)
            uh[facebd2dof[bd_index_temp,:,0]] = val_projection/bdTensor_Frame_projection
            isBdDof[facebd2dof[bd_index_temp,:,0]] = True

        ##切向分量
        
        bd_index_temp, = np.nonzero(bd_index_type[1])
        if len(bd_index_temp)>0:
            bdTensor_Frame_projection = np.einsum('ijk,ijk->ij',bdTensor_Frame[bd_index_temp,:,1],t[bd_index_temp])#(NEbd,eldof)
            val_projection = np.einsum('ijk,ijk->ij',val[bd_index_temp],t[bd_index_temp]) #(NEbd,eldof)
            uh[facebd2dof[bd_index_temp,:,1]] = val_projection/bdTensor_Frame_projection
            isBdDof[facebd2dof[bd_index_temp,:,1]] = True


             

        return isBdDof



    
    def set_nature_bc(self, gD, threshold=None, q=None):
        """
        设置 natural边界条件到右端项中，由于是混合元，故此时为u的gD自由边界条件
        若对应应力边界未设置，则默认该边界方向u为0
        """
        mesh = self.mesh
        gdim = self.geo_dimension()
        gdof = self.number_of_global_dofs()

        if type(threshold) is np.ndarray:
            index = threshold
        else:
            index = self.mesh.ds.boundary_face_index()
            if threshold is not None:
                bc = self.mesh.entity_barycenter('face',index=index)
                flag = threshold(bc)
                flag = (np.sum(flag,axis=0)>0)
                index = index[flag]


        bd2dof = self.edge_to_dof()[index] #(NEbd,ldof,tdim)

        #qf = self.integralalg.faceintegrator if q is None else mesh.integrator(q, 'face')
        #qf = mesh.integrator(self.p+1,'face')
        from fealpy.quadrature.GaussLobattoQuadrature import GaussLobattoQuadrature
        from fealpy.quadrature.GaussLegendreQuadrature import GaussLegendreQuadrature
        #qf = GaussLobattoQuadrature(np.ceil((3*self.p+1)/2))
        #qf = GaussLobattoQuadrature(self.p+1)
        qf = GaussLegendreQuadrature(self.p+1)
        bcs, ws = qf.get_quadrature_points_and_weights()





        phi = self.face_basis(bcs,index=index) #(NQ,NEbd,eldof,tdim,tdim)

        

        shape = list(phi.shape)
        shape[-1] = gdim
        phin = np.zeros(shape,dtype=float) #sigam*n, (NQ,NEbd,ldof,tdim,gdim)

        n = mesh.edge_unit_normal(bcs,index=index) #(NQ,NEbd,gdim)

        phin[...,0] = np.einsum('...ijlk,...ik->...ijl',phi[...,[0,2]],n)
        phin[...,1] = np.einsum('...ijlk,...ik->...ijl',phi[...,[2,1]],n)



        pp = mesh.bc_to_point(bcs,index=index) #(NQ,NEbd,gdim)

        t = mesh.edge_unit_tangent(bcs,index=index) #(NQ,NEbd,gidm)

        val = gD(pp,n=n,t=t) #(NQ,NFbd,gdim) 此时gD函数,可能给法向分量，也可能给切向分量，具体形式在gD中体现


        rm = mesh.reference_cell_measure(TD=1)
        D = mesh.first_fundamental_form(bcs,index=index)
        D = np.sqrt(np.linalg.det(D))#(NQ,NEbd)
        bb = np.einsum('m,mil,mijkl,mi->ijk', ws*rm, val, phin, D) #(NFbd,ldof,tdim)
        
        F = np.zeros(gdof,dtype=mesh.ftype)
        np.add.at(F,bd2dof,bb)



        #数值积分计算正确性检验
        '''
        boundary_edge_index = mesh.ds.boundary_edge_index()
        i = 0
        def W(x):
            x = np.array([x])
            shape = x.shape+(2,)
            bc = np.zeros(shape,dtype=np.float64)
            bc[...,0] = x
            bc[...,1] = 1.0-x
            J = mesh.jacobi_matrix(bc,index=[boundary_edge_index[i]])
            J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
            return J[0]

        k = self.p+1
        Gaus_quad = Gauss_type_quadrature(W, k)
        

        def f(x):
            x = np.array([x])
            shape = x.shape+(2,)
            bc = np.zeros(shape,dtype=np.float64)
            bc[...,0] = x
            bc[...,1] = 1.0-x
            phi = self.face_basis(bc,index=[boundary_edge_index[i]]) #(NQ,NEbd,ldof,tdim,tdim)
            n =  mesh.edge_unit_normal(bc,index=[boundary_edge_index[i]]) #(NQ,NEbd,gdim)
            t = mesh.edge_unit_tangent(bc,index=[boundary_edge_index[i]]) #(NQ,NEbd,gidm)

            shape = list(phi.shape)
            shape[-1] = gdim
            phin = np.zeros(shape,dtype=float) #sigam*n, (NQ,NEbd,ldof,tdim,gdim)

            phin[...,0] = np.einsum('...ijlk,...ik->...ijl',phi[...,[0,2]],n)
            phin[...,1] = np.einsum('...ijlk,...ik->...ijl',phi[...,[2,1]],n)

            phin = phin[...,0,0,:] #(NQ,NEbd,gdim)

            pp = mesh.bc_to_point(bc,index=[boundary_edge_index[i]])
            val = gD(pp,n=n,t=t) #(NQ,NFbd,gdim)

            val = np.einsum('...j,...j->...',phin,val)


            return val[0,0]

        val = Gaus_quad.integrate_weight(f)
        print(val)

        ss
        '''

        





       
        return F








                





















if __name__ == '__main__':
    from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
    from fealpy.quadrature.Gauss_type_quadrature import Gauss_type_quadrature
    from fealpy.mesh import TriangleMesh
    from fealpy.functionspace.HuZhangFiniteElementSpace2D import HuZhangFiniteElementSpace
    from fealpy.pde.poisson_Curved_2d import curve_circle
    import matplotlib.pyplot as plt
    import scipy.io as sio



    if True:

        #node and cell
        N = 3
        theta = np.arange(N)*2*np.pi/N
        node = np.zeros((N+1,2),dtype=np.float_)
        node[1:,0] = np.cos(theta)
        node[1:,1] = np.sin(theta)
        cell = np.zeros((N,3),dtype=np.int_)
        cell[:,1] = np.arange(N)+1
        cell[:,2] = np.arange(N)+2
        cell[-1,2] = 1
    else:
        i = 3
        Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
        Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']

        node = Node[i,0]
        cell = np.array(Cell[i,0],dtype=np.int_)

    curve_circle = curve_circle()

    mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)
    mesh1 = TriangleMesh(node,cell)

    #mesh.uniform_refine(n=4)
    #mesh1.uniform_refine(n=4)

    p = 3

    space = CurveHuZhangFiniteElementSpace_text(mesh,p=p)
    space1 = HuZhangFiniteElementSpace(mesh1,p=p)

    
    NN = mesh.number_of_nodes()
    NE = mesh.number_of_edges()
    NC = mesh.number_of_cells()

    tdim = space.tensor_dimension()

    cell2dof = space.cell_to_dof()#(NC, ldof,tdim)

    if False:

        #print(mesh.entity('cell')[0])
        #print(mesh.ds.cell_to_edge()[0])
        #print(space.mesh.multi_index_matrix[2](p))
        #bc = np.array([[2/3,1/3],[1/3,2/3]],dtype=np.float_)
        #bc = np.array([[1,0],[0,1]],dtype=np.float_)



        t = mesh.edge_unit_tangent(bc,index=[3])[:,0] #(2,2)
    


    
        n = t[:,[1,0]]
        n[:,1] *= -1



        Frame = np.zeros((2,tdim,tdim))

        Frame[:,0] = t[:,[0,1,0]]*t[:,[0,1,1]]
        Frame[:,1] = n[:,[0,1,0]]*n[:,[0,1,1]]
        Frame[:,2,:] = (t[:,[0,1,0]]*n[:,[0,1,1]]+n[:,[0,1,0]]*t[:,[0,1,1]])/np.sqrt(2)

        print(mesh.entity('cell')[2])

        print(Frame,'\n\n\n')

        print(space.Tensor_Frame[cell2dof[0,[7,8]]])


    if  False:
        gdim = 2
        bc = np.array([[1/2,1/2],
                        [1,0],
                        [0,1],
                        [1/3,2/3]])
        phie = space.edge_basis(bc,index=[3,4,5]) #(NQ,NE,ldof,tdim,tdim)
        print(phie.shape)
        n = mesh.edge_unit_normal(bc,index=[3,4,5]) #(NQ,NE,gdim)
 
        shape = phie.shape[:-1]+(gdim,) #(NQ,NE,ldof,tdim,gdim)

        phin = np.zeros(shape,dtype=np.float64) #(NQ,NE,ldof,tdim,gdim)

        phin[...,0] = np.einsum('...ijk,...k->...ij',phie[...,[0,2]],n)
        phin[...,1] = np.einsum('...ijk,...k->...ij',phie[...,[2,1]],n)

        print(np.max(np.abs(phin[...,0,:])))


        

    if False:

        def W(x):
            if type(x) is float:
                x = np.array([x])
            shape = x.shape+(2,)
            bc = np.zeros(shape,dtype=np.float64)
            bc[...,0] = x
            bc[...,1] = 1.0-x
            J = mesh.jacobi_matrix(bc,index=[5])
            J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
            return J[0] 

        def p(x):
            return x**9


        k = 6
        Gaus_quad = Gauss_type_quadrature(W, k)
        quadpts, weights = Gaus_quad.Gauss_Lobatto_Quadrature(k=6)

        print(quadpts)
        print(Gaus_quad.integrate_weight(p)-np.sum(p(quadpts)*weights))


    if True:
        x = 0.7236067977499793
        space.interpolation_points()
        bc = np.array([[0.0, 1.0]],dtype=np.float64)
        print(space.edge_basis(bc).shape)

        bc = np.array([[0.0,x,1-x]],dtype=np.float64)
        print(space.basis(bc).shape)
        print(space.div_basis(bc).shape)







    fig = plt.figure()
    axes = fig.gca()
    mesh.add_plot(axes)
    mesh.find_edge(axes,showindex=True)
    mesh.find_node(axes,showindex=True)
    mesh.find_cell(axes,showindex=True)
    fig.add_axes(axes)
    #plt.show()











