import numpy as np



class BarycentricSpace():
    '''
    通过给多重指标确定基函数
    '''
    def __init__(self,mesh,multi_index_matrix):
        #(ldof,gdim+1)
        self.multi_index_matrix = multi_index_matrix
        self.mesh = mesh
        self.gdim = multi_index_matrix.shape[1]-1
        self.ldof = multi_index_matrix.shape[0]
        self.bc = multi_index_matrix/(np.sum(multi_index_matrix,axis=-1)[:,None])

        self.init_basis_coefs()
        
    

    def init_basis_coefs(self):
        A = self.shape_function(self.bc) #(ldof,ldof)
        self.basis_coefs =  np.linalg.inv(A)


    

    def shape_function(self,bc):
        #bc.shape=(NQ,gdim+1)
        return np.prod(bc[...,None,:]**self.multi_index_matrix,axis=-1) #(NQ,ldof)

    

    def grad_shape_function(self,bc,index=np.s_[:],variables='u'):
        mesh = self.mesh
        ldof = self.ldof
        gdim = self.gdim
        multi_index_matrix = self.multi_index_matrix
        if 'grad_shape_function' in dir(mesh):
            grad_lambda = mesh.grad_shape_function(bc,index=index, p=1, variables=variables) #(NQ,NC,gdim+1,gdim)

            NC = grad_lambda.shape[-3]
            shape = bc.shape[:-1]+(NC,ldof,gdim) #(NQ,NC,ldof,gdim)
            gphi = np.zeros(shape,dtype=np.float64)

            for i in range(gdim+1):
                idx, = np.nonzero(multi_index_matrix[:,i]>0)
                if len(idx)>0:
                    multi_idx = multi_index_matrix[idx]
                    mul_p = np.copy(multi_idx[:,i])
                    multi_idx[:,i] = multi_idx[:,i] - 1
                    gphi[...,idx,:] = gphi[...,idx,:] + np.einsum('...ik,j,...j->...ijk',grad_lambda[:,:,i],mul_p,np.prod(bc[...,None,:]**multi_idx,axis=-1))

            return gphi

        else:
            grad_lambda = mesh.grad_lambda()[index] #(NC,gdim+1,gdim)
            NC =  grad_lambda.shape[0]
            shape = bc.shape[:-1]+(NC,ldof,gdim) #(NQ,NC,ldof,gdim)
            gphi = np.zeros(shape,dtype=np.float64)


            for i in range(gdim+1):
                idx, = np.nonzero(multi_index_matrix[:,i]>0)
                if len(idx)>0:
                    multi_idx = multi_index_matrix[idx]
                    mul_p = np.copy(multi_idx[:,i])
                    multi_idx[:,i] = multi_idx[:,i] - 1
                    gphi[...,idx,:] = gphi[...,idx,:] + np.einsum('ik,j,...j->...ijk',grad_lambda[:,i],mul_p,np.prod(bc[...,None,:]**multi_idx,axis=-1))

            return gphi









        



    def grad_basis(self,bc,index=np.s_[:],variables='u'):
        gphi = self.grad_shape_function(bc,index=index,variables=variables) #(NQ,NC,ldof,gdim)
        basis_coefs = self.basis_coefs #(ldof,ldof)
        gphi = np.einsum('...ik,ij->...jk',gphi,basis_coefs) #(NQ,NC,ldof,gdim)
        return gphi #(NQ,NC,ldof,gdim)





    def basis(self,bc):
        #bc.shape=(NQ,gdim+1)
        phi = self.shape_function(bc) #(NQ,ldof)
        phi = np.einsum('...i,ij->...j',phi,self.basis_coefs)
        return phi[...,None,:] #(NQ,1,ldof)









if __name__ == '__main__':
    import numpy as np
    import fealpy.mesh.MeshFactory as mf
    from fealpy.functionspace import LagrangeFiniteElementSpace
    from fealpy.functionspace import ParametricLagrangeFiniteElementSpace
    from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
    from fealpy.pde.poisson_Curved_2d import curve_circle

    if False:
        box = [0, 1, 0, 1]
        mesh = mf.boxmesh2d(box, nx=1, ny=1, meshtype='tri')
        Lspace = LagrangeFiniteElementSpace(mesh,p=3)
    else:
        N = 3
        theta = np.arange(N)*2*np.pi/N
        node = np.zeros((N+1,2),dtype=np.float_)
        node[1:,0] = np.cos(theta)
        node[1:,1] = np.sin(theta)
        cell = np.zeros((N,3),dtype=np.int_)
        cell[:,1] = np.arange(N)+1
        cell[:,2] = np.arange(N)+2
        cell[-1,2] = 1

        curve_circle = curve_circle()
        mesh = CurveLagrangeTriangleMesh(node,cell,curve=curve_circle)

        Lspace = ParametricLagrangeFiniteElementSpace(mesh, 3)





    multi_index_matrix = np.array([[3,0,0],
                                    [2,1,0],
                                    [2,0,1],
                                    [1,2,0],
                                    [1,1,1],
                                    [1,0,2],
                                    [0,5,0],
                                    [0,4,1],
                                    [0,3,2],
                                    [0,2,3],
                                    [0,1,4],
                                    [0,0,5]],dtype=np.int8)

    if False:
        multi_index_matrix = np.array([[3,0,0],
                                        [2,1,0],
                                        [2,0,1],
                                        [1,2,0],
                                        [1,1,1],
                                        [1,0,2],
                                        [0,3,0],
                                        [0,2,1],
                                        [0,1,2],
                                        [0,0,3]],dtype=np.int8)



    space = BarycentricSpace(mesh,multi_index_matrix)
    

    bc = np.array([[1,0,0]],dtype=np.float64)

    if True:
        bc =  np.array([[3,0,0],
                        [2,1,0],
                        [2,0,1],
                        [1,2,0],
                        [1,1,1],
                        [1,0,2],
                        [0,5,0],
                        [0,4,1],
                        [0,3,2],
                        [0,2,3],
                        [0,1,4],
                        [0,0,5]],dtype=np.int8)
        bc = bc/(np.sum(bc,axis=-1)[:,None])

    #print(bc.shape)
    print(space.grad_basis(bc).shape)
    #print(np.max(np.abs(space.basis(bc)-Lspace.basis(bc))))
    #print(np.max(np.abs((space.grad_basis(bc)-Lspace.grad_basis(bc)))))
    