import numpy as np

class curve_circle():
    def __init__(self):
        pass
    def project(self,p):
        p = p/np.sqrt(np.sum(p**2,axis=-1)[...,None])
        return p



    def phi(self,p,xi):
        #p.shape = (NEbd,2,2)
        #xi.shape = (NQ,) or (NQ,NEbd)

        #保证两个点之间的角度不超过pi
        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero((s[:,0] - s[:,1])>np.pi)
        s[idx,0] = s[idx,0] - 2*np.pi

        idx, =  np.nonzero((s[:,1] - s[:,0])>np.pi)
        s[idx,1] = s[idx,1] - 2*np.pi




        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        pp = np.zeros(shape,dtype=p.dtype)
        pp[...,0] = np.cos(s)
        pp[...,1] = np.sin(s)


        return pp








    def D_xi_phi(self,p,xi):
        #投影部分关于xi的求导
        #p.shape = (NEbd,2,2)
        #xi.shape = (NQ,) or (NQ,NEbd)

        #保证两个点之间的角度不超过pi
        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero((s[:,0] - s[:,1])>np.pi)
        s[idx,0] = s[idx,0] - 2*np.pi
        
        idx, =  np.nonzero((s[:,1] - s[:,0])>np.pi)
        s[idx,1] = s[idx,1] - 2*np.pi



        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        s = s[None,...] #(1,NEbd,2)
        pp = np.zeros(shape,dtype=p.dtype)

        pp[...,0] = -np.sin(s_xi)*(s[...,0]-s[...,1])
        pp[...,1] =  np.cos(s_xi)*(s[...,0]-s[...,1])

        return pp



    def unit_normal(self,p):
        r = np.sqrt(np.sum(p**2,axis=-1))
        return p/r[...,None]


    def theta(self,p):
        #p.shape = (...,2)
        shape = p.shape[:-1]
        p = p.reshape(-1,2) #(N,2)
        r = np.sqrt(np.sum(p**2,axis=-1))#(N,)
        s = np.arccos(p[:,0]/r)
        idx = (p[:,1] < 0)


        s[idx] = 2*np.pi - s[idx] #(0,2*pi]


        return s.reshape(shape)
