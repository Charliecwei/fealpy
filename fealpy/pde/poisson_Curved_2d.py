import numpy as np
from fealpy.decorator import cartesian


class curve_circle():
    def __init__(self):
        pass
    def project(self,p):
        p = p/np.sqrt(np.sum(p**2,axis=-1)[...,None])
        return p

    def phi(self,p,xi):
            #p.shape = (NEbd,2,2)
            #xi.shape = (NQ,) or (NQ,NEbd)

            s = self.theta(p) #(NEbd,2)
            idx, = np.nonzero((s[:,0] - s[:,1])>np.pi)
            #idx, = np.nonzero(s[:,0]>s[:,1])
            s[idx,0] = s[idx,0] - 2*np.pi
            


            


            if len(xi.shape)>1:
                shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
                s = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

            else:
                shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
                s = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

            pp = np.zeros(shape,dtype=p.dtype)
            pp[...,0] = np.cos(s)
            pp[...,1] = np.sin(s)

            
            
            

            return pp


    def lenoir_phi(self,p,bc):
        #p.shape = (NEbd,2,2)
        #bc.shape = (NQ,2) or (NQ,NEbd,2)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi #(NEbd,2)

        if len(bc.shape) > 2:
            shape = bc.shape[:-2] + p.shape[:-2] + (2,)
            s = np.einsum('...ij,ij->...i',bc,s) #(NQ,NEbd)
        
        else:
            shape = bc.shape[:-1] + p.shape[:-2] + (2,)
            s = np.einsum('...j,ij->...i',bc,s) #(NQ,NEbd)

        pp = np.zeros(shape,dtype=p.dtype)
        pp[...,0] = np.cos(s)
        pp[...,1] = np.sin(s)

        return pp



    def D_xi_lenoir_phi(self,p,xi,eta):
        #投影部分关于xi的求导
        #p.shape = (NEbd,2,2)
        #xi.shape = eta.shape = (NQ,) or (NQ,NEbd)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi

        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,2) #(NQ,NEbd,2)
            s_xi = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',eta,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,2) #(NQ,NEbd,2)
            s_xi = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',eta,s[...,1]) #(NQ,NEbd)

        pp = np.zeros(shape,dtype=p.dtype) #(NQ,NEbd,2,2)

        pp[...,0,:] = np.einsum('...i,ij->...ij',-np.sin(s_xi),s)
        pp[...,1,:] = np.einsum('...i,ij->...ij',np.cos(s_xi),s)

        return pp






    def D_xi_phi(self,p,xi):
        #投影部分关于xi的求导
        #p.shape = (NEbd,2,2)
        #xi.shape = (NQ,) or (NQ,NEbd)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi


        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        s = s[None,...] #(1,NEbd,2)
        pp = np.zeros(shape,dtype=p.dtype)

        pp[...,0] = -np.sin(s_xi)*(s[...,0]-s[...,1])
        pp[...,1] =  np.cos(s_xi)*(s[...,0]-s[...,1]) 

        return pp

    
    def D2_xi_phi(self,p,xi):
        #投影部分关于xi的二阶求导
        #p.shape = (NEbd,2,2)
        #xi.shape = (NQ,) or (NQ,NEbd)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi

        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        s = s[None,...] #(1,NEbd,2)
        pp = np.zeros(shape,dtype=p.dtype)

        pp[...,0] = -np.cos(s_xi)*((s[...,0]-s[...,1])**2)
        pp[...,1] =  -np.sin(s_xi)*((s[...,0]-s[...,1])**2) 


        return pp

        







    def theta(self,p):
        #p.shape = (...,2)
        shape = p.shape[:-1]
        p = p.reshape(-1,2) #(N,2)
        r = np.sqrt(np.sum(p**2,axis=-1))#(N,)
        s = np.arccos(p[:,0]/r)
        idx = (p[:,1] < 0)


        s[idx] = 2*np.pi - s[idx] #(0,2*pi]


        return s.reshape(shape)



    def jacobi(self,p):
        shape = p.shape+(2,) #(...,2,2)
        r = (np.sqrt(np.sum(p**2,axis=-1)))**3
        J = np.zeros(shape,dtype=p.dtype)
        J[...,0,0] = p[...,1]**2/r
        J[...,1,1] = p[...,0]**2/r
        J[...,0,1] = -(p[...,0]*p[...,1])/r
        J[...,1,0] = J[...,0,1]
        return J




class CircleSinSinData():
    def __init__(self):
        self.CircleCurve = curve_circle()



    def domain(self):
        return self.CircleCurve

  
    def init_mesh(self,p=None,n=0,returnnc=False,curve_method='scott_exact'):
        t = 1.0
        
        node = np.array([
        [0,0],
        [-t,-t],
        [t,-t],
        [t,t],
        [-t,t]],dtype=np.float64)
        cell = np.array([
        [0,1,2],
        [0,2,3],
        [0,3,4],
        [0,4,1]],dtype=np.int32)

        node[1:,:] = self.CircleCurve.project(node[1:,:])

        if returnnc:
            return node,cell
        else:
            if p is None:        
                from fealpy.mesh import TriangleMesh
                mesh = TriangleMesh(node,cell)
            else:
                from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
                mesh = CurveLagrangeTriangleMesh(node,cell,p=p,curve=self.CircleCurve,curve_method=curve_method)
            mesh.uniform_refine(n)
            return mesh
            
             
    

    @cartesian
    def solution(self,p):
        x = p[..., 0]
        y = p[..., 1]
        pi = np.pi
        u = np.sin(pi*x)*np.sin(pi*y)
        return u
    
    @cartesian
    def source(self,p):
        x = p[..., 0]
        y = p[..., 1]
        pi = np.pi
        f = 2*pi**2*np.sin(pi*x)*np.sin(pi*y)
        return f
    
    @cartesian
    def gradient(self,p):
        x = p[..., 0]
        y = p[..., 1]
        pi = np.pi
        grad = np.zeros(p.shape,dtype=np.float_)
        grad[...,0] = pi*np.cos(pi*x)*np.sin(pi*y)
        grad[...,1] = pi*np.sin(pi*x)*np.cos(pi*y)
        return grad

    @cartesian
    def dirichlet(self,p):
        p = self.CircleCurve.project(p)
        return self.solution(p)

    @cartesian
    def neumann(self,p, n):
        """ 
        Neuman  boundary condition

        Parameters
        ----------

        p: (NQ, NE, 2)
        n: (NE, NE, 2)

        grad*n : (NQ, NE, 2)
        """
        grad = self.gradient(p) #(NQ, NE, 2), (NQ, NE, 2)
        val = np.sum(grad*n,axis=-1) #(NQ,NE)
        return val


    def is_dirichlet_boundary(self,p):
        theta = self.CircleCurve.theta(p)
        return theta<=np.pi

    def is_neumann_boundary(self,p):
        theta = self.CircleCurve.theta(p)
        return theta>np.pi



