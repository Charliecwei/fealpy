import numpy as np
import sympy as sp
from  scipy import integrate
from fealpy.quadrature import Quadrature



class Gauss_type_quadrature:
    def __init__(self, W, k):
        '''
        计算在区域[0,1],权重为W(x)下的高斯数值积分公式，k为正交多项式次数
        '''
        self.W = W
        self.k = k
        self.init_orth_coefs()

    def init_orth_coefs(self):
        k = self.k
        W = self.W
        coefs = [np.array([1],dtype=np.float64)]
        gamma = []
        Orth_p = []
        b = []
        c = []
        beta = []

        def x(bc):
            return bc

        def p(bc):
            return self.Polynomials(bc,coefs=coefs[0])

        Orth_p.append(p)
        gamma.append(np.sqrt(self.inner_weigth(p,p)))
        b.append(self.inner_weigth(p, p, x=x)/(gamma[0]**2))
        


        


        for i in range(k):
            k_temp = len(coefs[i])
            coefs_temp = np.zeros(k_temp+1)
            coefs_temp[1:] = coefs[i]
            coefs_temp[:-1] -= b[i]*coefs[i]
            if i > 0:
                coefs_temp[:-2] -= c[i-1]*coefs[i-1]

            coefs.append(coefs_temp)
            def p(bc,i=i):
                return self.Polynomials(bc,coefs=coefs[i+1])

            Orth_p.append(p)

            gamma.append(np.sqrt(self.inner_weigth(p, p)))
            b.append(self.inner_weigth(p, p, x=x)/(gamma[i+1]**2))
            c.append((gamma[i+1]/gamma[i])**2)
            beta.append(gamma[i+1]/gamma[i])

        self.coefs = coefs
        self.Orth_p = Orth_p

        self.gamma = np.array(gamma,dtype=np.float64)
        self.b = np.array(b,dtype=np.float64)
        self.c = np.array(c,dtype=np.float64)

        self.alpha = np.array(b,dtype=np.float64)
        self.beta = np.array(beta,dtype=np.float64)


    
    def Gauss_Quadrature(self,k=None):
        '''
        k次正交多项式零点,有2k-1阶积分精度
        '''
        k = self.k if k is None else k
        beta = self.beta[:k-1]
        alpha = self.alpha[:k]

        A = np.diag(alpha)+np.diag(beta,k=-1)+np.diag(beta,k=1)


        quadpts, Q = np.linalg.eig(A)

        Weights = self.gamma[0]**2

        weights = Weights*(Q[0]**2)

        return quadpts, weights

    def Gauss_Radau_Quadrature(self,k=None):
        '''
        k次正交多项式零点,有2k-2阶积分精度, 0为其中一个零点
        '''
        k = self.k-1 if k is None else k-1


        coefs_k1 = np.copy(self.coefs[k+1])
        coefs_k0 = np.copy(self.coefs[k])
        coefs_k = np.copy(self.coefs[k-1])

        coefs = coefs_k1[1:]
        coefs[:-1] -= (coefs_k1[0]/coefs_k0[0])*coefs_k0[1:]

        coefs0 = coefs_k0[1:]
        coefs0[:-1] -= ((coefs_k0[0])/coefs_k[0])*coefs_k[1:]


        #积分点和权重

        A = np.diag(np.ones(k-1),k=-1)
        A[:,-1] = - coefs[:-1]

        quadpts = np.zeros(k+1,dtype=np.float64)
        weights = np.zeros(k+1,dtype=np.float64)
        
        quadpts[1:], Q= np.linalg.eig(A)


        def x(bc):
            return bc

        def q(bc):
            return self.Polynomials(bc,coefs=coefs)
        
        def Dq(bc):
            return self.Dx_Polynomials(bc,coefs=coefs)

        def q0(bc):
            return self.Polynomials(bc,coefs=coefs0)

        weights[0] = self.integrate_weight(q)/q(0.0)

        gamma = self.inner_weigth(q0, q0, x=x)

        weights[1:] = gamma/(quadpts[1:]*q0(quadpts[1:])*Dq(quadpts[1:]))

        return quadpts, weights

        

    def Gauss_Lobatto_Quadrature(self,k=None):
        '''
        k次正交多项式零点,有2k-3阶积分精度, 0, 1为其中两个零点
        '''      
        k = k - 2

        coefs_k2 = np.copy(self.coefs[k+2])
        coefs_k1 = np.copy(self.coefs[k+1])
        coefs_k0 = np.copy(self.coefs[k])
        coefs_k = np.copy(self.coefs[k-1])

        A = np.array([[coefs_k1[0],coefs_k0[0]],
                        [np.sum(coefs_k1),np.sum(coefs_k0)]],dtype=np.float64)

        b = -np.array([coefs_k2[0],np.sum(coefs_k2)],dtype=np.float64)

        x = np.linalg.solve(A, b)

        coefs = coefs_k2[1:]
        coefs[:-1] += coefs_k1[1:]*x[0]
        coefs[:-2] += coefs_k0[1:]*x[1]

        coefs_temp = np.copy(coefs)

        for i in range(len(coefs)-2):
            coefs[i+1] = coefs[i]+coefs_temp[i+1]
        coefs = coefs[:-1]



        A = np.array([[coefs_k0[0],coefs_k[0]],
                        [np.sum(coefs_k0),np.sum(coefs_k)]],dtype=np.float64)

        b = -np.array([coefs_k1[0],np.sum(coefs_k1)],dtype=np.float64)

        x = np.linalg.solve(A, b)


        coefs0 = coefs_k1[1:]
        coefs0[:-1] += coefs_k0[1:]*x[0]
        coefs0[:-2] += coefs_k[1:]*x[1]

        coefs_temp = np.copy(coefs0)
        for i in range(len(coefs0)-2):
            coefs0[i+1] = coefs0[i]+coefs_temp[i+1]
        coefs0 = coefs0[:-1]


        #计算积分点和权重
        A = np.diag(np.ones(k-1),k=-1)
        A[:,-1] = coefs[:-1]

        quadpts = np.zeros(k+2,dtype=np.float64)
        weights = np.zeros(k+2,dtype=np.float64)

        quadpts[-1] = 1.0
        quadpts[1:-1], Q = np.linalg.eig(A)

        
        def q(bc):
            return self.Polynomials(bc,coefs=coefs)

        def Dq(bc):
            return self.Dx_Polynomials(bc,coefs=coefs)

        def q0(bc):
            return self.Polynomials(bc,coefs=coefs0)

        def x0(bc):
            return bc
        
        def x1(bc):
            return 1.0-bc
        
        def x0_times_x1(bc):
            return bc*(1.0-bc)

        weights[0] = self.inner_weigth(x1, q)/q(0.0)
        weights[-1] = self.inner_weigth(x0, q)/q(1.0)
        bc = quadpts[1:-1]
        weights[1:-1] = self.inner_weigth(q0, q0, x=x0_times_x1)/(q0(bc)*Dq(bc)*x0_times_x1(bc))



        return quadpts, weights








        

        
       





        















        

    def inner_weigth(self,p,q,x=None):
        if x is None:
            def f(bc):
                return p(bc)*q(bc)
        else:
            def f(bc):
                return p(bc)*q(bc)*x(bc)
        
        return self.integrate_weight(f)
        

    def integrate_weight(self,f):
        W = self.W
        def F(bc):
            return f(bc)*W(bc)
        val,err = integrate.quad(F, 0, 1)
        if (err/np.abs(val) < 1e-10)|(err<1e-15):
            return val
        else:
            raise ValueError('积分误差较大,相对误差，绝对误差分别为',err/np.abs(val),err,np.abs(val))

            
        
    
    def Polynomials(self,bc,coefs=None):
        k = len(coefs)
        if type(bc) is float:
            bc = np.array([bc])
        return np.einsum('...i,i->...',bc[...,None]**np.arange(k),coefs)

    def Dx_Polynomials(self,bc,coefs=None):
        k = len(coefs)
        if k == 1:
            return 0*bc
        else:
            if type(bc) is float:
                bc = np.array([bc])
            return np.einsum('...i,i,i->...',bc[...,None]**np.arange(k-1),coefs[1:],np.arange(k-1)+1)
            






if __name__ == '__main__':
    import numpy as np
    from scipy import integrate
    from fealpy.pde.poisson_Curved_2d import CircleSinSinData  as PDE
    from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
    import matplotlib.pyplot as plt


    mdegree = 3
    nrefine = 0

    pde = PDE()
    curve = pde.domain()
    surface = pde.domain()
    mesh = pde.init_mesh(p=mdegree,n=nrefine) # p 次的拉格朗日四边形网格








    def W(x):
        if type(x) is float:
            x = np.array([x])
        shape = x.shape+(2,)
        bc = np.zeros(shape,dtype=np.float64)
        bc[...,0] = x
        bc[...,1] = 1.0-x
        J = mesh.jacobi_matrix(bc,index=[5])
        J = np.sqrt(np.sum(J**2,axis=(-1,-2)))
        return J[0]

    k = 7
    Gaus_quad = Gauss_type_quadrature(W, k)

    #quadpts, weights =  Gaus_quad.Gauss_Quadrature(k=3)

    def p(x):
        return 0*x+1.0

    #quadpts, weights = Gaus_quad.Gauss_Radau_Quadrature(k=3)

    quadpts, weights = Gaus_quad.Gauss_Lobatto_Quadrature(k=4)



    print(Gaus_quad.integrate_weight(p))

    fig = plt.figure()
    axes = fig.gca()
    mesh.add_plot(axes)
    mesh.find_edge(axes,showindex=True)
    plt.show()


