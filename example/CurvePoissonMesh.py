
import numpy as np
import matplotlib.pyplot as plt
from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh
import scipy.io as sio

class curve_circle():
    def __init__(self):
        pass
    def project(self,p):
        p = p/np.sqrt(np.sum(p**2,axis=-1)[...,None])
        return p

    def phi(self,p,xi):
            #p.shape = (NEbd,2,2)
            #xi.shape = (NQ,) or (NQ,NEbd)

            s = self.theta(p) #(NEbd,2)
            idx, = np.nonzero(s[:,0] > s[:,1])
            s[idx,0] = s[idx,0] - 2*np.pi


            if len(xi.shape)>1:
                shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
                s = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

            else:
                shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
                s = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

            pp = np.zeros(shape,dtype=p.dtype)
            pp[...,0] = np.cos(s)
            pp[...,1] = np.sin(s)
            

            return pp


    def lenoir_phi(self,p,bc):
        #p.shape = (NEbd,2,2)
        #bc.shape = (NQ,2) or (NQ,NEbd,2)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi #(NEbd,2)

        if len(bc.shape) > 2:
            shape = bc.shape[:-2] + p.shape[:-2] + (2,)
            s = np.einsum('...ij,ij->...i',bc,s) #(NQ,NEbd)
        
        else:
            shape = bc.shape[:-1] + p.shape[:-2] + (2,)
            s = np.einsum('...j,ij->...i',bc,s) #(NQ,NEbd)

        pp = np.zeros(shape,dtype=p.dtype)
        pp[...,0] = np.cos(s)
        pp[...,1] = np.sin(s)

        return pp



    def D_xi_lenoir_phi(self,p,xi,eta):
        #投影部分关于xi的求导
        #p.shape = (NEbd,2,2)
        #xi.shape = eta.shape = (NQ,) or (NQ,NEbd)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi

        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,2) #(NQ,NEbd,2)
            s_xi = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',eta,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,2) #(NQ,NEbd,2)
            s_xi = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',eta,s[...,1]) #(NQ,NEbd)

        pp = np.zeros(shape,dtype=p.dtype) #(NQ,NEbd,2,2)

        pp[...,0,:] = np.einsum('...i,ij->...ij',-sin(s_xi),s)
        pp[...,1,:] = np.einsum('...i,ij->...ij',cos(s_xi),s)

        return pp






    def D_xi_phi(self,p,xi):
        #投影部分关于xi的求导
        #p.shape = (NEbd,2,2)
        #xi.shape = (NQ,) or (NQ,NEbd)

        s = self.theta(p) #(NEbd,2)
        idx, = np.nonzero(s[:,0] > s[:,1])
        s[idx,0] = s[idx,0] - 2*np.pi


        if len(xi.shape)>1:
            shape = xi.shape[:-1] + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...i,i->...i',xi,s[...,0])+np.einsum('...i,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        else:
            shape =xi.shape + p.shape[:-2]+(2,) #(NQ,NEbd,2)
            s_xi = np.einsum('...,i->...i',xi,s[...,0])+np.einsum('...,i->...i',1-xi,s[...,1]) #(NQ,NEbd)

        s = s[None,...] #(1,NEbd,2)
        pp = np.zeros(shape,dtype=p.dtype)

        pp[...,0] = -np.sin(s_xi)*(s[...,0]-s[...,1])
        pp[...,1] =  np.cos(s_xi)*(s[...,0]-s[...,1]) 

        return pp







    def theta(self,p):
        #p.shape = (...,2)
        shape = p.shape[:-1]
        p = p.reshape(-1,2) #(N,2)
        r = np.sqrt(np.sum(p**2,axis=-1))#(N,)
        s = np.arccos(p[:,0]/r)
        idx = (p[:,1] < 0)

        s[idx] = 2*np.pi - s[idx]

        return s.reshape(shape)



    def jacobi(self,p):
        shape = p.shape+(2,) #(...,2,2)
        r = (np.sqrt(np.sum(p**2,axis=-1)))**3
        J = np.zeros(shape,dtype=p.dtype)
        J[...,0,0] = p[...,1]**2/r
        J[...,1,1] = p[...,0]**2/r
        J[...,0,1] = -(p[...,0]*p[...,1])/r
        J[...,1,0] = J[...,0,1]
        return J


mdegree = 4

r = 1.0
pi = np.pi
sin =  np.sin
cos = np.cos

N =4

curve_circle = curve_circle()

if True:
    theta = np.arange(N)*2*pi/N
    node = np.zeros((N+1,2),dtype=np.float_)
    node[1:,0] = cos(theta)
    node[1:,1] = sin(theta)
    cell = np.zeros((N,3),dtype=np.int_)
    cell[:,1] = np.arange(N)+1
    cell[:,2] = np.arange(N)+2
    cell[-1,2] = 1


else:
    i = 0

    Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']

    node = Node[i,0]
    cell = np.array(Cell[i,0],dtype=np.int_)





mesh0 = CurveLagrangeTriangleMesh(node,cell,p=mdegree,curve=curve_circle,curve_method='scott_exact')
mesh = CurveLagrangeTriangleMesh(node,cell,p=mdegree,curve=curve_circle,curve_method='lenoir_exact')

#mesh.uniform_refine(n=4)
if True:
    bc = np.array([[1/2,1/2]],dtype=np.float_)
else:
    bc = np.array([[1/3,1/6,1/2]],dtype=np.float_)

#print(mesh0.edge_tangent(),'\n')
#tange = mesh0.jacobi_matrix(bc)[0,:,:,0]
#tange = tange/np.sqrt(np.sum(tange**2,axis=-1))[:,None]
#print(tange)
#edge_index = mesh.ds.boundary_edge_index()[[0,1,2]]
#cell_index = mesh.ds.boundary_face_index()[[0,1,2]]

#print(np.max(np.abs(mesh.entity_measure('edge')-mesh0.entity_measure('edge'))))
#print(np.max(np.abs(mesh.entity_measure('cell')-mesh0.entity_measure('cell'))))

#print(np.max(np.abs(mesh.entity_measure('edge',index=edge_index)-mesh.entity_measure('edge')[edge_index])))
#print(np.max(np.abs(mesh.entity_measure('cell',index=cell_index)-mesh.entity_measure('cell')[cell_index])))





#print(np.max(np.abs(mesh.bc_to_point(bc)-mesh0.bc_to_point(bc))))












fig = plt.figure()
axes = fig.gca()
mesh.add_plot(axes)
mesh.find_edge(axes,showindex=True)
mesh.find_node(axes,showindex=True)
mesh.find_cell(axes,showindex=True)
fig.add_axes(axes)
plt.show()
