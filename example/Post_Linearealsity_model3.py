import numpy as np
import sys


from fealpy.tools.Post_processing import Post_processing
import matplotlib.pyplot as plt
from fealpy.quadrature import FEMeshIntegralAlg
import fealpy.mesh.MeshFactory as mf


print('begin load data,')
Post = []
for i in range(8):
    data = np.load('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/data{}_{}.npz'.format(2,i+1))
    Post.append(Post_processing(data['sh'],data['uh'],data['tspace_type'],data['degree'],node = data['node'],cell = data['cell']))
print('All data been load.')



if False:
    show_n = 1000
    x = np.linspace(1.0-0.1,1.0,show_n)
    shape = x.shape + (2,)
    p = np.zeros(shape,dtype=float)
    p[...,0] = x

    Post_now = Post[7]

    Sxx = Post_now.Sxx(p)
    fig, ax = plt.subplots(1,2)
    ax[0].plot(x,np.log10(-Sxx))
    
    ax[0].set(xlabel='x', ylabel='log10(-Sxx)',
       title='Sxx(x,0),x->1.0')




    y = np.linspace(0.0,0.1,show_n)
    p = np.zeros(shape,dtype=float)
    p[...,1] = y
    p[...,0] = 1.0
    Sxx = Post_now.Sxx(p)

    ax[1].plot(y,np.log10(-Sxx))

    ax[1].set(xlabel='y', ylabel='log10(-Sxx)',
       title='Sxx(1,y),y->0.0')
    
    plt.show()



if False:
    show_n = 20

    x = np.linspace(1.0-0.2,1.0-0.1,show_n)
    y = np.linspace(0.1,0.2,show_n)
    X,Y = np.meshgrid(x,y)
    shape = X.shape
    shape +=(2,)
    p = np.zeros(shape,dtype=float)
    p[...,0] = X
    p[...,1] = Y


    for i in range(7):
        print('Sxx:',np.max(np.abs(Post[i].Sxx(p)-Post[-1].Sxx(p))),'\n')

    for i in range(7):
        print('Syy:',np.max(np.abs(Post[i].Syy(p)-Post[-1].Syy(p))),'\n')

    for i in range(7):
        print('Sxy:',np.max(np.abs(Post[i].Sxy(p)-Post[-1].Sxy(p))),'\n')
    

if True:
    p = np.array([[0.5,0.75],
                  [0.25,0.5],
                  [0.5,0.5],
                  [0.75,0.5],
                  [0.5,0.25]])
    
    Sxx = np.zeros((5,6))
    Syy = np.zeros((5,6))
    Sxy = np.zeros((5,6))

    for i in range(6):
        print(i+1)
        Sxx[:,i] = Post[i+1].Sxx(p)
        Syy[:,i] = Post[i+1].Syy(p)
        Sxy[:,i] = Post[i+1].Sxy(p)

    print(Sxx,'\n')
    print(Syy,'\n')
    print(Sxy,'\n')

    for i in range(5):
        Sxx[:,i] = np.abs(Sxx[:,-1]-Sxx[:,i])
        Syy[:,i] = np.abs(Syy[:,-1]-Syy[:,i])
        Sxy[:,i] = np.abs(Sxy[:,-1]-Sxy[:,i])
    
    Sxx = Sxx[:,:-1]
    Syy = Syy[:,:-1]
    Sxy = Sxy[:,:-1]

    print(Sxx,'\n')
    print(Syy,'\n')
    print(Sxy,'\n')
