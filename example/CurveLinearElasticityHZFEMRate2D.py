import argparse
import numpy as np
import sympy as sp
import sys
from scipy.sparse import coo_matrix, csc_matrix, csr_matrix, spdiags, eye, bmat, construct
from scipy.io import savemat
import scipy.io as sio

import matplotlib.pyplot as plt


from fealpy.mesh.CurveLagrangeTriangleMesh import CurveLagrangeTriangleMesh

from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D import CurveHuZhangFiniteElementSpace
from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D_text import CurveHuZhangFiniteElementSpace_text
from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D_text_s import CurveHuZhangFiniteElementSpace_text_s
from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D_text_ss import CurveHuZhangFiniteElementSpace_text_ss
from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D_AT import CurveHuZhangFiniteElementSpace_AT
from fealpy.functionspace.CurveHuZhangFiniteElementSpace2D_bubble import CurveHuZhangFiniteElementSpace_bubble
from fealpy.functionspace.ParametricLagrangeFiniteElementSpace import ParametricLagrangeFiniteElementSpace
from fealpy.functionspace.DGLagrangeFiniteElementSpace_inner_bd import DGLagrangeFiniteElementSpace_inner_bd

#linear elasticity model
from fealpy.pde.linear_elasticity_model2D import Circle_GenLinearElasticitymodel2D

#solver 
from fealpy.solver.fast_solver import LinearElasticityHZFEMFastSolve


## error anlysis tool
from fealpy.tools.show import showmultirate, show_error_table

## matlab solve
import matlab.engine
eng = matlab.engine.start_matlab()



##  参数解析
parser = argparse.ArgumentParser(description=
        """
        曲边三角形网格上用胡张元求解线弹性力学问题
        """)


parser.add_argument('--sdegree',
        default=3, type=int,
        help='Lagrange 有限元空间的次数, 默认为 3 次.')

parser.add_argument('--mdegree',
        default=2, type=int,
        help='曲边网格的次数, 默认为 2 次.')

parser.add_argument('--nrefine',
        default=1, type=int,
        help='初始网格加密的次数, 默认初始加密 1 次.')

parser.add_argument('--maxit',
        default=4, type=int,
        help='默认网格加密求解的次数, 默认加密求解 4 次')


parser.add_argument('--curve_method',
        default='scott_exact', type=str,
        help='默认曲边处理，scott_exact')

parser.add_argument('--bdtype',
        default='displacement', type=str,
        help='边界条件, 默认为位移边界')

parser.add_argument('--mesh_type',
        default=1,type = int,
        help='生成的网格数据,默认为1')

parser.add_argument('--show_mesh',
        default=False, type = bool,
        help='网格展示，默认为False')


parser.add_argument('--tensor_space_type',
        default='lagrange', type=str,
        help ='选用的应力空间，默认为lagrange,可选有:gauss_labato,gauss,AT')

parser.add_argument('--stress_bdtype',
        default='插值', type=str,
        help='应力边界条件的处理，默认为插值，可选有:罚方法')



args = parser.parse_args()
sdegree = args.sdegree
mdegree = args.mdegree
nrefine = args.nrefine
maxit = args.maxit
curve_method = args.curve_method
bdtype = args.bdtype
mesh_type = args.mesh_type
show_mesh = args.show_mesh
tensor_space_type = args.tensor_space_type
stress_bdtype = args.stress_bdtype





#由位移生成pde模型
#E = 1e3
#nu = 0.3
#lam = E*nu/((1+nu)*(1-2*nu))
#mu = E/(2*(1+nu))


pi = sp.pi
sin = sp.sin
cos = sp.cos
exp = sp.exp
ln = sp.ln

#给定应力算例
lam = 1
mu = 1

#一般pde算例
x = sp.symbols('x0:2')
##在边界位移为0
#u = [sin(2*pi*(1-(x[0]**2+x[1]**2))),sin(2*pi*(1-(x[0]**2+x[1]**2)))] 

##不为0
#u = [sin(2*pi*x[0])*sin(2*pi*x[1]),sin(2*pi*x[0])*sin(2*pi*x[1])]
u = [exp(x[0]*x[1])*cos(x[0]),exp(x[1])*sin(x[0]+x[1])]
#u = [sin(x[0]**2+x[1]**2),cos(x[0]**4+x[1]**4)]
#u = [x[0]**5*x[1]**9,sin(x[0])+cos(x[1])]



if bdtype == 'displacement': 
    pde = Circle_GenLinearElasticitymodel2D(u,x,lam=lam,mu=mu,
            Dirichletbd_n='(0<=theta)&(theta<=2*np.pi)',
            Dirichletbd_t='(0<=theta)&(theta<=2*np.pi)')

elif bdtype == 'stress': 
    pde = Circle_GenLinearElasticitymodel2D(u,x,lam=lam,mu=mu,
            Neumannbd_nn='(theta>=0)&(theta<=2*np.pi)',
            Neumannbd_nt='(theta>=0)&(theta<=2*np.pi)')
elif bdtype == 'stress_and_displacement': 
    pde = Circle_GenLinearElasticitymodel2D(u,x,lam=lam,mu=mu,
                Dirichletbd_n='(0<theta)&(theta<np.pi)',
                Dirichletbd_t='(0<theta)&(theta<np.pi)',
                Neumannbd_nn='(theta>=np.pi)&(theta<=2*np.pi)',
                Neumannbd_nt='(theta>=np.pi)&(theta<=2*np.pi)')



curve_circle = pde.curve_circle

if mesh_type == 1:
    N = 10
    theta = np.arange(N)*2*np.pi/N
    node = np.zeros((N+1,2),dtype=np.float_)
    node[1:,0] = np.cos(theta)
    node[1:,1] = np.sin(theta)
    cell = np.zeros((N,3),dtype=np.int_)
    cell[:,1] = np.arange(N)+1
    cell[:,2] = np.arange(N)+2
    cell[-1,2] = 1

    mesh = CurveLagrangeTriangleMesh(node, cell,curve=curve_circle,curve_method=curve_method) 
    mesh.uniform_refine(n=nrefine)

elif mesh_type == 2:
    Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']

elif mesh_type == 3:
    Node = sio.loadmat('/Users/chen/Desktop/meshdata.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/meshdata.mat')['Cell']




errorType = ['$||\sigma - \sigma_h ||_{0}$',
             '$||div(\sigma - \sigma_h)||_{0}$',
             '$||u - u_h||_{0}$',
             '$||\sigma - \sigma_I ||_{0}$',
             '$||div(\sigma - \sigma_I)||_{0}$',
             '$||u - u_I||_{0}$',
             '$|| u_I - u_h||_{0}$'
             ]
Ndof = np.zeros((maxit,))
errorMatrix = np.zeros((len(errorType), maxit), dtype=np.float64)
gdim = 2
p = sdegree

for i in range(maxit):
    print("The {}-th computation:".format(i))

    if mesh_type >= 2:
        node = Node[i,0]
        cell = np.array(Cell[i,0],dtype=np.int_)
        mesh = CurveLagrangeTriangleMesh(node,cell,p=mdegree,curve=curve_circle,curve_method=curve_method)

    if tensor_space_type == 'lagrange':
        tspace = CurveHuZhangFiniteElementSpace(mesh, sdegree,q=2*sdegree)
    elif tensor_space_type == 'gauss_labato':
        tspace = CurveHuZhangFiniteElementSpace_text(mesh, sdegree,q=2*sdegree)
    elif tensor_space_type == 'gauss':
        tspace = CurveHuZhangFiniteElementSpace_text_s(mesh, sdegree,q=2*sdegree)
    elif tensor_space_type == 'AT':
        tspace = CurveHuZhangFiniteElementSpace_AT(mesh, sdegree,q=2*sdegree)
    elif tensor_space_type == 'bubble':
        tspace = CurveHuZhangFiniteElementSpace_bubble(mesh, sdegree,sdegree+1)



    
    if tensor_space_type == 'bubble':
        vspace = DGLagrangeFiniteElementSpace_inner_bd(mesh, sdegree-1,sdegree)
    else:
        vspace = DGLagrangeFiniteElementSpace_inner_bd(mesh, sdegree-1)
        #vspace = ParametricLagrangeFiniteElementSpace(mesh, sdegree-1, spacetype='D',q=2*sdegree)
        

    tgdof = tspace.number_of_global_dofs()
    vgdof = vspace.number_of_global_dofs()  



    sh = tspace.function()
    uh = vspace.function(dim=gdim)    

    if tensor_space_type == 'bubble':
        sI = tspace.function()
    else:
        sI = tspace.interpolation(pde.stress)

    uI = vspace.interpolation(pde.displacement)

    

    

    M = tspace.compliance_tensor_matrix(mu=pde.mu,lam=pde.lam)
    B0,B1 = tspace.div_matrix(vspace)



    F1 =  -vspace.source_vector(pde.source)
    #边界处理
    F0 = tspace.set_nature_bc(pde.dirichlet,threshold=pde.is_dirichlet_boundary) #此处以位移边界为dirichlet边界 
        

    if stress_bdtype=='插值':       
        #边界赋值
        isBDdof = tspace.set_essential_bc(sh,pde.neumann,threshold=pde.is_neumann_boundary)
        F0 -= M@sh
        F0[isBDdof] = sh[isBDdof]
        F1[:,0] -= B0@sh 
        F1[:,1] -= B1@sh
        

        bdIdx = np.zeros(tgdof, dtype=int)
        bdIdx[isBDdof] = 1
        Tbd = spdiags(bdIdx,0,tgdof,tgdof)
        T = spdiags(1-bdIdx,0,tgdof,tgdof)
        M = T@M@T + Tbd
        B0 = B0@T
        B1 = B1@T



     #solve
    
    
    FF = np.r_[F0,F1.T.reshape(-1)]
    AA = bmat([[M, B0.transpose(), B1.transpose()],[B0, None, None],[B1,None,None]],format='csr')



        
    savemat('/Users/chen/Desktop/data.mat',{'AA':AA,'bb':FF})
    eng.addpath('/Users/chen/Desktop')
    x = np.array(eng.matlab_solve())[:,0]
    
    #error
    sh[:] = x[:tgdof]
    uh[:,0] = x[tgdof:tgdof+vgdof]
    uh[:,1] = x[tgdof+vgdof:tgdof+2*vgdof]

        








    gdof = tgdof+gdim*vgdof
    Ndof[i] = gdof


    errorMatrix[0,i] = tspace.integralalg.error(pde.stress,sh.value)
    errorMatrix[1,i] = tspace.integralalg.error(pde.div_stress,sh.div_value)
    errorMatrix[2,i] = vspace.integralalg.error(pde.displacement,uh.value)

    errorMatrix[3,i] = tspace.integralalg.error(pde.stress,sI.value)
    errorMatrix[4,i] = tspace.integralalg.error(pde.div_stress,sI.div_value)
    errorMatrix[5,i] = vspace.integralalg.error(pde.displacement,uI.value)
    errorMatrix[6,i] = vspace.integralalg.error(uh.value,uI.value)

    if show_mesh:
        fig = plt.figure()
        axes = fig.gca()
        mesh.add_plot(axes)
        #mesh.find_edge(axes,showindex=True)
        fig.add_axes(axes)
        

    if (i < maxit-1)&(mesh_type==1):
        mesh.uniform_refine()


show_error_table(Ndof, errorType, errorMatrix)
showmultirate(plt, 0, Ndof, errorMatrix, errorType)
plt.show()




    





















