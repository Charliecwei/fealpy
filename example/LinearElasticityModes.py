import argparse
import numpy as np
import sys
from scipy import io
from scipy.sparse import csr_matrix, spdiags, bmat, construct
from timeit import default_timer as dtimer #统计时间

###################load mesh#########################
import fealpy.mesh.MeshFactory as mf
from fealpy.mesh import TriangleMesh
from fealpy.mesh.box2d import box2d
#################load function space###########################################################################################
from fealpy.functionspace.HuZhangFiniteElementSpace2D import HuZhangFiniteElementSpace
from fealpy.functionspace.HuZhangFiniteElementSpace2D_corner import HuZhangFiniteElementSpace as HuZhangFiniteElementSpace_corner
from fealpy.functionspace.HuMaFiniteElementSpace2D import HuMaFiniteElementSpace2D
from fealpy.functionspace.LagrangeFiniteElementSpace import LagrangeFiniteElementSpace
from fealpy.functionspace.multindex import Multindex


#################load linear elasticity model#########################################################################
from fealpy.pde.linear_elasticity_model2D import DisplacementTestmodel, StressTestmodel, Stress_concentrationTestmodel, Load1model


###########################load solver######################################## 
from scipy.sparse.linalg import spsolve
import matlab.engine #当矩阵规模比较大时候，调用
from fealpy.solver.LinearElasticityHZFEMFastSolve import LinearElasticityHZFEMFastSolve #胡张元快速算法
from fealpy.solver.fast_solver import LinearElasticityHZFEMFastSolve as LinearElasticityHZFEMFastSolve_0

################################# Post Prossing#######################################
from fealpy.tools.Cartesian_coordinates_function import Cartesian_coordinates_function, Cartesian_coordinates_function2
from fealpy.tools.plotshow import Contourf_show, Image_show
from fealpy.tools.Post_processing import Post_processing
import matplotlib.pyplot as plt




#############################################  参数解析 ##############################
parser = argparse.ArgumentParser(description=
        """
        三角形网格上用胡张元求解线弹性力学问题
        """)



parser.add_argument('--degree',
        default=3, type=int,
        help='Lagrange 有限元空间的次数, 默认为 3 次.')

parser.add_argument('--mesh_n',
        default=5, type=int,
        help='初始网格划分.')

parser.add_argument('--show_mesh',
        default=False,type=bool,
        help='是否展示网格刨分')

parser.add_argument('--tspace_type',
        default='HZ',type=str,
        help='应力空间元')

parser.add_argument('--show_stress',
        default=False,type=bool,
        help='是否显示应力')

parser.add_argument('--show_displacement',
        default=False,type=bool,
        help='是否显示位移')

parser.add_argument('--show_n',
        default=50,type=int,
        help='显示网格分辨率')

parser.add_argument('--pde_model',
        default=1, type=int,
        help='求解的pde模型')


parser.add_argument('--show_boundary',
        default=False,type=bool,
        help='展示边界条件')


parser.add_argument('--save_data',
        default=False,type=bool,
        help='保存数据')



parser.add_argument('--level',
        default=50,type=int,
        help='绘图level')


parser.add_argument('--mesh_type',
        default=1,type=int,
        help='网格种类')


parser.add_argument('--solver',
        default='direct',type=str,
        help='求解方程组')



args = parser.parse_args()

degree = args.degree
mesh_n = args.mesh_n
tspace_type = args.tspace_type
show_mesh = args.show_mesh
show_stress = args.show_stress
show_displacement = args.show_displacement
show_n = args.show_n
pde_model = args.pde_model
show_boundary = args.show_boundary
save_data = args.save_data
level = args.level
mesh_type = args.mesh_type
solver = args.solver

##########################Lame coeffices###############################
E = 1e3
nu = 0.3
lam = E*nu/((1+nu)*(1-2*nu))
mu = E/(2*(1+nu))

#print(-(2*mu+lam)/100,-lam/100)
#########################pde model#####################################
if pde_model == 1:
    pde = DisplacementTestmodel(lam=lam,mu=mu)
elif pde_model == 2:
    pde = StressTestmodel(lam=lam,mu=mu)
elif pde_model == 3:
    pde = Stress_concentrationTestmodel(lam=lam,mu=mu)
elif pde_model == 4:
    pde = Load1model(lam=1.0)
else:
    raise ValueError("No define pde model!")

#########################General mesh#################################
if mesh_type == 1:
    mesh = mf.special_boxmesh2d(pde.domain(),n=mesh_n,meshtype='cross')

elif mesh_type == 2:
    h = np.power(0.1,mesh_n)
    h0 = 0.25
    node,cell = box2d(h0=h0,h=h)
    mesh = TriangleMesh(node,cell)

elif mesh_type == 3:
    cell = np.loadtxt("/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/fepgdata/cell{}.txt".format(mesh_n))
    node = np.loadtxt("/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/fepgdata/node{}.txt".format(mesh_n))
    cell = np.array(cell-1,dtype=np.int_)

    mesh = TriangleMesh(node,cell)

elif mesh_type == 4:
    mesh = mf.boxmesh2d(pde.domain(),nx=mesh_n,ny=mesh_n)

if show_mesh:
    fig = plt.figure()
    axes = fig.gca()
    mesh.add_plot(axes)
    #mesh.find_node(axes, showindex=True)
    #mesh.find_edge(axes, showindex=True)
    #mesh.find_cell(axes, showindex=True)




gdim = mesh.geo_dimension()
##########################有限元求解##################################
##########################load space#################################
print('begin construct matrix:')
t0 = dtimer()

if tspace_type == 'HZ':
    tspace = HuZhangFiniteElementSpace(mesh, degree)
    vspace = LagrangeFiniteElementSpace(mesh, degree-1, spacetype='D') 
elif tspace_type == 'HM':
    tspace = HuMaFiniteElementSpace2D(mesh,degree)
    vspace = LagrangeFiniteElementSpace(mesh,degree,spacetype='D')
elif tspace_type == 'HZ_corner':
        tspace = HuZhangFiniteElementSpace_corner(mesh,degree)
        vspace = LagrangeFiniteElementSpace(mesh, degree-1, spacetype='D') 

tgdof = tspace.number_of_global_dofs()
vgdof = vspace.number_of_global_dofs()

sh = np.zeros(tgdof,dtype=np.float64)
uh = np.zeros((vgdof,2),dtype=np.float64)

print(pde.mu,pde.lam)
########################General matrix###########################
M = tspace.compliance_tensor_matrix(mu=pde.mu,lam=pde.lam)
if solver == 'fast':
    M0 = tspace.compliance_tensor_matrix(mu=pde.mu,lam=0.0)

B0,B1 = tspace.div_matrix(vspace)


#######################source term#####################################
F1 =  -vspace.source_vector(pde.source,dim=gdim)



#########################################################边界处理##########################################################
F0 = tspace.set_nature_bc(pde.dirichlet,threshold=pde.is_dirichlet_boundary) #此处以位移边界为dirichlet边界
if tspace_type == 'HZ':
    isBDdof = tspace.set_essential_bc(sh, pde.neumann,M,B0,B1,F0, threshold=pde.is_neumann_boundary)#以应力边界为neumann边界
elif tspace_type == 'HM':
    isBDdof = tspace.set_essential_bc(sh, pde.neumann, threshold=pde.is_neumann_boundary)#以应力边界为neumann边界
elif tspace_type == 'HZ_corner':
    isBDdof = tspace.set_essential_bc(sh, pde.neumann,threshold=pde.is_neumann_boundary)#以应力边界为neumann边界





F0 -= M@sh
F0[isBDdof] = sh[isBDdof]
F1[:,0] -= B0@sh 
F1[:,1] -= B1@sh


bdIdx = np.zeros(tgdof, dtype=int)
bdIdx[isBDdof] = 1
Tbd = spdiags(bdIdx,0,tgdof,tgdof)
T = spdiags(1-bdIdx,0,tgdof,tgdof)
M = T@M@T + Tbd
if solver == 'fast':
    M0 = T@M0@T + Tbd

B0 = B0@T
B1 = B1@T

print('construct matrix time:',dtimer()-t0)



####################################################求解##################################################################
print('begin solve')
t0 = dtimer()

if solver == 'direct':
    FF = np.r_[F0,F1.T.reshape(-1)]
    AA = bmat([[M, B0.transpose(), B1.transpose()],[B0, None, None],[B1,None,None]],format='csr')

    if len(FF) < 100000:
        x = spsolve(AA,FF)
    else:
        io.savemat('/Users/chen/Desktop/data.mat',{'AA':AA,'bb':FF})
        print('matrix been save')
        if len(FF) < 2000000:
            print('matlab solve')
            eng = matlab.engine.start_matlab()
            eng.addpath('/Users/chen/Desktop')
            x = np.array(eng.matlab_solve())[:,0]
        else:
            raise ValueError('equation be too large,stop!')

elif solver == 'fast':
    B = construct.vstack([B0,B1],format='csr')
    A = [M,B,None]
    F = [F0,F1]


    Fast_solver = LinearElasticityHZFEMFastSolve(A,F,vspace,tspace,pde.mu,isBDdof=isBDdof)
    #Fast_solver = LinearElasticityHZFEMFastSolve_0(A,F,vspace,M0=M0)
    x = Fast_solver.solve()

print('solve time:',dtimer()-t0)
###############################################################################################################
sh = x[:tgdof]
if tspace_type == 'HZ_corner':
        sh[tspace.Corner_coffefices_idx] = np.einsum('ijk,ik->ij',tspace.Corner_coffefices,sh[tspace.Corner_coffefices2_idx])
uh[:,0] = x[tgdof:tgdof+vgdof]
uh[:,1] = x[tgdof+vgdof:]

Gdof = tgdof+gdim*vgdof
print('number of gdof:',Gdof)


if save_data:
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    if pde_model == 1:
        np.savez('/Users/chen/Desktop/Linear_elasity_data/DisplacementTestmodel_data/data{}_{}'.format(mesh_type,mesh_n),degree=degree,sh=sh,uh=uh,node=node,cell=cell,tspace_type=tspace_type)
        print('save data to /Users/chen/Desktop/Linear_elasity_data/DisplacementTestmodel_data/data{}_{}'.format(mesh_type,mesh_n))
    elif pde_model == 2:
        np.savez('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/data{}_{}'.format(mesh_type,mesh_n),degree=degree,sh=sh,uh=uh,node=node,cell=cell,tspace_type=tspace_type)
        print('save data to /Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/data{}_{}'.format(mesh_type,mesh_n))
    elif pde_model == 3:
        np.savez('/Users/chen/Desktop/Linear_elasity_data/Stress_concentrationTestmodel_data/data{}_{}'.format(mesh_type,mesh_n),degree=degree,sh=sh,uh=uh,node=node,cell=cell,tspace_type=tspace_type)
        print('save data to /Users/chen/Desktop/Linear_elasity_data/Stress_concentrationTestmodel_data/data{}_{}'.format(mesh_type,mesh_n))








##################################################后处理##################################################################

Post = Post_processing(sh,uh,tspace_type,degree,tspace=tspace,vspace=vspace)

##########################################check boundary condition#########################
if show_boundary:
    ############stress##################
    N = mesh_n*degree+1
    x = np.linspace(0,1,N)
    pp = np.zeros((N,2),dtype=np.float64)
    pp[:,0] = x

    Stress = Post.Stress(pp,k=[2,1])
    print('下边界法向应力：\n')
    print(-Stress,'\n')

    
    pp[:,1] = pp[:,1]+1.0
    Stress = Post.Stress(pp,k=[2,1])
    print('上边界法向应力：\n') 
    print(Stress,'\n')


    pp[:,1] = x
    pp[:,0] = 0*pp[:,0]
    Stress = Post.Stress(pp,k=[0,2])
    print('左边界法向应力：\n')
    print(-Stress,'\n')


    pp[:,0] = pp[:,0]+1.0
    Stress = Post.Stress(pp,k=[0,2])
    print('右边界法向应力：\n')
    print(Stress,'\n')



    #########displacement###############
    N = mesh_n*(degree-1)+1
    x = np.linspace(0,1,N)
    pp = np.zeros((N,2),dtype=np.float64)
    pp[:,0] = x
    
    Displacement = Post.Displacement(pp)
    print('下边界位移：\n')
    print(Displacement,'\n')


    pp[:,1] = pp[:,1]+1.0
    Displacement = Post.Displacement(pp)
    print('上边界位移：\n')
    print(Displacement,'\n')


    pp[:,1] = x
    pp[:,0] = 0*pp[:,0]
    Displacement = Post.Displacement(pp)
    print('左边界位移：\n')
    print(Displacement,'\n')


    pp[:,0] = pp[:,0]+1.0
    Displacement = Post.Displacement(pp)
    print('右边界位移：\n')
    print(Displacement,'\n')








##################################################绘制云图###########################################
if show_displacement:
    # 绘制位移图
    Post.show_displacement(show_n,level=level)


    
if show_stress: 
    # 绘制应力图
    Post.show_stress(show_n,level=level)


if show_mesh|show_stress|show_displacement:
    plt.show()



#pp = np.array([[0.0,0.0],[0.0,1.0],[1.0,0.0],[1.0,1.0]])


#print(Post.Sxx(pp))
#print(Post.Syy(pp))
#print(Post.Sxy(pp))


#y = np.linspace(0,1,1000)
#pp = np.zeros(y.shape+(2,))
#pp[:,1] = y

#syy = Post.Syy(pp)

#print(np.min(syy),np.max(syy))








