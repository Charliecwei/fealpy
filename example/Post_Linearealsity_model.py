import numpy as np
import sys


from fealpy.tools.Post_processing import Post_processing
import matplotlib.pyplot as plt
import pdb

pde_model = int(sys.argv[1])
show_n = int(sys.argv[2])


if pde_model == 1:
    data = np.load('/Users/chen/Desktop/Linear_elasity_data/DisplacementTestmodel_data/data.npz')
elif pde_model == 2:
    data = np.load('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/data16.npz')
elif pde_model == 3:
    data = np.load('/Users/chen/Desktop/Linear_elasity_data/Stress_concentrationTestmodel_data/data45883.npz')

else:
    raise ValueError('No pde model!')

#######################load post program#######################
Post = Post_processing(data['sh'],data['uh'],data['tspace_type'],data['degree'],node = data['node'],cell = data['cell'])


#pdb.set_trace()

#Post.show_displacement(20,level=11,show_image=True)

Post.show_stress(show_n,level=50)

#Post.show_stress(20,level=12,show_image=True)

#show_n = 5
#x = np.linspace(0,1,show_n)
#y = np.linspace(1,0,show_n) #用imshows绘图时,y坐标轴刚好反了过来 
#X,Y = np.meshgrid(x,y)
#shape = X.shape
#shape +=(2,)
#p = np.zeros(shape,dtype=float)
#p[...,0] = X
#p[...,1] = Y
#p = p[[0,1,2,-3,-2,-1],[0,0,0,0,0,0]]
#Sigmah = Post.Stress(p)
#print(Sigmah[...,0])

#print(p)
#idx_y = np.array([0,1,2,-1,-2,-3])
#idx_x = np.array([0,1,2,3,4])
#print(Sigmah[idx_y[:,None],idx_x[None,:],0])



#print(Sigmah[:,[0,1,2,3],0],'\n')

#print(Sigmah[0,:])




#Post.show_mesh(show_cell=True)

plt.show()
