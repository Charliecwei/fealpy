import argparse

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from fealpy.decorator import cartesian, barycentric
from fealpy.pde.poisson_Curved_2d import CircleSinSinData  as PDE
from fealpy.pde.curve import curve_circle

from fealpy.functionspace import ParametricLagrangeFiniteElementSpace

from fealpy.mesh import LagrangeTriangleMesh
from fealpy.mesh.circul import circulmesh

from fealpy.boundarycondition import DirichletBC, NeumannBC
from fealpy.tools.show import showmultirate, show_error_table


from scipy.sparse.linalg import spsolve
from scipy.sparse import bmat
import scipy.io as sio




## 参数解析
parser = argparse.ArgumentParser(description=
        """
        2D曲边三角形网格上的任意次等参有限元方法
        """)

parser.add_argument('--sdegree',
        default=1, type=int,
        help='Lagrange 有限元空间的次数, 默认为 1 次.')

parser.add_argument('--mdegree',
        default=1, type=int,
        help='网格的阶数, 默认为 1 次.')


parser.add_argument('--nrefine',
        default=2, type=int,
        help='初始网格加密的次数, 默认初始加密 2 次.')

parser.add_argument('--maxit',
        default=4, type=int,
        help='默认网格加密求解的次数, 默认加密求解 4 次')

parser.add_argument('--bdtype',
        default='dirichlet', type=str,
        help='边界条件, 默认为Dirichlet边界条件')

parser.add_argument('--mesh_type',
        default=1,type = int,
        help='生成的网格数据,默认为1')

parser.add_argument('--show_mesh',
        default=False, type = bool,
        help='网格展示，默认为False')

parser.add_argument('--solver',
        default='direct',type=str,
        help='求解方程组')



args = parser.parse_args()
sdegree = args.sdegree
mdegree = args.mdegree
nrefine = args.nrefine
maxit = args.maxit
bdtype = args.bdtype
mesh_type = args.mesh_type
show_mesh = args.show_mesh
solver = args.solver




pde = PDE()
curve_circle = curve_circle()

if mesh_type == 1: 
    N = 4
    theta = np.arange(N)*2*np.pi/N
    node = np.zeros((N+1,2),dtype=np.float_)
    node[1:,0] = np.cos(theta)
    node[1:,1] = np.sin(theta)
    cell = np.zeros((N,3),dtype=np.int_)
    cell[:,1] = np.arange(N)+1
    cell[:,2] = np.arange(N)+2
    cell[-1,2] = 1
    

    mesh = LagrangeTriangleMesh(node,cell,p=mdegree,surface=curve_circle)
    mesh.uniform_refine(n=nrefine)

elif mesh_type == 2:
    Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']



elif mesh_type == 2:
    Node = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/matlab.mat')['Cell']

elif mesh_type == 3:
    Node = sio.loadmat('/Users/chen/Desktop/meshdata.mat')['Node']
    Cell = sio.loadmat('/Users/chen/Desktop/meshdata.mat')['Cell']

elif mesh_type == 4:
    h = 0.5



errorType = ['$|| u - u_h||_{\Omega,0}$',
             '$||\\nabla u - \\nabla u_h||_{\Omega, 0}$',
             '$|| u - u_I||_{\Omega,0}$',
             '$||\\nabla u - \\nabla u_I||_{\Omega, 0}$',
             '$|| u_I - u_h ||_{\Omega, \infty}$'
             ]
errorMatrix = np.zeros((len(errorType), maxit), dtype=np.float64)
NDof = np.zeros(maxit, dtype=np.int_)









for i in range(maxit):
    print("The {}-th computation:".format(i))
    if (mesh_type >= 2)&(mesh_type<=3):
        node = Node[i,0]
        cell = np.array(Cell[i,0],dtype=np.int_)
        mesh = LagrangeTriangleMesh(node,cell,p=mdegree,surface=curve_circle)

    elif mesh_type == 4:
        node,cell = circulmesh(h)
        h = h/2.0
        mesh = LagrangeTriangleMesh(node,cell,p=mdegree,surface=curve_circle)


    space = ParametricLagrangeFiniteElementSpace(mesh, p=sdegree)
    NDof[i] = space.number_of_global_dofs()

    uI = space.interpolation(pde.solution)

    uh = space.function()
    A = space.stiff_matrix()
    F = space.source_vector(pde.source)
    

    if  bdtype == 'dirichlet': 
        bc = DirichletBC(space,pde.dirichlet)
        A, F = bc.apply(A,F,uh)    
        uh[:] = spsolve(A, F)
    elif bdtype == 'neumann': 
        bc = NeumannBC(space, pde.neumann)
        A, F = bc.apply(F, A=A)
        uh[:] = spsolve(A, F)[:-1]

    else:
        bc = NeumannBC(space, pde.neumann,threshold=pde.is_neumann_boundary)
        F = bc.apply(F)

        bc = DirichletBC(space,pde.dirichlet,threshold=pde.is_dirichlet_boundary)
        A, F = bc.apply(A,F,uh)    
        uh[:] = spsolve(A, F)








    errorMatrix[0, i] = space.integralalg.error(pde.solution, uh.value)
    errorMatrix[1, i] = space.integralalg.error(pde.gradient, uh.grad_value)

    errorMatrix[2, i] = space.integralalg.error(pde.solution, uI.value)
    errorMatrix[3, i] = space.integralalg.error(pde.gradient, uI.grad_value)
    errorMatrix[4, i] = np.max(np.abs(uI - uh))


    if show_mesh:
        fig = plt.figure()
        axes = fig.gca()
        mesh.add_plot(axes)
        fig.add_axes(axes)

    
    if i < maxit-1:
        mesh.uniform_refine()




show_error_table(NDof, errorType, errorMatrix)
showmultirate(plt, 0, NDof, errorMatrix,  errorType, propsize=20)
plt.show()

