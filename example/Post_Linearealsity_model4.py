import numpy as np
import sys

from fealpy.tools.plotshow import Show_table
from fealpy.tools.Post_processing import Post_processing
from fealpy.tools.FepgPost_processing import FepgPost_processing
import matplotlib.pyplot as plt



print('begin laod data')
HZPost = []
for i in range(7):
    data = np.load('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/data{}_{}.npz'.format(3,2**(i+1)))
    HZPost.append(Post_processing(data['sh'],data['uh'],data['tspace_type'],data['degree'],node = data['node'],cell = data['cell']))
    
    print(HZPost[i].tspace.number_of_global_dofs()+2*HZPost[i].vspace.number_of_global_dofs())

print('All Hu Zhang data been load.')

FepgPost = []
for i in range(7):
    sh = np.loadtxt('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/fepgdata/S{}.txt'.format(2**(i+1)))
    uh = np.loadtxt('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/fepgdata/U{}.txt'.format(2**(i+1)))
    cell = np.loadtxt('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/fepgdata/cell{}.txt'.format(2**(i+1)))
    node = np.loadtxt('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/fepgdata/node{}.txt'.format(2**(i+1)))
    cell = np.array(cell-1,dtype=np.int_)
    FepgPost.append(FepgPost_processing(sh,uh,node,cell))
    
    print(FepgPost[i].number_of_global_dofs)

print('All Fepg data been load.')




if False:
    #绘图
    show_n = 128
    HZPost[-1].show_stress(show_n)


    plt.show()


if False:
    #远离角点
    if False:
        p = np.array([[0.5,0.75],
                      [0.25,0.5],
                      [0.5,0.5],
                      [0.75,0.5],
                      [0.5,0.25]])
    
    elif False:
    #靠近角点（0，0）
        p = np.array([[0.0,0.0],
                      [0.0,0.025],
                      [0.0,0.05],
                      [0.0,0.075],
                      [0.0,0.1],
                        
                      [0.01,0.0],
                      [0.01,0.025],
                      [0.01,0.05],
                      [0.01,0.075],
                      [0.01,0.1]])


    elif True:
    #靠近角点（1，0）
        p = np.array([[1,0],
                      [1,0.025],
                      [1,0.05],
                      [1,0.075],
                      [1,0.1],

                      [1-0.01,0],
                      [1-0.01,0.025],
                      [1-0.01,0.05],
                      [1-0.01,0.075],
                      [1-0.01,0.1]])




    for j in range(p.shape[0]):
        
        S = np.zeros((2,7))
        
        print('\multirow{2}{*}','{(%.3f,'%p[j,0],'%.3f)}'%p[j,1])
        for i in range(7):
            S[0,i] = HZPost[i].Sxx(p[j])
            S[1,i] = FepgPost[i].Sxx(p[j])
            #S[0,i] = HZPost[i].Syy(p[j])
            #S[1,i] = FepgPost[i].Syy(p[j])
            
            #S[0,i] = HZPost[i].Sxy(p[j])
            #S[1,i] = FepgPost[i].Sxy(p[j])

        if True:
            #compute error
            S = S - S[0,-1]
            S = S[:,:-1]
        
        Show_table(S)
        print('\\\\')












if True:
    k = 2
    
    x = np.linspace(0,1,1000)
    
    p = np.zeros(x.shape+(2,))
    p[...,0] = x
    


    fig, ax = plt.subplots(2,2)


    S = HZPost[-1].Stress(p,k)
    ax[0,0].plot(x, S, label='HZ P3')
    
    S = FepgPost[i].Stress(p,k)
    ax[0,0].plot(x, S, dashes=[6, 2], label='Fepg P1')
    
    ax[0,0].legend()
    ax[0,0].set_title('Sxy(x,0)')
    
    
    p[...,1] = 1.0
    S = HZPost[-1].Stress(p,k)
    ax[0,1].plot(x, S, label='HZ P3')

    S = FepgPost[i].Stress(p,k)
    ax[0,1].plot(x, S, dashes=[6, 2], label='Fepg P1')

    ax[0,1].legend()
    ax[0,1].set_title('Sxy(x,1)')







    y = np.linspace(0,1,1000)
    p = np.zeros(y.shape+(2,))
    p[...,1] = y


    S = HZPost[-1].Stress(p,k)
    ax[1,0].plot(y, S, label='HZ P3')
   

    S = FepgPost[i].Stress(p,k)
    ax[1,0].plot(y, S, dashes=[6, 2], label='Fepg P1')
    
    ax[1,0].legend()
    ax[1,0].set_title('Sxy(0,y)')


    
    p[...,0] = 1.0
    S = HZPost[-1].Stress(p,k)
    ax[1,1].plot(y, S, label='HZ P3')

    S = FepgPost[i].Stress(p,k)
    ax[1,1].plot(y, S, dashes=[6, 2], label='Fepg P1')

    ax[1,1].legend()
    ax[1,1].set_title('Sxy(1,y)')




    plt.show()










