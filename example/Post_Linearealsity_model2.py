import numpy as np
import sys


from fealpy.tools.Post_processing import Post_processing
import matplotlib.pyplot as plt
from fealpy.quadrature import FEMeshIntegralAlg
import fealpy.mesh.MeshFactory as mf
from fealpy.tools.plotshow import Show_table





print('begin load data,')
Post = []
for i in range(7):
    data = np.load('/Users/chen/Desktop/Linear_elasity_data/StressTestmodel_data/data{}.npz'.format(2**(i+1)))
    Post.append(Post_processing(data['sh'],data['uh'],data['tspace_type'],data['degree'],node = data['node'],cell = data['cell']))

    print(Post[i].tspace.number_of_global_dofs()+2*Post[i].vspace.number_of_global_dofs())

print('All data been load.')



if True:
    p = np.array([[0.5,0.75],
                  [0.25,0.5],
                  [0.5,0.5],
                  [0.75,0.5],
                  [0.5,0.25]])

    Sxx = np.zeros((5,6))
    Syy = np.zeros((5,6))
    Sxy = np.zeros((5,6))

    #for i in range(6):
        #Sxx[:,i] = Post[i+1].Sxx(p)
        #Syy[:,i] = Post[i+1].Syy(p)
        #Sxy[:,i] = Post[i+1].Sxy(p)
    
    
    Sxx = np.array([[-99.9996,-100.9073,-101.0790 ,- 101.0832,-101.0752 ,-101.0717],
         [-100.6577,-101.0628,-101.1467,-101.1564,-101.1571,-101.1572],
         [-99.9892,-101.7963,-102.4932,-102.7121,-102.7755,-102.7946],
         [-96.0843,-99.2332,-100.3730,-100.7676,-100.9026,-100.9497],
         [-99.9996,-100.9073,-101.0790,-101.0832,-101.0752,-101.0717]])


    Syy = np.array([[-3.3141,-1.8685,-1.1498,-0.91280,-0.84186 ,-0.81947],
         [-0.37717,0.53137,0.83706,0.92485,0.95087,0.95929],
         [-4.0704,-3.0171,-2.4097,-2.16628,-2.0791,-2.0482],
         [-14.1983,-14.4536,-14.3298,-14.1733,-14.0808,-14.0385],
         [-3.3141,-1.8685,-1.1498,-0.91280,-0.84186 ,-0.81947]])


    Sxy = np.array([[-0.54970,0.33864,0.82638,1.0203,1.0828,1.1028],
         [2.80e-14,2.91e-14,-3.86e-14,-1.47e-13,6.59e-13,-1.23e-12],
         [2.47e-14,5.72e-14,1.06e-14,-8.61e-14,2.93e-13,-3.43e-12],
         [1.18e-14,2.51e-14,6.25e-14,3.86e-14,4.51e-13,-1.41e-12],
         [0.54970,-0.33864,-0.82638,-1.0203,-1.0828,-1.1028]])

    #Show_table(p,Sxy)



    for i in range(5):
        Sxx[:,i] = np.abs(Sxx[:,-1]-Sxx[:,i])
        Syy[:,i] = np.abs(Syy[:,-1]-Syy[:,i])
        Sxy[:,i] = np.abs(Sxy[:,-1]-Sxy[:,i])

    Sxx = Sxx[:,:-1]
    Syy = Syy[:,:-1]
    Sxy = Sxy[:,:-1]

    #Show_table(p,Sxy)
   
    #print('Sxx err:',Sxx,'\n')
    #print('Syy err:',Syy,'\n')
    #print('Sxy err:',Sxy,'\n')






























if False:
    show_n = 20
    x = np.linspace(1.0-0.1,1.0-0.05,show_n)
    y = np.linspace(0.05,0.1,show_n) #用imshows绘图时,y坐标轴刚好反了过来 
    X,Y = np.meshgrid(x,y)
    shape = X.shape
    shape +=(2,)
    p = np.zeros(shape,dtype=float)
    p[...,0] = X
    p[...,1] = Y
    
    #p = np.array([[0.015,0.012],[0.015,0.02]])
    for i in range(6):
        print('Sxx:',np.max(np.abs(Post[i].Sxx(p)-Post[-1].Sxx(p))),'\n')

    for i in range(6):
        print('Syy:',np.max(np.abs(Post[i].Syy(p)-Post[-1].Syy(p))),'\n')

    for i in range(6):
        print('Sxy:',np.max(np.abs(Post[i].Sxy(p)-Post[-1].Sxy(p))),'\n')




if False:
    show_n = 10
    x = np.linspace(1.0-0.01,1.0,show_n)
    shape = x.shape +(2,)
    p = np.zeros(shape,dtype=float)
    p[...,0] = x

    print('Sxx, x轴靠近（1，0）:\n',Post[-1].Sxx(p),'\n\n')



    y = np.linspace(0.01,0.0,show_n)
    shape = x.shape +(2,)
    p = np.zeros(shape,dtype=float)
    p[...,0] += 1.0
    p[...,1] = y

    print('Sxx, y轴靠近（1，0）：\n',Post[-1].Sxx(p),'\n\n')




if False:

    show_n = 256


    x = np.linspace(0,1,show_n)
    y = np.linspace(1,0,show_n) #用imshows绘图时,y坐标轴刚好反了过来 
    X,Y = np.meshgrid(x,y)
    shape = X.shape
    shape +=(2,)
    p = np.zeros(shape,dtype=float)
    p[...,0] = X
    p[...,1] = Y

    Sigmah = Post128.Stress(p)
    print('s')
    print(np.max(np.abs(Sigmah[:,0,0]+100)))
    print(np.max(np.abs(Sigmah[:,0,2])))

    print(np.max(np.abs(Sigmah[0,:,1])))
    print(np.max(np.abs(Sigmah[0,:,2])))


    print(np.max(np.abs(Sigmah[-1,:,1])))
    print(np.max(np.abs(Sigmah[-1,:,2])))







############################收敛速度########################
if False:
    #mesh = Post8.mesh
    mesh_n = 10
    mesh = mf.special_boxmesh2d([0,1,0,1],n=mesh_n,meshtype='cross') 
    cellmeasure = mesh.entity_measure('cell')
    q = 5
    qf = mesh.integrator(q, etype='cell')

    bcs, ws = qf.get_quadrature_points_and_weights()
    ps = mesh.bc_to_point(bcs) #(NQ,NC,gdim)



    integralalg = FEMeshIntegralAlg(mesh, q,cellmeasure=cellmeasure)



    
    S = []
    U = []

    for i in range(7):
        print('The {} compute:'.format(i))
        S.append(Post[i].Stress(ps))
        U.append(Post[i].Displacement)


    u_err = []
    v_err = [] 
    sxx_err = []
    syy_err = []
    sxy_err = []
    
    for i in range(6):
        print('The {} compute err:'.format(i))
        f = (S[i][...,0] - S[-1][...,0])**2
        #print(ws.shape,f.shape,cellmeasure.shape)
        err = np.einsum('i,ij,j->j',ws,f,cellmeasure)
        err = np.sqrt(np.sum(err))
        #print(err-integralalg.error(Post[i].Sxx,Post[-1].Sxx))

        sxx_err.append(err)


        
        f = (S[i][...,1] - S[-1][...,1])**2
        err = np.einsum('i,ij,j->j',ws,f,cellmeasure)
        err = np.sqrt(np.sum(err))
        #print(err-integralalg.error(Post[i].Syy,Post[-1].Syy))
        syy_err.append(err)



        f = (S[i][...,2] - S[-1][...,2])**2
        err = np.einsum('i,ij,j->j',ws,f,cellmeasure)
        err = np.sqrt(np.sum(err))
        #print(err-integralalg.error(Post[i].Sxy,Post[-1].Sxy))
        sxy_err.append(err)





        f = (U[i][...,0]-U[-1][...,0])**2
        err = np.einsum('i,ij,j->j',ws,f,cellmeasure)
        err = np.sqrt(np.sum(err))
        u_err.append(err)








    print('Sxx error:\t',sxx_err,'\n')
    print('Syy error:\t',syy_err,'\n')
    print('Sxy error:\t',sxy_err,'\n')









